﻿using UnityEngine;
using System.Collections;

public class Dead : MonoBehaviour {

	private Stats stats;
	private bool isDead = false;


	public GameObject model;

	private float forceOnDeath = 10.0f;

	// Use this for initialization
	void Start () {
		stats = GetComponent<Stats>();
		if (model == null)
			Debug.LogError(transform.name + ": Has no model to use as Ragdoll in Dead.cs");
	}
	
	// Update is called once per frame
	void Update () {
		if (!isDead && stats.health.Current <= stats.health.Min)
		{
			isDead = true;
			//GetComponent<EnemyAnimationHandler>().SetTrigger(EnemyAnimationHandler.AnimationState.Death);
			//Destroy(gameObject, 1.0f);
			DisableComponents();
		}
	}

	private void DisableComponents()
	{
		GetComponent<RagdollManager>().enabled = true;
		GetComponent<RagdollManager>().SetActive(true);
		GetComponent<RagdollManager>().AddForce(-transform.forward * forceOnDeath);
		model.transform.parent = null;
		Destroy(gameObject);
		model.transform.FindChild("Body").GetComponent<GnaargEyes>().SetState(GnaargEyes.State.Deactivating);
	}
}

