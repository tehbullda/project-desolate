﻿using UnityEngine;
using System.Collections;

public class WeaponSparks : MonoBehaviour
{
	private const float normalOffset = 0.05f;
	public LayerMask layerMask;
	public float startPosition;
	public float endPosition;
	public ParticleSystem sparks;
	float distance;

	private BoxCollider collider;
	// Use this for initialization
	void Start()
	{
		collider = GetComponent<BoxCollider>();
	}

	// Update is called once per frame
	void Update()
	{
		Ray ray = new Ray(transform.position + transform.forward * startPosition, -transform.forward);
		//distance = endPosition - startPosition;
		distance = startPosition - endPosition;
		Debug.DrawRay(ray.origin, ray.direction * distance);
		RaycastHit hit;
		if (collider.enabled == true)
		{
			if (Physics.Raycast(ray, out hit, distance, layerMask))
			{
				Debug.Log("Hit");
				sparks.transform.position = hit.point + hit.normal * normalOffset;

				sparks.Play();
				Debug.DrawLine(ray.origin, hit.point, Color.red);
			}
		}
		else
		{
			sparks.Stop();
		}
	}
	void OnDrawGizmos()
	{
		Gizmos.color = Color.red;
		Gizmos.DrawSphere(transform.position + transform.forward * startPosition, 0.05f);
		Gizmos.DrawSphere(transform.position + transform.forward * endPosition, 0.05f);
	}
}
