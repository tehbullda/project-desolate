﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour
{

    public float lifetime = 7.0f;
    public float damage = 25.0f;
    public float projectileSpeed = 20.0f;
    private Vector3 direction;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        transform.position += direction * projectileSpeed * Time.deltaTime;
        lifetime -= Time.deltaTime;
        if (lifetime <= 0.0f)
        {
            Destroy(transform.gameObject);
        }
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            other.GetComponent<Health>().ReduceHealth(damage);
        }
        if (!other.name.Contains("Projectile") && !other.name.Contains("HumanoidEnemyRanged") && !other.name.Contains("LoadingTrigger"))
        {
            Destroy(transform.gameObject);
        }
    }
    public void SetDirection(Vector3 dir)
    {
        direction = dir;
    }
}
