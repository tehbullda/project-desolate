﻿using UnityEngine;
using System.Collections;

public class BossAOE : MonoBehaviour {

    public ParticleSystem windUp;
    public ParticleSystem AOE;


    private float windUpTime = 1.8f;


    //private float startRadius = 0;
    //private float endRadius = 5;
    private float currentRadius;
    //private float expansionSpeed = 10;
    private float damageTime = 0.15f;
    private bool damaging = false;
    private float damage = 40.0f;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
#if UNITY_EDITOR
		if (Input.GetKeyDown(KeyCode.Y))
            StartCoroutine(AOEAttack());
#endif
	}
    public IEnumerator AOEAttack()
    {
        Debug.Log("Coroutine Started");
        windUp.Play();
        yield return new WaitForSeconds(windUpTime);
        AOE.Play();
        damaging = true;
        yield return new WaitForSeconds(damageTime);
        damaging = false;
        Debug.Log("Coroutine Done");
    }
    void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player" && damaging)
        {
            damaging = false;
            other.GetComponent<Health>().ReduceHealth(damage);
            AnalyticsSubmitter.UpdateVariableAdd(AnalyticsSubmitter.AnalyticsType.DamageTakenFromBoss, (int)damage);
        }
    }
}
