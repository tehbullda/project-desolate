﻿using UnityEngine;
using System.Collections;

public class GenericElevatorButton : MonoBehaviour
{
    public enum State
    {
        Start,
        End
    }
    public State currentState;
    public Vector3 startPosition;
    public Vector3 endPosition;
    public float moveSpeed;

    private Vector3 targetPosition;

    private float color;
    private bool activated = false;
    public ParticleSystem smoke;

    // Use this for initialization
    void Start()
    {
        transform.position = startPosition;

    }

    // Update is called once per frame
    void Update()
    {
        UpdateEmission();
    }
    void UpdateEmission()
    {
        color = activated ? 0.6f : Mathf.PingPong(Time.time / 2, 0.3f);

        GetComponent<Renderer>().material.SetColor("_EmissionColor", new Color(color, color, color, 1.0f));

    }
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player" && !activated)
        {
            Activate();
        }
    }
    public void Activate()
    {

        StartCoroutine(ElevatorActivated());

    }
    public State GetCurrentState()
    {
        return currentState;
    }
    IEnumerator ElevatorActivated()
    {
        activated = true;
        targetPosition = getTargetPosition(currentState);
        smoke.Play();
        yield return new WaitForSeconds(1.0f);
        while (transform.position != targetPosition)
        {
            transform.position = Vector3.MoveTowards(transform.position, targetPosition,moveSpeed * Time.deltaTime);
            yield return null;
        }
        yield return new WaitForSeconds(1.0f);
        activated = false;

        SwitchState();
        //Open
    }

    void SwitchState()
    {
        if (State.End == currentState)
            currentState = State.Start;
        else if (State.Start == currentState)
            currentState = State.End;
    }
    Vector3 getTargetPosition(State state)
    {
        if (state == State.Start)
            return endPosition;
        if (state == State.End)
            return startPosition;

        Debug.Log("currentState not set");
        return startPosition;
    }
    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawSphere(startPosition, 0.3f);
        Gizmos.DrawSphere(endPosition, 0.3f);
        Gizmos.DrawLine(startPosition, endPosition);
    }
}
