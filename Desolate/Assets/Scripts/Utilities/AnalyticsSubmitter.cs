﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Analytics;

public class AnalyticsSubmitter : MonoBehaviour
{

    public enum AnalyticsType
    {
        DamageTaken,
        DamageTakenFromBoss,
        DamageDealt,
        GameCompleted,
        DeathCount,
        KillCount,
        HealingCount,
        HealthExpansions,
        StaminaExpansions,
        Count
    }
    private static int damageTaken = 0, damageTakenFromBoss = 0, damageDealt = 0, gameCompleted = 0, deathCount = 0, killCount = 0, healingCount = 0, healthExpansions = 0, staminaExpansions = 0;
    private bool wantsquit = false, submitteddata = false;
    // Use this for initialization
    void Start()
    {
        //Debug.Log(SendReport());
    }

    // Update is called once per frame
    void Update()
    {
        if (wantsquit && !submitteddata)
        {
            StartCoroutine(QuitWorkaroundRoutine());
        }
    }
    private void ResetAll()
    {
        damageTaken = 0;
        damageTakenFromBoss = 0;
        damageDealt = 0;
        gameCompleted = 0;
        deathCount = 0;
        killCount = 0;
        healingCount = 0;
    }
    public static void UpdateVariableAdd(AnalyticsType type, int data)
    {
        switch (type)
        {
            case AnalyticsType.DamageTaken:
                damageTaken += data;
                break;
            case AnalyticsType.DamageTakenFromBoss:
                damageTakenFromBoss += data;
                break;
            case AnalyticsType.DamageDealt:
                damageDealt += data;
                break;
            case AnalyticsType.GameCompleted:
                gameCompleted += data;
                break;
            case AnalyticsType.DeathCount:
                deathCount += data;
                break;
            case AnalyticsType.KillCount:
                killCount += data;
                break;
            case AnalyticsType.HealingCount:
                healingCount += data;
                break;
            case AnalyticsType.HealthExpansions:
                healthExpansions += data;
                break;
            case AnalyticsType.StaminaExpansions:
                staminaExpansions += data;
                break;
            default:
                break;
        }
    }
    public static void UpdateVariableSet(AnalyticsType type, int data)
    {
        switch (type)
        {
            case AnalyticsType.DamageTaken:
                damageTaken = data;
                break;
            case AnalyticsType.DamageTakenFromBoss:
                damageTakenFromBoss = data;
                break;
            case AnalyticsType.DamageDealt:
                damageDealt = data;
                break;
            case AnalyticsType.GameCompleted:
                gameCompleted = data;
                break;
            case AnalyticsType.DeathCount:
                deathCount = data;
                break;
            case AnalyticsType.KillCount:
                killCount = data;
                break;
            case AnalyticsType.HealingCount:
                healingCount = data;
                break;
            case AnalyticsType.HealthExpansions:
                healthExpansions = data;
                break;
            case AnalyticsType.StaminaExpansions:
                staminaExpansions = data;
                break;
            default:
                break;
        }
    }
    public static AnalyticsResult SendReport()
    {
        return Analytics.CustomEvent("gameEnded", new Dictionary<string, object>
            {
            {"damageTaken", damageTaken },
            {"damageTakenFromBoss", damageTakenFromBoss },
            {"damageDealt", damageDealt },
            {"gameCompleted", gameCompleted },
            {"deathCount", deathCount },
            {"killCount", killCount },
            {"healingsUsed", healingCount },
            {"healthExpansions", healthExpansions },
            {"staminaExpansions", staminaExpansions }
            });
    }

    void OnApplicationQuit()
    {
        if (!submitteddata)
        {
            wantsquit = true;
            Application.CancelQuit();
        }
    }
    private IEnumerator QuitWorkaroundRoutine()
    {
        SendReport();
        //Debug.Log(result);
        submitteddata = true;
        GameObject.Find("BlackFadeScreen").GetComponent<FadeScript>().FadeElement(Color.black, 2.0f, 0);
        yield return new WaitForSeconds(2.5f);
        Application.Quit();
    }
}
