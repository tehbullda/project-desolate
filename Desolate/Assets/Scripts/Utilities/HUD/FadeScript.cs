﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEditor;
using UnityEditor.AnimatedValues;

public class FadeScript : MonoBehaviour
{
	public bool fadeSelf = false;
	public MaskableGraphic element;
	public bool onStart;
	public Color startColor;
	public Color targetColor;
	public float time;
	public float timeBeforeFade;

	Coroutine fadeCoroutine;
	// Use this for initialization
	void Start()
	{
		if (fadeSelf)
			element = GetComponentInChildren<MaskableGraphic>();
		element.color = startColor;
		if (onStart && element)
		{
			FadeElement(targetColor, time, timeBeforeFade);
		}
		element.gameObject.SetActive(true);
	}

	// Update is called once per frame
	void Update()
	{

	}
	public void FadeElement(Color target, float time, float timeBeforeFade)
	{

		if (fadeCoroutine != null)
			StopCoroutine(fadeCoroutine);
		fadeCoroutine = StartCoroutine(Fade(target, time, timeBeforeFade));

	}
	private void ForceColor(Color target)
	{
		element.color = target;
	}
	private IEnumerator Fade(Color target, float time, float timeBeforeFade)
	{
		yield return new WaitForSeconds(timeBeforeFade);
		float currentTime = 0.0f;
		Color original = element.color;
		while (currentTime < time)
		{
			currentTime += Time.deltaTime;
			element.color = Color.Lerp(original, target, currentTime / time);
			yield return new WaitForEndOfFrame();
		}
		fadeCoroutine = null;
	}
}
[CanEditMultipleObjects]
[CustomEditor(typeof(FadeScript))]
public class FadeEditor : Editor
{

	public override void OnInspectorGUI()
	{
		FadeScript fadeScript = (FadeScript)target;

		EditorGUILayout.Space();
		EditorGUILayout.LabelField("Fade", EditorStyles.boldLabel);

		fadeScript.onStart = EditorGUILayout.Toggle("Play On Start: ", fadeScript.onStart);
		if (fadeScript.onStart)
		{
			fadeScript.time = EditorGUILayout.FloatField("    Fade Time: ", fadeScript.time);
			fadeScript.timeBeforeFade = EditorGUILayout.FloatField("    Time Before Fade: ", fadeScript.timeBeforeFade);
			fadeScript.startColor = EditorGUILayout.ColorField("    Start Color: ", fadeScript.startColor);
			fadeScript.targetColor = EditorGUILayout.ColorField("    Target Color: ", fadeScript.targetColor);
		}
		fadeScript.fadeSelf = EditorGUILayout.Toggle("Fade Self: ", fadeScript.fadeSelf);
		if (!fadeScript.fadeSelf)
		{
			fadeScript.element = EditorGUILayout.ObjectField("    Element: ", fadeScript.element, typeof(MaskableGraphic), true) as MaskableGraphic;
		}
	}
}


