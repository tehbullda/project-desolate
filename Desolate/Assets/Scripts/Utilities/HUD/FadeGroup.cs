﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEditor;

public class FadeGroup : MonoBehaviour
{
	public bool fadeAllChildren;
	public FadeScript[] objectsToFade;
	public bool onStart = false;
	public float timeBeforeFade = 0.0f;
	public float time;
	public Color startColor;
	public Color targetColor;
	// Use this for initialization
	void Start()
	{

		if (fadeAllChildren)
		{
			objectsToFade = new FadeScript[transform.childCount];
			for (int i = 0; i < transform.childCount; ++i)
			{
				Transform child = transform.GetChild(i);
				if (!child.GetComponent<FadeScript>())
					child.gameObject.AddComponent<FadeScript>();

				child.GetComponent<FadeScript>().fadeSelf = true;
				//child.GetComponent<FadeScript>().onStart = onStart;
				//child.GetComponent<FadeScript>().timeBeforeFade = timeBeforeFade;
				child.GetComponent<FadeScript>().startColor = startColor;
				if (onStart)
					child.GetComponent<FadeScript>().FadeElement(targetColor, time, timeBeforeFade);
				objectsToFade[i] = child.GetComponent<FadeScript>();

			}
		}
	}
	void Update()
	{
		if (Input.GetKeyDown(KeyCode.Space))
			FadeElements(targetColor, time, timeBeforeFade);
	}

	public void FadeElements(Color targetColor, float fadeTime, float timeBeforeFade)
	{
		for (int i = 0; i < objectsToFade.Length; ++i)
		{
			objectsToFade[i].FadeElement(targetColor, fadeTime, timeBeforeFade);
		}
	}
}

[CanEditMultipleObjects]
[CustomEditor(typeof(FadeGroup))]
public class FadeGroupEditor : Editor
{

	public override void OnInspectorGUI()
	{
		FadeGroup fadeScript = (FadeGroup)target;

		EditorGUILayout.Space();
		EditorGUILayout.LabelField("Fade", EditorStyles.boldLabel);

		fadeScript.onStart = EditorGUILayout.Toggle("Play On Start: ", fadeScript.onStart);
		if (fadeScript.onStart)
		{
			fadeScript.time = EditorGUILayout.FloatField("    Fade Time: ", fadeScript.time);
			fadeScript.timeBeforeFade = EditorGUILayout.FloatField("    Time Before Fade: ", fadeScript.timeBeforeFade);
			fadeScript.startColor = EditorGUILayout.ColorField("    Start Color: ", fadeScript.startColor);
			fadeScript.targetColor = EditorGUILayout.ColorField("    Target Color: ", fadeScript.targetColor);
		}
	}
}
