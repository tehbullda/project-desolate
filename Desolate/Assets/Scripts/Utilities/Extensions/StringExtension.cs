﻿using UnityEngine;
using System.Collections;
using System.Text.RegularExpressions;

public static class StringExtension {
	/// <summary>
	/// Adds a space before each capital letter in a camelCase string, and capitalizes first letter
	/// </summary>
	public static string FormatCamelCase(this string p_string)
	{
		if (string.IsNullOrEmpty(p_string) || p_string.Length < 2)
			return p_string;

		MatchCollection matches = Regex.Matches(p_string, @"([a-z][A-Z])");

		for (int i = 0; i < matches.Count; ++i)
		{
			p_string = p_string.Insert(matches[i].Index + (1 + i), " ");
		}

		return char.ToUpper(p_string[0]) + p_string.Substring(1);
	}

	/// <summary>
	/// Capitalizes the first letter in a given string
	/// </summary>
	public static string CapitalizeFirstLetter(this string p_string)
	{
		if (string.IsNullOrEmpty(p_string))
			return p_string;

		if (p_string.Length == 1)
			return char.ToUpper(p_string[0]).ToString();

		return char.ToUpper(p_string[0]) + p_string.Substring(1);
	}

	/// <summary>
	/// Removes the string <i>p_toRemove</i> from all strings in the array
	/// </summary>
	public static string[] RemoveFromAll(this string[] p_strings, string p_toRemove)
	{
		for (int i = 0; i < p_strings.Length; ++i)
		{
			p_strings[i] = p_strings[i].Replace(p_toRemove, "");
		}

		return p_strings;
	}
}
