﻿using UnityEngine;
using System.Collections;

public static class Vector3Extension {
	public static Vector3 RandomAdd(this Vector3 target, float min, float max)
	{
		target.x += UnityEngine.Random.Range(min, max);
		target.y += UnityEngine.Random.Range(min, max);
		target.z += UnityEngine.Random.Range(min, max);
		return target;
	}
}
