﻿using UnityEngine;
using UnityEditor;
using System.Collections;

public static class TransformExtensions {
#if (UNITY_EDITOR)
	/// <summary>
	/// Checks if a child of the object is currently selected in the editor
	/// </summary>
	public static bool IsChildSelected(this Transform parent)
	{
		for (int i = 0; i < parent.childCount; ++i)
		{
			if (Selection.activeTransform == parent.GetChild(i))
				return true;
		}

		return false;
	}
#endif
}
