﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Analytics;
using System.Collections.Generic;

public class GameCompleteUIFader : MonoBehaviour
{

    bool dunn = false;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (ProgressStatics.gameComplete && !dunn)
        {
            AnalyticsSubmitter.SendReport();
            StartCoroutine(Fade(1.0f));
            dunn = true;
            AnalyticsSubmitter.UpdateVariableAdd(AnalyticsSubmitter.AnalyticsType.GameCompleted, 1);
            
        }

    }
    IEnumerator Fade(float toAlpha)
    {
        Image img = GetComponent<Image>();
        Color original = img.color;
        Color target = new Color(original.r, original.g, original.b, toAlpha);
        float elapsedtime = 0.0f;
        float totaltime = 4.0f;
        while (elapsedtime < totaltime)
        {
            elapsedtime += Time.deltaTime;
            img.color = Color.Lerp(original, target, (elapsedtime / totaltime));
            yield return new WaitForEndOfFrame();
        }
        GameObject.Find("BlackFadeScreen").GetComponent<FadeScript>().FadeElement(Color.black, 1.0f, 0);
        yield return new WaitForSeconds(1.0f);
        ProgressStatics.gameComplete = false;
		SceneManager.LoadScene(0);
    }
}
