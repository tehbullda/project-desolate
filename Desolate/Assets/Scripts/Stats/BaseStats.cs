﻿using UnityEngine;
using System.Collections;

public class BaseStats : ScriptableObject {
	[Header("Primary Stats")]
	[Tooltip("The amount of health the unit starts with")]
	public int health;
	[Tooltip("The amount of damage the unit starts with")]
	public int damage;
	[Tooltip("The amount of stamina the unit starts with")]
	public float stamina;
	[Tooltip("How stable the unit is - higher number means more time between staggers")]
	public int stability;
	[Tooltip("Base healing power, affecting how much a unit heals")]
	public int healingPower;
	[Tooltip("Armor reduces the damage taken by the unit")]
	public int armor;
	[Tooltip("How quickly a unit moves")]
	public int movementSpeed;
}
