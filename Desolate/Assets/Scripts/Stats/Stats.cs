﻿using UnityEngine;
using System;
using System.Collections;

//Wrapper
public class Stats : MonoBehaviour {
	public struct IntStat
	{
		public IntStat(int p_baseValue)
		{
			min = 0;
			max = p_baseValue;
			current = p_baseValue;
			baseValue = p_baseValue;
		}

		private int min;
		private int max;
		private int current;
		private int baseValue;

		public int Min
		{
			get { return min; }
			set { min = value; }
		}

		public int Max
		{
			get { return max; }
			set { max = value; }
		}

		public int Current
		{
			get { return current; }
			set { current = Mathf.Clamp(value, min, max); }
		}

		public int BaseValue
		{
			get { return baseValue; }
			set { baseValue = value; }
		}
	}

	public struct FloatStat
	{
		public FloatStat(float p_baseValue)
		{
			min = 0.0f;
			max = p_baseValue;
			current = p_baseValue;
			baseValue = p_baseValue;
		}

		private float min;
		private float max;
		private float current;
		private float baseValue;

		public float Min
		{
			get { return min; }
			set { min = value; }
		}

		public float Max
		{
			get { return max; }
			set { max = value; }
		}

		public float Current
		{
			get { return current; }
			set { current = value; }
		}

		public float BaseValue
		{
			get { return baseValue; }
			set { baseValue = value; }
		}
	}

	[SerializeField] private BaseStats statAsset;
	[NonSerialized] public IntStat health;
	[NonSerialized] public IntStat damage;
	[NonSerialized] public FloatStat stamina;
	[NonSerialized] public IntStat stability;
	[NonSerialized] public IntStat healingPower;
	[NonSerialized] public IntStat armor;
	[NonSerialized] public IntStat movementSpeed;

	void Awake()
	{
		health = new IntStat(statAsset.health);
		damage = new IntStat(statAsset.damage);
		stamina = new FloatStat(statAsset.stamina);
		stability = new IntStat(statAsset.stability);
		healingPower = new IntStat(statAsset.healingPower);
		armor = new IntStat(statAsset.armor);
		movementSpeed = new IntStat(statAsset.movementSpeed);
	}
}
