﻿using UnityEngine;
using System.IO;
using System.Text;
using ProtoBuf;
using ProtoBuf.Meta;

public static class SaveManager
{
	private static RuntimeTypeModel typeModel;

	[ProtoContract]
	public struct Header
	{
		[ProtoMember(1)] public int major;
		[ProtoMember(2)] public int minor;
		[ProtoMember(3)] public long date;

		[ProtoMember(4)] public float secondsPlayed;
	}
	
	public static void Init()
	{
		typeModel = RuntimeTypeModel.Create();
		typeModel.AutoAddMissingTypes = true;
		typeModel.AutoAddProtoContractTypesOnly = true;
		typeModel.AutoCompile = true;

		typeModel.Add(typeof(Vector2), false)
			.Add("x", "y");
		typeModel.Add(typeof(Vector3), false)
			.Add("x", "y", "z");
		typeModel.Add(typeof(Vector4), false)
			.Add("x", "y", "z", "w");
		typeModel.Add(typeof(Quaternion), false)
			.Add("x", "y", "z", "w");
		typeModel.Add(typeof(Color32), false)
			.Add("r", "g", "b", "a");

		//Add more stuff here

		typeModel.Add(typeof(Header), true);
	}

	public static void WriteSchemas()
	{
		if (typeModel == null)
			Init();

		string fileName = string.Format("SaveData-{0}.{1}.proto", GameVersion.VERSION_MAJOR, GameVersion.VERSION_MINOR);
		fileName = Path.Combine("SaveSchemas", fileName);

		StringBuilder sb = new StringBuilder(1024);
		sb.Append(typeModel.GetSchema(typeof(Header)));
		sb.Append(typeModel.GetSchema(typeof(SaveData)));

		File.WriteAllText(fileName, sb.ToString());
	}

	public static bool Save()
	{
		if (typeModel == null)
			Init();
		
		Header header = CreateHeader();

		SerializationContext context = new SerializationContext();
		context.State = System.Runtime.Serialization.StreamingContextStates.Persistence;
		context.Context = SaveData.Get();

		try
		{
			using (FileStream fileStream = File.Open(Application.persistentDataPath + "savefile.sav", FileMode.Truncate))
			{
				typeModel.SerializeWithLengthPrefix(fileStream, header, typeof(Header), PrefixStyle.Base128, 1);
				typeModel.Serialize(fileStream, SaveData.Get(), context);
			}
		}
		catch (System.Exception ex)
		{
			Debug.LogErrorFormat("Error loading save - {0}", ex);
			return false;
		}

		return true;
	}

	public static bool Load()
	{
		if (typeModel == null)
			Init();
		
		Header header = new Header();

		SerializationContext context = new SerializationContext();
		context.State = System.Runtime.Serialization.StreamingContextStates.Persistence;
		context.Context = SaveData.Get();

		try
		{
			using (FileStream fileStream = File.Open(Application.persistentDataPath + "savefile.sav", FileMode.Open))
			{
				header = (Header)typeModel.DeserializeWithLengthPrefix(fileStream, null, typeof(Header), PrefixStyle.Base128, 1);
				typeModel.Deserialize(fileStream, SaveData.Get(), typeof(SaveData), context);
			}
		}
		catch (ProtoException ex)
		{
			Debug.LogErrorFormat("Error loading save - {0}", ex);
			return false;
		}
		catch (IOException ex)
		{
			Debug.LogErrorFormat("Error loading save - {0}", ex);
			return false;
		}

		return true;
	}

	private static Header CreateHeader()
	{
		return new Header()
		{
			major = GameVersion.VERSION_MAJOR,
			minor = GameVersion.VERSION_MINOR,
			date = System.DateTime.Now.ToFileTimeUtc(),
			secondsPlayed = Time.realtimeSinceStartup		//Need to generate an actual playtime here, but that's for later.
		};
	}
}

#if (UNITY_EDITOR)
public class SchemaMenuItem : MonoBehaviour
{
	[UnityEditor.MenuItem("Build/Build Save Schemas")]
	public static void BuildSchemas()
	{
		SaveManager.WriteSchemas();
		Debug.Log("Done saving schemas!");
	}
}
#endif