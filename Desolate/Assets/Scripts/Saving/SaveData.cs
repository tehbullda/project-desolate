﻿using UnityEngine;
using System.Collections.Generic;
using ProtoBuf;

[ProtoContract]
public class SaveData : MonoBehaviour {
	private static SaveData singleton = null;
	
	[ProtoMember(1, AsReference = true)] public DoorStateManager doorStateManager;

	public static SaveData Get()
	{
		if (singleton == null)
		{
			singleton = GameObject.FindObjectOfType<SaveData>();
		}

		return singleton;
	}
}