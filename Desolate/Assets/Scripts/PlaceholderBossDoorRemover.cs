﻿using UnityEngine;
using System.Collections;

public class PlaceholderBossDoorRemover : MonoBehaviour {

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
        if (ProgressStatics.bossdefeated)
            gameObject.SetActive(false);

	}
}
