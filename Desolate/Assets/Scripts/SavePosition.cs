﻿using UnityEngine;
using System.Collections;

public class SavePosition : MonoBehaviour {

    public const string playerSaveTag = "PlayerPosition";
    [SerializeField]
    [Tooltip("Objects position will be saved every x seconds")]
    private float saveInterval;
    private float currentTime;
	void Start () {
        currentTime = saveInterval;
        //if (SaveManager.CheckTagExists(playerSaveTag) && !ProgressStatics.resetPlayer)
        //{
        //    transform.position = SaveManager.ReadData<Vector3>(playerSaveTag);
        //}
	}
	void Update () {
        currentTime -= Time.deltaTime;
        if (currentTime <= 0.0f)
        {
            currentTime = saveInterval;
            //SaveManager.SaveData<Vector3>(transform.position, playerSaveTag);
        }
	}
}
