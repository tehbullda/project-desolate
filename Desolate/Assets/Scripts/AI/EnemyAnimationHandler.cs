﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemyAnimationHandler : MonoBehaviour
{
	public enum AnimationState
	{
		Idle,
		Locomotion,
		Combat_1,
		Combat_2,
		Combat_3,
		Death,
		RotateLeft,
		RotateRight,
		Strafe,
		Healing,
		Staggered,
		Boss_Shielding,
		Count
	}
	private const AnimationState defaultState = AnimationState.Idle;
	private Animator animator;
	private AnimationState currentState;
	private NavMeshAgent agent;
	private float triggerActivationInternalCooldown = 0.25f;
	private float currentCooldown;
	void Start()
	{
		animator = GetComponentInChildren<Animator>();
		agent = GetComponent<NavMeshAgent>();
		currentState = defaultState;
	}

	void Update()
	{
		//if (agent.velocity.magnitude > 0.0f)
		//	animator.SetFloat(EnemyHashIDs.speedFloat, agent.speed / agent.velocity.magnitude);
		//else
		//{
		//	animator.SetFloat(EnemyHashIDs.speedFloat, 0);
		//}
		UpdateSpeed();
		if (currentCooldown >= 0.0f)
		{
			currentCooldown -= Time.deltaTime;
			//return;
		}
		if (animator.GetCurrentAnimatorStateInfo(0).fullPathHash == EnemyHashIDs.locomotionState)
		{
			currentState = AnimationState.Locomotion;
		}
		if (animator.GetCurrentAnimatorStateInfo(0).fullPathHash == EnemyHashIDs.idleState)
		{
			currentState = AnimationState.Idle;
		}
	}
	public AnimationState GetCurrentState()
	{
		return currentState;
	}
	public bool SetTrigger(AnimationState state)
	{
		if (state == AnimationState.Staggered)
		{
			ActivateTrigger(state);
			currentState = state;
		}
		if (currentState == AnimationState.Idle && currentCooldown <= 0.0f && ActivateTrigger(state))
		{
			currentState = state;
			return true;
		}
		return false;
	}
	public bool IsRotating()
	{
		if (animator.GetCurrentAnimatorStateInfo(0).fullPathHash == EnemyHashIDs.rotationLeftState || animator.GetCurrentAnimatorStateInfo(0).fullPathHash == EnemyHashIDs.rotationRightState)
		{
			return true;
		}
		return false;
	}
	public bool IsAttacking()
	{
		if (animator.GetCurrentAnimatorStateInfo(0).fullPathHash == EnemyHashIDs.lightAttackState || animator.GetCurrentAnimatorStateInfo(0).fullPathHash == EnemyHashIDs.heavyAttackState || animator.GetCurrentAnimatorStateInfo(0).fullPathHash == EnemyHashIDs.sweepAttackState)
		{
			return true;
		}
		return false;
	}
	public bool IsOnCooldown()
	{
		return currentCooldown > 0.0f;
	}
	public bool IsIdle()
	{
		return animator.GetBool(EnemyHashIDs.idleBool);
	}
	public bool ShouldNotMove()
	{
		if (animator.GetCurrentAnimatorStateInfo(0).fullPathHash == EnemyHashIDs.locomotionState || animator.GetCurrentAnimatorStateInfo(0).fullPathHash == EnemyHashIDs.idleState)
			return false;
		return true;
	}
	public void SetRotationBoolsFalse()
	{
		animator.SetBool(EnemyHashIDs.rotatingLeftBool, false);
		animator.SetBool(EnemyHashIDs.rotatingRightBool, false);
	}
	private bool ActivateTrigger(AnimationState state)
	{
		switch (state)
		{
			case AnimationState.Idle:
			case AnimationState.Locomotion:
			case AnimationState.Count:
				Debug.Log("Attempt to activate animationState that is not a trigger.");
				return false;
			case AnimationState.Combat_1:
				animator.SetTrigger(EnemyHashIDs.lightAttackTrigger);
				break;
			case AnimationState.Combat_2:
				animator.SetTrigger(EnemyHashIDs.heavyAttackTrigger);
				break;
			case AnimationState.Combat_3:
				animator.SetTrigger(EnemyHashIDs.sweepAttackTrigger);
				break;
			case AnimationState.Death:
				animator.SetTrigger(EnemyHashIDs.deadTrigger);
				break;
			case AnimationState.RotateLeft:
				animator.SetBool(EnemyHashIDs.rotatingLeftBool, true);
				break;
			case AnimationState.RotateRight:
				animator.SetBool(EnemyHashIDs.rotatingRightBool, true);
				break;
			case AnimationState.Strafe:
				animator.SetTrigger(EnemyHashIDs.strafeTrigger);
				break;
			case AnimationState.Healing:
				animator.SetTrigger(EnemyHashIDs.healingTrigger);
				break;
			case AnimationState.Staggered:
				animator.SetTrigger(EnemyHashIDs.staggeredTrigger);
				break;
			case AnimationState.Boss_Shielding:
				animator.SetBool(EnemyHashIDs.bossShieldingBool, true);
				break;
		}
		animator.SetBool(EnemyHashIDs.idleBool, false);
		currentCooldown = triggerActivationInternalCooldown;
		return true;
	}
	private void UpdateSpeed()
	{
		float distanceForMaxSpeed = 5.0f;
		if (agent.remainingDistance > distanceForMaxSpeed)
		{
			animator.SetFloat(EnemyHashIDs.speedFloat, 1.0f);
		}
		else if (agent.remainingDistance > 1.5f)
		{
			animator.SetFloat(EnemyHashIDs.speedFloat, agent.remainingDistance / distanceForMaxSpeed);
		}
		else
		{
			animator.SetFloat(EnemyHashIDs.speedFloat, 0);
		}
	}
}
