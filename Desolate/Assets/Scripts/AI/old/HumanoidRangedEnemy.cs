﻿using UnityEngine;
using System.Collections;

public class HumanoidRangedEnemy : MonoBehaviour
{

    enum State
    {
        Idle,
        Engaged,
        Count
    }
    [SerializeField]
    private GameObject projectile;
    private GameObject player;
    [SerializeField]
    private Animator animator;
    private NavMeshAgent agent;
    [SerializeField]
    private float moveSpeed = 3.0f, acceleration = 2.0f, rotationSpeed = 10.0f, stoppingDistanceEngaged = 7.0f, aggroRange = 10.0f, distanceForReset = 20.0f, timeBetweenFiringAttacks = 1.5f, cone = 15.0f;
    private float currentAttackCD;
    private State state;
    private Vector3 startPos;
    [SerializeField]
    private float stoppingDistanceIdle = 1.0f;
    private Quaternion targetRotation;
    private float rotationTimer = 1.5f, rotationTimerCurrent;
    bool projectilefired = false;
    // Use this for initialization
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        agent.speed = moveSpeed;
        agent.acceleration = acceleration;
        agent.stoppingDistance = stoppingDistanceIdle;
        if (!player)
        {
            player = GameObject.Find("RemadePlayer");
        }
        state = State.Idle;
        startPos = transform.position;
        rotationTimerCurrent = rotationTimer;
    }

    // Update is called once per frame
    void Update()
    {
        if (!player)
            return;
        animator.SetFloat("Speed", agent.velocity.magnitude / agent.speed);
        if (animator.GetCurrentAnimatorStateInfo(0).IsName("RangedAttack"))
        {
            if (animator.GetCurrentAnimatorStateInfo(0).normalizedTime > 0.5f && !projectilefired)
            {
                projectilefired = true;
                FireProjectile();
                animator.SetBool("Attacking_Ranged", false);
            }
            return;
        }
        animator.SetBool("RotationLeft", false);
        animator.SetBool("RotationRight", false);
        //animator.SetBool("RotationLeft", false);
        //animator.SetBool("RotationRight", false);
        projectilefired = false;
        float distance = Vector3.Distance(transform.position, player.transform.position);
        if (state == State.Idle)
        {
            if (distance <= aggroRange)
            {
                state = State.Engaged;
                agent.destination = player.transform.position;
                agent.stoppingDistance = stoppingDistanceEngaged;
                currentAttackCD = timeBetweenFiringAttacks;
            }
        }
        else if (state == State.Engaged)
        {
            targetRotation = Quaternion.LookRotation(player.transform.position - transform.position);
            if (distance > distanceForReset)
            {
                agent.destination = startPos;
                agent.stoppingDistance = stoppingDistanceIdle;
                state = State.Idle;
            }
            if (distance <= stoppingDistanceEngaged)
            {
                agent.speed *= 0.9f;
                currentAttackCD -= Time.deltaTime;
                Vector2 differencewithoutY = new Vector2(transform.position.x - player.transform.position.x, transform.position.z - player.transform.position.z);
                if (Vector2.Angle(differencewithoutY, new Vector2(transform.forward.x, transform.forward.z)) > 180 - (cone / 2))
                {
                    if (currentAttackCD <= 0.0f)
                    {
                        animator.SetBool("Attacking_Ranged", true);
                    }
                }
                else
                {
                    rotationTimerCurrent -= Time.deltaTime;
                    if (rotationTimerCurrent <= 0.0f)
                    {
                        //Now we need to find out if we are going to rotate right of left ... lets do this
                        float angle = Quaternion.Angle(transform.rotation, targetRotation);
                        Quaternion checker = transform.rotation * Quaternion.AngleAxis(2.0f, new Vector3(0, 1, 0)); //And the award for best solution of the year goes to...
                        if (Quaternion.Angle(checker, targetRotation) > angle)
                        {
                            animator.SetBool("RotationLeft", true);
                        }
                        else
                        {
                            animator.SetBool("RotationRight", true);
                        }
                        transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, rotationSpeed * Time.deltaTime);
                        if (Quaternion.Angle(transform.rotation, targetRotation) < 4.0f || Quaternion.Angle(transform.rotation, player.transform.rotation) < 4.0f)
                        {
                            rotationTimerCurrent = rotationTimer;
                            animator.SetBool("RotationLeft", false);
                            animator.SetBool("RotationRight", false);
                        }
                    }
                }
            }
            else
            {
                agent.SetDestination(player.transform.position);
            }
        }
        animator.SetFloat("Speed", agent.velocity.magnitude / agent.speed);
    }
    private void FireProjectile()
    {
        GameObject tmp = (GameObject)GameObject.Instantiate(projectile, transform.position, Quaternion.identity);
        Vector3 dir = player.transform.position - transform.position;
        if (Random.Range(0.0f, 1.0f) > 0.7f)
            tmp.GetComponent<Projectile>().SetDirection(player.transform.position - transform.position);
        else
            tmp.GetComponent<Projectile>().SetDirection(new Vector3(Random.Range(-5f, 5f), dir.y, Random.Range(-5f, 5f)));
        currentAttackCD = timeBetweenFiringAttacks;
        animator.SetBool("Attacking_Ranged", false);
    }
}
