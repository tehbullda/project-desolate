﻿using UnityEngine;
using System.Collections;
using System.Text.RegularExpressions;

public class AggressivePatrolBehavior : AIBehaviorBase
{

    public GameObject patrolParent;
    public float stoppingDistancePatrolling;
    public GameObject playerObject;
    public float playerDetectionRange;
    public Animator animator;
    NavMeshAgent agent;
    GameObject currentGoal;
    bool chasing/*, attacking, locked*/;

    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        if (patrolParent.transform.childCount > 0)
        {
            currentGoal = patrolParent.transform.GetChild(0).gameObject;
            agent.SetDestination(currentGoal.transform.position);
        }
        chasing = /*attacking = locked =*/ false;
        if (!playerObject)
        {
            playerObject = GameObject.Find("RemadePlayer");
        }
    }
    void Update()
    {
        Sense();
        Decide();
        Act();
    }

    protected override void Sense()
    {
        if (Vector3.Distance(transform.position, playerObject.transform.position) < playerDetectionRange && !chasing)
        {
            chasing = true;
        }
        else if (Vector3.Distance(transform.position, currentGoal.transform.position) < stoppingDistancePatrolling)
        {
            int currentIndex = System.Int32.Parse(Regex.Match(currentGoal.name, @"\d+").Value) - 1;
            currentGoal = (patrolParent.transform.childCount-1) == currentIndex ? patrolParent.transform.GetChild(0).gameObject : patrolParent.transform.GetChild(++currentIndex).gameObject;
            agent.SetDestination(currentGoal.transform.position);
        }
    }
    protected override void Decide()
    {

    }
    protected override void Act()
    {
        animator.SetFloat("Speed", agent.velocity.magnitude / agent.speed);
    }
}
