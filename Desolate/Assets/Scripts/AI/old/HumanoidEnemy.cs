﻿using UnityEngine;
using System.Collections;

public class HumanoidEnemy : MonoBehaviour
{
    private NavMeshAgent agent;
    public float playerDetectionRangeIdle, playerDetectionRangeChasing, stoppingRangeWhenChasing, stoppingRangeWhenReturning;
    public GameObject playerObject;
    private Vector3 startPos;
    public Animator animator;
    Quaternion targetRotation;
    public float rotationSpeed, viableAttackCone, idleTimeBeforeReset;
    private float currentIdleTime, playerDetectionRange, idletimeBeforeBehaviorResumes = 1.0f, currentIdleBehaviorTime = 0.0f;
    bool chasingPlayer = false, attackingPlayer = false, idle = false, dead = false;
    public float lightAttackChance = 0.33f, heavyAttackChance = 0.33f, sweepAttackChance = 0.33f;
    private float rotationTimerCurrent, rotationTimer = 0.5f;
    private float actualLightAtkChance, actualHeavyAtkChance, actualSweepAtkChance;
    private GenericEnemyStats stats;
    private WeaponHitList list;
    // Use this for initialization
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        list = GetComponentInChildren<WeaponHitList>();
        startPos = transform.position;
        playerDetectionRange = playerDetectionRangeIdle;
        //GetComponent<RelicChecklist>().HasCollected(RelicChecklist.Relics)
        if (!playerObject)
        {
            playerObject = GameObject.Find("RemadePlayer");
        }
        actualLightAtkChance = lightAttackChance;
        actualHeavyAtkChance = heavyAttackChance;
        actualSweepAtkChance = sweepAttackChance;
        stats = GetComponent<GenericEnemyStats>();
    }
    // Update is called once per frame
    void Update()
    {
        if (stats && stats.health <= 0)
        {
            dead = true;
        }
        if (agent == null || dead == true || IsInBlockingAnimation())
            return;
        animator.SetBool("RotatingLeft", false);
        animator.SetBool("RotatingRight", false);
        if (playerObject && !attackingPlayer)
        {
            float distance = Vector3.Distance(transform.position, playerObject.transform.position);
            if (distance < playerDetectionRange)
            {
                playerDetectionRange = playerDetectionRangeChasing;
                currentIdleTime = 0.0f;
                chasingPlayer = true;
                targetRotation = Quaternion.LookRotation(playerObject.transform.position - transform.position);

                if (distance < agent.stoppingDistance)
                {
                    if (Vector3.Angle(transform.position - playerObject.transform.position, transform.forward) > 180 - (viableAttackCone / 2))
                    {
                        ResetAnimatorBools();
                        StartRandomAttack();
                        attackingPlayer = true;
                        currentIdleBehaviorTime = 0.0f;
                    }
                    else
                    {
                        if (!attackingPlayer)
                        {
                            rotationTimerCurrent -= Time.deltaTime;
                            if (rotationTimerCurrent <= 0.0f)
                            {
                                //Now we need to find out if we are going to rotate right of left ... lets do this
                                float angle = Quaternion.Angle(transform.rotation, targetRotation);
                                Quaternion checker = transform.rotation * Quaternion.AngleAxis(2.0f, new Vector3(0, 1, 0)); //And the award for best solution of the year goes to...
                                if (Quaternion.Angle(checker, targetRotation) > angle)
                                {
                                    animator.SetBool("RotatingLeft", true);
                                }
                                else
                                {
                                    animator.SetBool("RotatingRight", true);
                                }
                                transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, rotationSpeed * Time.deltaTime);
                                if (Quaternion.Angle(transform.rotation, targetRotation) < 3.0f || Quaternion.Angle(transform.rotation, playerObject.transform.rotation) < 3.0f)
                                {
                                    rotationTimerCurrent = rotationTimer;
                                    animator.SetBool("RotatingLeft", false);
                                    animator.SetBool("RotatingRight", false);
                                }
                            }
                            //transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, rotationSpeed * Time.deltaTime * 2);
                            ResetAnimatorBools();
                            //ExtraAttackCheck();
                        }
                    }
                }
                else
                {
                    agent.SetDestination(playerObject.transform.position);
                    agent.stoppingDistance = stoppingRangeWhenChasing;
                    ResetAnimatorBools();
                    transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, rotationSpeed * Time.deltaTime);
                }
            }
            else if (!idle)
            {

                ResetAnimatorBools();
                if (!chasingPlayer)
                {
                    agent.SetDestination(startPos);
                    agent.stoppingDistance = stoppingRangeWhenReturning;
                    playerDetectionRange = playerDetectionRangeIdle;
                }
                else
                {
                    agent.velocity *= 0.99f;
                    currentIdleTime += Time.deltaTime;
                    if (currentIdleTime > idleTimeBeforeReset)
                    {
                        chasingPlayer = false;
                    }
                }
            }
        }
        else if (attackingPlayer)
        {
            //if (animator.GetCurrentAnimatorStateInfo(0).normalizedTime < 0.15f || idle)
            //    transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, rotationSpeed * Time.deltaTime * 0.05f);
            currentIdleBehaviorTime += Time.deltaTime;
            if (currentIdleBehaviorTime > idletimeBeforeBehaviorResumes)
            {
                attackingPlayer = false;
                idle = false;
            }
        }
        animator.SetFloat("Speed", agent.velocity.magnitude / agent.speed);
    }
    void StartRandomAttack()
    {
        float result = Random.Range(0.0f, actualLightAtkChance + actualHeavyAtkChance + actualSweepAtkChance);
        if (result < actualLightAtkChance)
        {
            animator.SetBool("Attacking_Light", true);
            actualLightAtkChance = actualLightAtkChance > 1.0f ? lightAttackChance : actualLightAtkChance - 0.01f;
        }
        else if (result - actualLightAtkChance < actualHeavyAtkChance)
        {
            animator.SetBool("Attacking_Heavy", true);
            actualHeavyAtkChance = actualHeavyAtkChance > 1.0f ? heavyAttackChance : actualHeavyAtkChance - 0.01f;
        }
        else if (result - (actualLightAtkChance + actualHeavyAtkChance) < actualSweepAtkChance)
        {
            animator.SetBool("Attacking_Sweep", true);
            actualSweepAtkChance = actualSweepAtkChance > 1.0f ? sweepAttackChance : actualSweepAtkChance - 0.01f;
        }
        else
        {
            //When this happens the dude stands idle and does nothing, this should happen 2-4 times per minute
            idle = true;
            actualLightAtkChance = lightAttackChance;
            actualHeavyAtkChance = heavyAttackChance;
            actualSweepAtkChance = sweepAttackChance;
        }
        list.Clear();
    }
    bool ExtraAttackCheck()
    {
        if (Random.Range(0.0f, (actualLightAtkChance + actualHeavyAtkChance + actualSweepAtkChance) * 5) < actualHeavyAtkChance * 0.25f) //magic numbers to test/tweak the chance of this attack
        {
            animator.SetBool("Attacking_Heavy", true);
            actualHeavyAtkChance = actualHeavyAtkChance > heavyAttackChance ? heavyAttackChance : actualHeavyAtkChance - 0.01f;
            attackingPlayer = true;
            currentIdleBehaviorTime = 0.0f;
            list.Clear();
            return true;
        }
        else
        {
            actualHeavyAtkChance += 0.001f;
            return false;
        }
    }
    void ResetAnimatorBools()
    {
        animator.SetBool("Attacking_Light", false);
        animator.SetBool("Attacking_Heavy", false);
        animator.SetBool("Attacking_Sweep", false);
    }
    bool IsInBlockingAnimation()
    {
        if (animator.GetCurrentAnimatorStateInfo(0).IsName("SweepAttack") || animator.GetCurrentAnimatorStateInfo(0).IsName("LightAttack") || animator.GetCurrentAnimatorStateInfo(0).IsName("HeavyAttack"))
        {
            ResetAnimatorBools();
            return true;
        }
        return false;
    }
}
