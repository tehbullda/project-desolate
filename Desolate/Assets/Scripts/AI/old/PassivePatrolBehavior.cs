﻿using UnityEngine;
using System.Collections;
using System.Text.RegularExpressions;

public class PassivePatrolBehavior : AIBehaviorBase
{

    public GameObject patrolParent;
    public float stoppingDistancePatrolling;
    //[Tooltip("Requires Rigidbody to function")]
    public bool erraticMovement = false;
    //public Animator animator;
    NavMeshAgent agent;
    GameObject currentGoal;
    private Quaternion targetRotation;
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        //agent.stoppingDistance = stoppingDistancePatrolling;
        if (patrolParent.transform.childCount > 0)
        {
            currentGoal = patrolParent.transform.GetChild(0).gameObject;
            agent.SetDestination(currentGoal.transform.position);
        }
    }
    void Update()
    {
        Sense();
        Decide();
        Act();
    }

    protected override void Sense()
    {
        if (Vector3.Distance(transform.position, currentGoal.transform.position) < stoppingDistancePatrolling)
        {
            int currentIndex = System.Int32.Parse(Regex.Match(currentGoal.name, @"\d+").Value) - 1;
            currentGoal = (patrolParent.transform.childCount - 1) == currentIndex ? patrolParent.transform.GetChild(0).gameObject : patrolParent.transform.GetChild(++currentIndex).gameObject;
            agent.SetDestination(currentGoal.transform.position);
        }
    }
    protected override void Decide()
    {

    }
    protected override void Act()
    {
        if (erraticMovement)
        {
            targetRotation = transform.localRotation * Quaternion.Euler(new Vector3(0, Random.Range(-1.0f, 1.0f), 0));
            transform.localRotation = Quaternion.Lerp(transform.localRotation, targetRotation, 2.0f * Time.deltaTime);
        }
        //animator.SetFloat("Speed", agent.velocity.magnitude / agent.speed);
    }
}
