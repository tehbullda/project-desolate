﻿using UnityEngine;
using System.Collections;

public abstract class AIBehaviorBase : MonoBehaviour
{
    protected abstract void Sense();
    protected abstract void Decide();
    protected abstract void Act();
}
