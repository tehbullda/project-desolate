﻿using UnityEngine;
using System.Collections;

public class BossBehavior : MonoBehaviour
{
    private Material bossMat;
    private GameObject bossBar;
    public Animator animator;
    private GameObject playerObject;
    private UIBar HPBar;
    private float maxhealth, currenthealth;
    private GenericEnemyStats stats;
    private NavMeshAgent agent;
    private bool idle = false;
    private float currentIdleBehaviorTime = 0.0f;
    private float idletimeBeforeBehaviorResumes = 2.0f;
    //private float resetattackstimer = 1.0f; //Horrible solution, rewrite when time exists
    //private float currentresettimer = 0.0f; //Horrible solution, rewrite when time exists
    private float timeBeforeAoE = 14.0f;
    private float currentAoEtimer = 0.0f;
    [SerializeField]
    private AnimationCurve chanceCurve;
    private float viableAttackCone = 35.0f;
    private Quaternion targetRotation;
    private float rotationSpeed = 9.5f;
    private float stoppingRange = 3.25f;
    private Coroutine co;
    private bool coroutineRunning = false;
    [SerializeField]
    private Color original;
    [SerializeField]
    private float attackChanceLight = 0.33f;
    [SerializeField]
    private float attackChanceHeavy = 0.33f;
    [SerializeField]
    private float attackChanceSweep = 0.33f;
    [SerializeField]
    private BossAOE scriptAoE;
    private WeaponHitList list;
    private float rotationTimerCurrent, rotationTimer = 0.5f;
    // Use this for initialization
    void Start()
    {
        stats = GetComponent<GenericEnemyStats>();
        if (ProgressStatics.bossdefeated)
        {
            //stats.curr = -0.1f;
            Destroy(gameObject, 2.0f);
        }
        if (!ProgressStatics.bossEngaged)
            return; //Safeguard against scripts running in the wrong order
        agent = GetComponent<NavMeshAgent>();
        list = GetComponentInChildren<WeaponHitList>();
        if (!bossBar)
        {
            bossBar = GameObject.Find("Boss HPBar").transform.GetChild(0).gameObject;
        }
        HPBar = bossBar.GetComponent<UIBar>();
        bossBar.gameObject.SetActive(true);
        HPBar.SetPercentage(1.0f);
        maxhealth = currenthealth = stats.health;
        bossMat = GetComponentInChildren<Renderer>().material;
        bossMat.color = original;
        agent.stoppingDistance = stoppingRange;
    }
    IEnumerator FlashMaterial(Color color)
    {
        coroutineRunning = true;
        float elapsedtime = 0.0f;
        float totaltime = 1.0f;
        while (elapsedtime < totaltime)
        {
            elapsedtime += Time.deltaTime;
            bossMat.color = Color.Lerp(color, original, (elapsedtime / totaltime));
            yield return new WaitForEndOfFrame();
        }
        coroutineRunning = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (!ProgressStatics.bossEngaged)
            return;
        if (!stats || !HPBar || !bossMat)
        {
            Start();
        }
        animator.SetBool("RotatingLeft", false);
        animator.SetBool("RotatingRight", false);
        if (currenthealth != stats.health)
        {
            if (coroutineRunning)
            {
                StopCoroutine(co);
            }
            co = StartCoroutine(FlashMaterial(Color.red));
            currenthealth = stats.health;
            HPBar.SetPercentage(currenthealth / maxhealth);
        }
        if (stats.health <= 0.0f)
        {
            bossBar.gameObject.SetActive(false);
            ProgressStatics.bossdefeated = true;
            ProgressStatics.bossEngaged = false;
        }
        else
        {
            if (agent == null || playerObject == null || IsInBlockingAnimation())
                return;
            StopAttacks();
            if (playerObject && !idle)
            {
                targetRotation = Quaternion.LookRotation(playerObject.transform.position - transform.position);
                float distance = Vector2.Distance(new Vector2(transform.position.x, transform.position.z), new Vector2(playerObject.transform.position.x, playerObject.transform.position.z));
                if (distance < agent.stoppingDistance)
                {
                    if (CheckAoE())
                    {
                        StopAttacks(false);
                        return;
                    }
                    Vector2 differencewithoutY = new Vector2(transform.position.x - playerObject.transform.position.x, transform.position.z - playerObject.transform.position.z);
                    if (Vector2.Angle(differencewithoutY, new Vector2(transform.forward.x, transform.forward.z)) > 180 - (viableAttackCone / 2))
                    {
                        currentAoEtimer += Time.deltaTime * 0.5f; //Increase timer slower if facing the player
                        StopAttacks(false);
                        StartRandomAttack();
                    }
                    else
                    {
                        currentAoEtimer += Time.deltaTime; //Increase timer faster if not facing player
                        rotationTimerCurrent -= Time.deltaTime;
                        if (rotationTimerCurrent <= 0.0f)
                        {
                            float angle = Quaternion.Angle(transform.rotation, targetRotation);
                            Quaternion checker = transform.rotation * Quaternion.AngleAxis(2.0f, new Vector3(0, 1, 0)); //And the award for best solution of the year goes to...
                            if (Quaternion.Angle(checker, targetRotation) > angle)
                            {
                                animator.SetBool("RotatingLeft", true);
                            }
                            else
                            {
                                animator.SetBool("RotatingRight", true);
                            }
                            transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, rotationSpeed * Time.deltaTime);
                            if (Quaternion.Angle(transform.rotation, targetRotation) < 3.0f || Quaternion.Angle(transform.rotation, playerObject.transform.rotation) < 3.0f)
                            {
                                rotationTimerCurrent = rotationTimer;
                                animator.SetBool("RotatingLeft", false);
                                animator.SetBool("RotatingRight", false);
                            }
                        }
                        //animator.SetBool("Attacking_Light", false);
                        //animator.SetBool("Attacking_Heavy", false);
                        //animator.SetBool("Attacking_Sweep", false);
                        //animator.SetBool("Attacking_AoE", false); //Maybe this should happen here but i don't think it should
                        StopAttacks(false);
                    }
                }
                else
                {
                    //currentAoEtimer = 0.0f;
                    agent.SetDestination(playerObject.transform.position);
                    StopAttacks();
                    transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, rotationSpeed * Time.deltaTime);
                }
            }
            else if (idle)
            {
                StopAttacks();
                if (currentIdleBehaviorTime > 0.0f)
                {
                    currentIdleBehaviorTime -= Time.deltaTime;
                }
                else
                {
                    idle = false;
                }
            }
            //else if (idle)
            //{
            //    currentresettimer += Time.deltaTime;
            //    if (currentresettimer > resetattackstimer)
            //    {
            //        StopAttacks(); // Jesus christ this solution is horrible
            //    }
            //    targetRotation = Quaternion.LookRotation(playerObject.transform.position - transform.position);
            //    if (animator.GetCurrentAnimatorStateInfo(0).normalizedTime < 0.2f)
            //    {
            //        transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, rotationSpeed * Time.deltaTime);
            //    }
            //    float distance = Vector2.Distance(new Vector2(transform.position.x, transform.position.z), new Vector2(playerObject.transform.position.x, playerObject.transform.position.z));
            //    if (distance < agent.stoppingDistance && currentIdleBehaviorTime > 0.5f) // ugly fix for aoe chaining
            //        currentAoEtimer += Time.deltaTime;
            //    currentIdleBehaviorTime += Time.deltaTime;
            //    if (currentIdleBehaviorTime > idletimeBeforeBehaviorResumes)
            //    {
            //        idle = false;
            //    }
            //}
            animator.SetFloat("Speed", agent.velocity.magnitude / agent.speed);
        }
    }
    public void SetPlayer(GameObject player)
    {
        playerObject = player;
    }
    private void StartRandomAttack()
    {
        if (animator.GetBool("Attacking_AoE"))
            return;
        float random = Random.Range(0.0f, 1.0f);
        if (random < attackChanceLight)
        {
            animator.SetBool("Attacking_Light", true);
            currentIdleBehaviorTime = 1.0f;
        }
        else if (random - attackChanceLight < attackChanceHeavy)
        {
            animator.SetBool("Attacking_Heavy", true);
            currentIdleBehaviorTime = 0.5f;
        }
        else if (random - (attackChanceHeavy + attackChanceLight) < attackChanceSweep)
        {
            animator.SetBool("Attacking_Sweep", true);
            currentIdleBehaviorTime = 0.5f;
        }
        list.Clear();
        idle = true;
        //currentIdleBehaviorTime = 0.0f;
        //currentresettimer = 0.0f;
    }
    private void StopAttacks(bool AoEAswell = true)
    {
        animator.SetBool("Attacking_Light", false);
        animator.SetBool("Attacking_Heavy", false);
        animator.SetBool("Attacking_Sweep", false);
        if (AoEAswell)
            animator.SetBool("Attacking_AoE", false);
    }
    private bool CheckAoE()
    {
        if (Random.Range(0.1f, 1.0f) < chanceCurve.Evaluate(currentAoEtimer / timeBeforeAoE))
        {
            animator.SetBool("Attacking_AoE", true);
            if (scriptAoE)
            {
                scriptAoE.StartCoroutine(scriptAoE.AOEAttack());
                //Debug.Log("AOEEEEEE");
            }
            //idle = true;
            //currentIdleBehaviorTime = -2.0f;
            //currentresettimer = 0.0f;
            currentAoEtimer = 0.0f;
            list.Clear();
            return true;
        }
        return false;
    }
    private bool IsInBlockingAnimation()
    {
        if (animator.GetCurrentAnimatorStateInfo(0).IsName("LightAttack") || animator.GetCurrentAnimatorStateInfo(0).IsName("HeavyAttack") || animator.GetCurrentAnimatorStateInfo(0).IsName("SweepAttack") || animator.GetCurrentAnimatorStateInfo(0).IsName("AoEAttack"))
        {
            return true;
        }
        return false;
    }
}
