﻿using UnityEngine;
using System.Collections;

public static class EnemyHashIDs
{
	public static int idleState = Animator.StringToHash("Base Layer.Idle");
	public static int locomotionState = Animator.StringToHash("Base Layer.Locomotion");
	public static int rollingState = Animator.StringToHash("Base Layer.Roll");
	public static int lightAttackState = Animator.StringToHash("Base Layer.LightAttack");
	public static int heavyAttackState = Animator.StringToHash("Base Layer.HeavyAttack");
	public static int sweepAttackState = Animator.StringToHash("Base Layer.SweepAttack");
	public static int rotationLeftState = Animator.StringToHash("Base Layer.RotationLeft");
	public static int rotationRightState = Animator.StringToHash("Base Layer.RotationRight");
	public static int interactState = Animator.StringToHash("Base Layer.Interact");
	public static int speedFloat = Animator.StringToHash("Speed");
	public static int lightAttackTrigger = Animator.StringToHash("Attacking_1");
	public static int heavyAttackTrigger = Animator.StringToHash("Attacking_2");
	public static int sweepAttackTrigger = Animator.StringToHash("Attacking_3");
	public static int deadTrigger = Animator.StringToHash("Dead");
	public static int rotatingLeftBool = Animator.StringToHash("RotatingLeft");
	public static int rotatingRightBool = Animator.StringToHash("RotatingRight");
	public static int rollingTrigger = Animator.StringToHash("Roll");
	public static int idleBool = Animator.StringToHash("Idle");
	public static int strafeTrigger = Animator.StringToHash("Strafe");
	public static int healingTrigger = Animator.StringToHash("Heal");
	public static int staggeredTrigger = Animator.StringToHash("Staggered");

	public static int bossShieldingBool = Animator.StringToHash("Shielding");
}
