﻿using UnityEngine;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;
#endif
public class WaypointCreation : MonoBehaviour
{
    public GameObject waypointpoint;
    public Texture waypointbridge;
    private Vector3 originalPosition;
    private Quaternion originalRotation;
    [MenuItem("GameObject/WaypointCreate", false, 10)]
    static void Create()
    {
        GameObject go = Instantiate(Resources.Load<GameObject>("PatrolPath"));
		go.transform.position = Selection.activeTransform.position;
		//go.transform.parent = Selection.activeTransform;
		if (Selection.activeTransform.tag == "Enemy" && Selection.activeTransform.GetComponent<Patrol>())
			Selection.activeTransform.GetComponent<Patrol>().SetPatrolPathObj(go);
        go.name = "PatrolPath";   
    }
    // Use this for initialization
    void Start()
    {
        originalPosition = transform.position;
        originalRotation = transform.rotation;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = originalPosition;
        transform.rotation = originalRotation;
    }
    public void AddWaypoint(bool onObject = true, bool onLast = false)
    {
        GameObject temp = Instantiate(waypointpoint);
        temp.name = "Waypoint " + (transform.childCount+1);
        if (onObject)
            temp.transform.position = transform.position;
        if (onLast)
        {
            if (transform.childCount > 0)
                temp.transform.position = transform.GetChild(transform.childCount-1).position;
        }
        temp.transform.SetParent(transform);
    }
    public Object GetLastWaypointAdded()
    {
        return transform.GetChild(transform.childCount - 1);
    }
}
#if UNITY_EDITOR
[CustomEditor(typeof(WaypointCreation))]
public class Waypoints : Editor
{
    public int test;

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        WaypointCreation script = (WaypointCreation)target;
        if (GUILayout.Button("Add Waypoint on parent"))
        {
            script.AddWaypoint();
            //target = script.GetLastWaypointAdded();
        }
        if (GUILayout.Button("Add Waypoint on last child"))
        {
            script.AddWaypoint(false, true);
            //target = script.GetLastWaypointAdded();
        }
    }
}
#endif
