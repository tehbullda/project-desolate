﻿using UnityEngine;
using System.Collections;
using System;

public class LookForPlayer : Behavior
{
	[SerializeField]
	private LayerMask layerMask;
	[SerializeField, Tooltip("True = the enemy only 'sees' the player if he is within line of sight")]
	private bool lineOfSight = false;
	[SerializeField, Tooltip("Within this cone, detection range is full")]
	private int visionCone;
	[SerializeField, Tooltip("Full detection range")]
	private float detectionRange;
	[SerializeField, Tooltip("This is the range where the enemy notices the player no matter the rotations, lineofsight does not apply")]
	private float circularDetectionRange;
	[SerializeField, Tooltip("True = enemy hears if the player makes noise (ex. hits wall with sword). Not implemented")]
	private bool hearsPlayer = false;
	[SerializeField, Tooltip("detectionRange + extra for hearsPlayer to have a measurable effect")]
	private float extraDetectionRangeFromSound;
	private GameObject playerObject;
	private Agent agentScript;
	private Vector3 homePos;
	public override bool StartAI()
	{
		playerObject = GameObject.Find("RemadePlayer");
		agentScript = GetComponent<Agent>();
		homePos = transform.position;
		if (!playerObject)
		{
			Debug.Log("Unable to find player in " + this.name);
			return false;
		}
		NextBehavior = GetComponent<EngagePlayer>();
		if (!NextBehavior)
		{
			Debug.Log("No EngagePlayer-behavior found in " + this.name);
			return false;
		}
		return true;
	}
	public override AIResult UpdateAI()
	{
		if (playerObject)
		{
			Vector3 playerPos = playerObject.transform.position;
			playerPos.y = transform.position.y;
			float distance = Vector3.Distance(transform.position, playerObject.transform.position);
			if (distance < detectionRange)
			{
				if (distance < circularDetectionRange)
				{
					return AIResult.Pause;
				}
				if (!lineOfSight)
				{
					if (Vector3.Angle(transform.forward, playerObject.transform.position - transform.position) < (visionCone / 2.0f))
						return AIResult.Pause;
				}
				else
				{
					RaycastHit hit;
					if (Physics.Raycast(transform.position, playerObject.transform.position - transform.position, out hit, distance + 1.0f))
					{
						if (hit.collider.tag == "Player")
						{
							if (Vector3.Angle(transform.forward, playerObject.transform.position - transform.position) < (visionCone / 2.0f))
								return AIResult.Pause;
						}
					}
				}
			}
		}
		else
		{
			return AIResult.Stop;
		}
		return AIResult.Continue;
	}
	public override bool ResumeAI()
	{
		agentScript.QueuePosition(homePos, Priority);
		agentScript.SetTargetRotation(homePos - transform.position);
		return true;
	}
	public override bool IsReady()
	{
		return playerObject != null;
	}
}
