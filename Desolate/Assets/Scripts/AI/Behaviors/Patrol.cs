﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using AI;

public class Patrol : Behavior
{
	[SerializeField]
	private GameObject patrolPathParent = null;
	[SerializeField]
	private float patrolMoveSpeed;
	[SerializeField, Tooltip("True = it checks every waypoints' tasklist when idle and tries to perform the entire list during its downtime.")]
	private bool handlesTasklists = false;
	private WaypointScript.Waypoint[] waypoints;
	private List<Task.TaskInfo> tasks = new List<Task.TaskInfo>();
	private float pauseTime;
	private int currentWaypointIndex;
	private Agent agentScript;
	private bool waiting = false;
	public override bool StartAI()
	{
		agentScript = GetComponent<Agent>();
		if (!patrolPathParent)
		{
			//Debug.Log("Patrol path parent not set on " + this.ToString() + ".");
			return false;
		}
		waypoints = new WaypointScript.Waypoint[patrolPathParent.transform.childCount];
		for (int i = 0; i < waypoints.Length; ++i)
		{
			waypoints[i] = patrolPathParent.transform.GetChild(i).GetComponent<WaypointScript>().GetWaypointData();
		}
		currentWaypointIndex = 0;
		pauseTime = -0.01f; //Fucking floats
		return true;
	}
	public override AIResult UpdateAI()
	{
		if (waypoints.Length == 0)
		{
			return AIResult.Stop;
		}
		pauseTime -= Time.deltaTime;
		float distance = Vector3.Distance(transform.position, waypoints[currentWaypointIndex].position);
		if (distance < waypoints[currentWaypointIndex].closeEnoughDistance)
		{
			pauseTime = waypoints[currentWaypointIndex].agentPauseTime;
			waiting = false;
            if (handlesTasklists && waypoints[currentWaypointIndex].taskList.Length > 0)
			{
				tasks.Clear();
				tasks.AddRange(waypoints[currentWaypointIndex].taskList);
			}
			currentWaypointIndex = waypoints.Length <= currentWaypointIndex + 1 ? 0 : currentWaypointIndex + 1;
		}
		if (tasks.Count > 0 && pauseTime > 0.0f)
		{
			if (HandleTask(tasks[0]))
			{
				tasks.RemoveAt(0);
			}
		}
		if (pauseTime <= 0.0f && !waiting)
		{
			waiting = true;
            agentScript.SetAgentMovespeed(waypoints[currentWaypointIndex].moveSpeed);
			agentScript.QueuePosition(waypoints[currentWaypointIndex].position, Priority);
		}
		return AIResult.Continue;
	}
	public override bool ResumeAI()
	{
		agentScript.QueuePosition(waypoints[currentWaypointIndex].position, Priority);
		agentScript.SetAgentMovespeed(waypoints[currentWaypointIndex].moveSpeed);
		tasks.Clear();
		return true;
	}
	public override bool IsReady()
	{
		return patrolPathParent != null ? true : false;
	}
	public uint GetPriority()
	{
		return priority;
	}
	public void SetPatrolPathObj(GameObject obj)
	{
		patrolPathParent = obj;
	}
	private bool HandleTask(AI.Task.TaskInfo task)
	{
		switch (task.taskType)
		{
			case Task.TaskType.TurnToFace:
				if (task.facingObject)
					agentScript.SetTargetRotation(task.facingObject.transform.position - transform.position);
				else
					agentScript.SetTargetRotation(task.faceDir);
				break;
			case Task.TaskType.PlayAnim:
				agentScript.QueueAnimation(task.anim, Priority);
				break;
			case Task.TaskType.Idle:
				break;
		}
		return true;
	}
}
