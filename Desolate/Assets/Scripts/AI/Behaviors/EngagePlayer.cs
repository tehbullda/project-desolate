﻿using UnityEngine;
using System.Collections;
using System;
[RequireComponent(typeof(LookForPlayer))]
public class EngagePlayer : Behavior
{
	private enum Action
	{
		Idling,
		Chasing,
		Attacking,
		Rotating,
		Strafing,
		Healing,
		Returning,
		None
	}
	private Action currentAction = Action.None;
	private GameObject playerObject;
	[SerializeField]
	private float maxDistanceFromHome = 25.0f;
	[SerializeField]
	private float maxDistanceFromPlayer = 15.0f;
	[SerializeField]
	private float meleeDistance = 2.0f;
	[SerializeField]
	private float closeEnoughRotDegreesMelee = 15.0f;
	[SerializeField]
	private float closeEnoughRotDegreesFollow = 25.0f;
	private Vector3 homePos;
	private Agent agentScript;
	private Vector3 targetPos;
	private EnemyAnimationHandler animationHandler;
	[SerializeField]
	private float lightAttackChance = 0.33f, heavyAttackChance = 0.33f, sweepAttackChance = 0.33f;
	private float actualLightAtkChance, actualHeavyAtkChance, actualSweepAtkChance;
	private float currentIdleTime = 0.0f;
	[SerializeField]
	private bool lineOfSight = false, canUseHealing = false;
	private bool strafeOnce = false;
	private bool playerPosKnown = false;
	private bool returning = false;
	private bool actionHandled = false;
	[Header("Advanced Settings")]
	[SerializeField]
	private float clampedMinDetectionRange = 7.5f;
	[SerializeField]
	private float MaxDetectionRange = 15.0f, defaultDetectionRange = 7.5f, actionCooldown = 0.5f, idleTimeBetweenAttacks = 2.5f, idleTimeRandomEvent = 5.0f;
	[SerializeField]
	private int healingCount = 1;
	[SerializeField, Tooltip("When the player is within melee distance for this long, an attack will trigger no matter the rotation.")]
	private float timeBeforeForcedAttack = 6.0f;
	private float forcedAttackTimer = 0.0f;
	private float currentDetectionRange = 0.0f;
	private float distanceFromHome, distanceToTargetPos;
	[SerializeField, Range(0.0f, 1.0f)]
	private float strafeChance = 0.2f;
	public override bool StartAI()
	{
		playerObject = GameObject.Find("RemadePlayer");
		if (!playerObject)
		{
			return false;
		}
		agentScript = GetComponent<Agent>();
		homePos = transform.position;
		actualLightAtkChance = lightAttackChance;
		actualHeavyAtkChance = heavyAttackChance;
		actualSweepAtkChance = sweepAttackChance;
		animationHandler = GetComponent<EnemyAnimationHandler>();
		targetPos = playerObject.transform.position;
		forcedAttackTimer = 0.0f;
        return true;
	}

	public override AIResult UpdateAI()
	{
		currentIdleTime -= Time.deltaTime;
		UpdateDetectionRange();
		UpdateTargetPos();
		if (ReadyForNewAction())
		{
			if (returning)
			{
				return AIResult.Pause;
			}
			UpdateAction();
		}
		CheckStrafe();
		if (playerPosKnown && distanceToTargetPos < meleeDistance)
		{
			forcedAttackTimer += Time.deltaTime;
			if (forcedAttackTimer > timeBeforeForcedAttack)
			{
				currentAction = Action.Attacking;
				forcedAttackTimer = 0.0f;
				actionHandled = false;
			}
		}
		else
		{
			forcedAttackTimer = 0.0f;
		}
		agentScript.ForceSetDestination(targetPos);
		HandleAction();

		return AIResult.Continue;
	}
	private void UpdateDetectionRange()
	{
		distanceFromHome = Vector3.Distance(transform.position, homePos);
		currentDetectionRange = clampedMinDetectionRange + MaxDetectionRange * ((maxDistanceFromHome - distanceFromHome) / maxDistanceFromHome);
	}
	private void UpdateTargetPos()
	{
		if (lineOfSight)
		{
			RaycastHit hit;
			if (Physics.Raycast(transform.position, playerObject.transform.position - transform.position, out hit, currentDetectionRange))
			{
				if (hit.transform.tag == "Player")
				{
					playerPosKnown = true;
					returning = false;
					targetPos = playerObject.transform.position;
				}
				else
				{
					playerPosKnown = false;
				}
			}
			else
			{
				playerPosKnown = false;
			}
		}
		else
		{
			if (Vector3.Distance(transform.position, playerObject.transform.position) < currentDetectionRange)
			{
				playerPosKnown = true;
				targetPos = playerObject.transform.position;
				returning = false;
            }
			else
				playerPosKnown = false;
		}
		distanceToTargetPos = Vector3.Distance(transform.position, targetPos);
	}
	private bool ReadyForNewAction()
	{
		switch (currentAction)
		{
			case Action.Idling:
			case Action.None:
			case Action.Returning:
			default:
				return currentIdleTime <= 0.0f;
			case Action.Rotating:
			case Action.Chasing:
			case Action.Attacking:
			case Action.Healing:
			case Action.Strafing:
				return false;
		}
	}
	private void UpdateAction()
	{
		strafeOnce = true;
		if (distanceFromHome >= maxDistanceFromHome)
		{
			if (targetPos != default(Vector3))
			{
				//agentScript.QueuePosition(targetPos, priority);
				currentAction = Action.Returning;
				actionHandled = false;
            }
		}
		else
		{
			if (playerPosKnown && distanceToTargetPos <= meleeDistance)
			{
				currentAction = Action.Attacking;
				
            }
			else
			{
				//IF HEALTH IS LOW, CHANCE TO HEAL HERE PROBABLY if(canUseHealing && healingCount > 0) {currentAction = Action.Heal}
				currentAction = Action.Chasing;
			}
			actionHandled = false;
		}
		if ((currentAction == Action.Returning || currentAction == Action.Chasing || currentAction == Action.Idling) && !RotatedAppropriately(closeEnoughRotDegreesFollow))
		{
			currentAction = Action.Rotating;
			actionHandled = false;
		}
		if (currentAction == Action.Attacking && !RotatedAppropriately(closeEnoughRotDegreesMelee))
		{
			currentAction = Action.Rotating;
			actionHandled = false;
		}
	}
	private void CheckStrafe()
	{
		if (strafeOnce && playerPosKnown && distanceToTargetPos < meleeDistance*1.5f && currentAction == Action.None && UnityEngine.Random.Range(0.0f, 1.0f) < strafeChance)
		{
			currentAction = Action.Strafing;
			actionHandled = false;
			currentIdleTime += actionCooldown*2;
		}
		strafeOnce = false;
	}
	private void HandleAction()
	{
		if (actionHandled)
			return;
		currentIdleTime = 0.0f;
		switch (currentAction)
		{
			case Action.Idling:
				currentIdleTime = idleTimeRandomEvent;
				actionHandled = true;
				break;
			case Action.Chasing:
				agentScript.ForceSetDestination(targetPos, true);
				actionHandled = true;
				break;
			case Action.Attacking:
				TriggerRandomAttack();
				forcedAttackTimer = 0.0f;
				currentIdleTime = idleTimeBetweenAttacks;
				actionHandled = true;
				break;
			case Action.Rotating:
				agentScript.SetTargetRotation(targetPos - transform.position);
				actionHandled = true;
				break;
			case Action.Strafing:
				agentScript.QueueAnimation(EnemyAnimationHandler.AnimationState.Strafe, Priority);
				actionHandled = true;
				break;
			case Action.Healing:
				agentScript.QueueAnimation(EnemyAnimationHandler.AnimationState.Healing, Priority);
				healingCount -= 1;
				actionHandled = true;
				break;
			case Action.Returning:
				agentScript.QueuePosition(targetPos, Priority);
				//agentScript.SetTargetRotation(homePos - transform.position);
				returning = true;
				actionHandled = true;
				break;
			case Action.None:
				currentIdleTime = idleTimeBetweenAttacks;
				break;
		}
		currentIdleTime += actionCooldown;
		currentAction = actionHandled ? Action.None : currentAction;
	}
	private bool RotatedAppropriately(float closeEnough)
	{
		Vector3 playerDir = playerObject.transform.position - transform.position;
		return Vector3.Angle(new Vector3(transform.forward.x, 0, transform.forward.z), new Vector3(playerDir.x, 0, playerDir.z)) < closeEnough;
	}
	private void TriggerRandomAttack()
	{
		float result = UnityEngine.Random.Range(0.0f, actualLightAtkChance + actualHeavyAtkChance + actualSweepAtkChance);
		if (result < actualLightAtkChance)
		{
			agentScript.QueueAnimation(EnemyAnimationHandler.AnimationState.Combat_1, Priority);
			actualLightAtkChance = actualLightAtkChance > 1.0f ? lightAttackChance : actualLightAtkChance - 0.01f;
		}
		else if (result - actualLightAtkChance < actualHeavyAtkChance)
		{
			agentScript.QueueAnimation(EnemyAnimationHandler.AnimationState.Combat_2, Priority);
			actualHeavyAtkChance = actualHeavyAtkChance > 1.0f ? heavyAttackChance : actualHeavyAtkChance - 0.01f;
		}
		else if (result - (actualLightAtkChance + actualHeavyAtkChance) < actualSweepAtkChance)
		{
			agentScript.QueueAnimation(EnemyAnimationHandler.AnimationState.Combat_3, Priority);
			actualSweepAtkChance = actualSweepAtkChance > 1.0f ? sweepAttackChance : actualSweepAtkChance - 0.01f;
		}
		else
		{
			currentAction = Action.Idling;
			actualLightAtkChance = lightAttackChance;
			actualHeavyAtkChance = heavyAttackChance;
			actualSweepAtkChance = sweepAttackChance;
			return;
		}
	}
	public override bool ResumeAI()
	{
		currentAction = Action.None;
		targetPos = default(Vector3);
		currentIdleTime = 0.0f;
		forcedAttackTimer = 0.0f;
		actualLightAtkChance = lightAttackChance;
		actualHeavyAtkChance = heavyAttackChance;
		actualSweepAtkChance = sweepAttackChance;
		return true;
	}
	public override bool IsReady()
	{
		return playerObject != null;
	}
}
