﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
namespace BossGnaarg
{
	public class GnaargCommander : Behavior
	{
		private enum Action
		{
			Idling,
			Chasing,
			Attacking,
			Rotating,
			Strafing,
			Shielding,
			None
		}
		private Action currentAction = Action.None;
		[SerializeField]
		private UIBar bossBar;
		[SerializeField]
		private BaseStats stats;
		private int maxHealth, currenthealth;
		private GameObject playerObject;
		[SerializeField]
		private float meleeDistance = 2.0f;
		[SerializeField]
		private float closeEnoughRotDegreesMelee = 15.0f;
		[SerializeField]
		private float closeEnoughRotDegreesFollow = 25.0f;
		private Agent agentScript;
		private Vector3 targetPos;
		private EnemyAnimationHandler animationHandler;
		[SerializeField]
		private float lightAttackChance = 0.33f, heavyAttackChance = 0.33f, sweepAttackChance = 0.33f;
		private float actualLightAtkChance, actualHeavyAtkChance, actualSweepAtkChance;
		private float currentIdleTime = 0.0f;
		private bool strafeOnce = false, actionHandled = false;
		public bool shielding = false;
		[Header("Advanced Settings")]
		[SerializeField]
		private float actionCooldown = 0.5f;
		[SerializeField]
		private float idleTimeBetweenAttacks = 2.5f, idleTimeRandomEvent = 5.0f;
		[SerializeField]
		private float[] shieldHPtriggers;
		private bool queueShield = false;
		[SerializeField, Tooltip("When the player is within melee distance for this long, an attack will trigger no matter the rotation.")]
		private float timeBeforeForcedAttack = 6.0f;
		private float forcedAttackTimer = 0.0f;
		private float distanceToTargetPos;
		[SerializeField, Range(0.0f, 1.0f)]
		private float strafeChance = 0.2f;
		public override bool StartAI()
		{
			playerObject = GameObject.Find("RemadePlayer");
			if (!playerObject)
			{
				return false;
			}
			agentScript = GetComponent<Agent>();
			actualLightAtkChance = lightAttackChance;
			actualHeavyAtkChance = heavyAttackChance;
			actualSweepAtkChance = sweepAttackChance;
			animationHandler = GetComponent<EnemyAnimationHandler>();
			targetPos = playerObject.transform.position;
			forcedAttackTimer = 0.0f;
			bossBar = GameObject.Find("Boss HPBar").GetComponentInChildren<UIBar>(true);
			ScanData data;
			ScanDataManager.GetScanData(GetComponent<ScannableObject>().scanDataName, out data);

			bossBar.GetComponentInChildren<Text>().text = data.title;

			bossBar.gameObject.SetActive(true);
			bossBar.SetPercentage(1.0f);
			maxHealth = currenthealth = stats.health;
			return true;
		}

		public override AIResult UpdateAI()
		{
			if (shielding)
			{
				return AIResult.Continue;
			}
			currentIdleTime -= Time.deltaTime;
			UpdateTargetPos();
			if (ReadyForNewAction())
			{
				UpdateAction();
			}
			CheckStrafe();
			if (distanceToTargetPos < meleeDistance)
			{
				forcedAttackTimer += Time.deltaTime;
				if (forcedAttackTimer > timeBeforeForcedAttack)
				{
					currentAction = Action.Attacking;
					forcedAttackTimer = 0.0f;
					actionHandled = false;
				}
			}
			else
			{
				forcedAttackTimer = 0.0f;
			}
			agentScript.ForceSetDestination(targetPos);
			HandleAction();
			UpdateHealthbar();
			return AIResult.Continue;
		}
		private void UpdateTargetPos()
		{
			targetPos = playerObject.transform.position;
			targetPos.y = transform.position.y;
			distanceToTargetPos = Vector3.Distance(transform.position, targetPos);
		}
		private bool ReadyForNewAction()
		{
			switch (currentAction)
			{
				case Action.Idling:
				case Action.None:
				default:
					return currentIdleTime <= 0.0f;
				case Action.Rotating:
				case Action.Chasing:
				case Action.Attacking:
				case Action.Strafing:
				case Action.Shielding:
					return false;
			}
		}
		private void UpdateAction()
		{
			if (queueShield)
			{
				currentAction = Action.Shielding;
				actionHandled = false;
				return;
			}
			strafeOnce = true;
			if (distanceToTargetPos <= meleeDistance)
			{
				currentAction = Action.Attacking;

			}
			else
			{
				currentAction = Action.Chasing;
			}
			actionHandled = false;

			if ((currentAction == Action.Chasing || currentAction == Action.Idling) && !RotatedAppropriately(closeEnoughRotDegreesFollow))
			{
				currentAction = Action.Rotating;
				actionHandled = false;
			}
			if (currentAction == Action.Attacking && !RotatedAppropriately(closeEnoughRotDegreesMelee))
			{
				currentAction = Action.Rotating;
				actionHandled = false;
			}
		}
		private void CheckStrafe()
		{
			if (strafeOnce && distanceToTargetPos < meleeDistance * 1.5f && currentAction == Action.None && UnityEngine.Random.Range(0.0f, 1.0f) < strafeChance && animationHandler.IsIdle())
			{
				currentAction = Action.Strafing;
				actionHandled = false;
				currentIdleTime += actionCooldown * 2;
			}
			strafeOnce = false;
		}
		private void HandleAction()
		{
			if (actionHandled)
				return;
			currentIdleTime = 0.0f;
			switch (currentAction)
			{
				case Action.Idling:
					currentIdleTime = idleTimeRandomEvent;
					actionHandled = true;
					break;
				case Action.Chasing:
					agentScript.ForceSetDestination(targetPos, true);
					actionHandled = true;
					break;
				case Action.Attacking:
					TriggerRandomAttack();
					forcedAttackTimer = 0.0f;
					currentIdleTime = idleTimeBetweenAttacks;
					actionHandled = true;
					break;
				case Action.Rotating:
					agentScript.SetTargetRotation(targetPos - transform.position);
					actionHandled = true;
					break;
				case Action.Strafing:
					agentScript.QueueAnimation(EnemyAnimationHandler.AnimationState.Strafe, Priority);
					actionHandled = true;
					break;
				case Action.Shielding:
					animationHandler.SetTrigger(EnemyAnimationHandler.AnimationState.Boss_Shielding);
					actionHandled = true;
					shielding = true;
					agentScript.ForceClearDestination();
					break;
				case Action.None:
					currentIdleTime = idleTimeBetweenAttacks;
					break;
			}
			currentIdleTime += actionCooldown;
			currentAction = actionHandled ? Action.None : currentAction;
		}
		private void UpdateHealthbar()
		{
			bossBar.SetPercentage(stats.health / maxHealth);
		}
		private bool RotatedAppropriately(float closeEnough)
		{
			Vector3 playerDir = playerObject.transform.position - transform.position;
			return Vector3.Angle(new Vector3(transform.forward.x, 0, transform.forward.z), new Vector3(playerDir.x, 0, playerDir.z)) < closeEnough;
		}
		private void TriggerRandomAttack()
		{
			float result = UnityEngine.Random.Range(0.0f, actualLightAtkChance + actualHeavyAtkChance + actualSweepAtkChance);
			if (result < actualLightAtkChance)
			{
				agentScript.QueueAnimation(EnemyAnimationHandler.AnimationState.Combat_1, Priority);
				actualLightAtkChance = actualLightAtkChance > 1.0f ? lightAttackChance : actualLightAtkChance - 0.01f;
			}
			else if (result - actualLightAtkChance < actualHeavyAtkChance)
			{
				agentScript.QueueAnimation(EnemyAnimationHandler.AnimationState.Combat_2, Priority);
				actualHeavyAtkChance = actualHeavyAtkChance > 1.0f ? heavyAttackChance : actualHeavyAtkChance - 0.01f;
			}
			else if (result - (actualLightAtkChance + actualHeavyAtkChance) < actualSweepAtkChance)
			{
				agentScript.QueueAnimation(EnemyAnimationHandler.AnimationState.Combat_3, Priority);
				actualSweepAtkChance = actualSweepAtkChance > 1.0f ? sweepAttackChance : actualSweepAtkChance - 0.01f;
			}
			else
			{
				currentAction = Action.Idling;
				actualLightAtkChance = lightAttackChance;
				actualHeavyAtkChance = heavyAttackChance;
				actualSweepAtkChance = sweepAttackChance;
				return;
			}
		}
		public override bool IsReady()
		{
			return true;
		}
		public override bool ResumeAI()
		{
			return true;
		}
	}
}
