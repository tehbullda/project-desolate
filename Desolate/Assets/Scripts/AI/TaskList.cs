﻿using UnityEngine;
using System.Collections;
namespace AI
{
	public class Task
	{
		public enum TaskType
		{
			TurnToFace,
			PlayAnim,
			Idle,
		}
		[System.Serializable]
		public struct TaskInfo
		{
			public TaskType taskType;
			[Tooltip("Direction to face, if both this and facingObject is set, facingObject is prioritized.")]
			public Vector3 faceDir;
			[Tooltip("Direction to face, if both this and faceDir is set, this is prioritized.")]
			public GameObject facingObject;
			[Tooltip("Only used for the task Idle.")]
			public float duration;
			[Tooltip("Only used for the task PlayAnim.")]
			public EnemyAnimationHandler.AnimationState anim;
		}
	}
}