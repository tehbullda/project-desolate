﻿using UnityEngine;
using System.Collections;
[RequireComponent(typeof(Agent))]
public abstract class Behavior : MonoBehaviour
{
	/// <summary>
	/// Continue means the update was successful and it wants to run again.
	/// Pause means the behavior has finished and wants to be paused.
	/// Error means something went wrong and it should be removed from active.
	/// </summary>
	public enum AIResult
	{
		Continue,
		Pause,
		Stop
	}
	[SerializeField, Range(1, 10), Tooltip("Lower value means higher priority. Prio 1 is run first")]
	protected uint priority = 1;
	public uint Priority
	{
		get { return priority; }
		protected set { priority = (uint)Mathf.Clamp(value, 1, 10); }
	}
	[SerializeField, Tooltip("When a behavior doesn't return continue the agent checks if next is set and either adds next to active behaviors or attempts to unpause it if it exists.")]
	protected Behavior nextBehavior;
	public Behavior NextBehavior
	{
		get { return nextBehavior; }
		protected set { nextBehavior = value; }
	}
	[SerializeField, Tooltip("True = this is the only behavior that runs, all others are paused until it finishes.")]
	protected bool exclusive = false;
	public bool Exclusive
	{
		get { return exclusive; }
		protected set { exclusive = value; }
	}
	[SerializeField, Tooltip("Behavior is active on start, priority & exclusive tells which one is run first if several are active on start.")]
	protected bool activeOnStart = true;
	public bool ActiveOnStart
	{
		get { return activeOnStart; }
		protected set { activeOnStart = value; }
	}
	protected bool paused = false;
	public bool Paused
	{
		get { return paused; }
		set { paused = value; }
	}
	public abstract bool StartAI();
	public abstract AIResult UpdateAI();
	public abstract bool ResumeAI();
	public abstract bool IsReady();
}
