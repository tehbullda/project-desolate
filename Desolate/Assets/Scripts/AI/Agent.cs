﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
public class Agent : MonoBehaviour
{
	private struct PositionQueue
	{
		public PositionQueue(Vector3 pos, uint prio)
		{
			position = pos;
			priority = prio;
		}
		public Vector3 position;
		public uint priority;
	}
	private List<PositionQueue> queuedPositions = new List<PositionQueue>();
	private struct AnimationStateQueue
	{
		public AnimationStateQueue(EnemyAnimationHandler.AnimationState stt, uint prio)
		{
			state = stt;
			priority = prio;
		}
		public static bool operator ==(AnimationStateQueue a, AnimationStateQueue b)
		{
			return a.state == b.state && a.priority == b.priority;
		}
		public static bool operator !=(AnimationStateQueue a, AnimationStateQueue b)
		{
			return a.state != b.state || a.priority != b.priority;
		}
		public override bool Equals(object obj)
		{
			return base.Equals(obj);
		}
		public override int GetHashCode()
		{
			return base.GetHashCode();
		}
		public EnemyAnimationHandler.AnimationState state;
		public uint priority;
	}
	private List<Behavior> behaviorsLoaded = new List<Behavior>();
	private List<Behavior> behaviorsActive = new List<Behavior>();
	private EnemyAnimationHandler animationHandler;
	private NavMeshAgent navMeshAgent;
	private Quaternion targetRotation;
	[SerializeField]
	private float rotationSpeed = 4.0f;
	[SerializeField]
	private float closeEnoughRotation = 13.0f;
	private float defaultAgentSpeed;
	private AnimationStateQueue leftRotation = new AnimationStateQueue(EnemyAnimationHandler.AnimationState.RotateLeft, 1);
	private AnimationStateQueue rightRotation = new AnimationStateQueue(EnemyAnimationHandler.AnimationState.RotateRight, 1);
	private List<AnimationStateQueue> queuedAnimStates = new List<AnimationStateQueue>();
	private bool queuedRotation = false;
	void Start()
	{
		navMeshAgent = GetComponent<NavMeshAgent>();
		defaultAgentSpeed = navMeshAgent.speed;
		animationHandler = GetComponent<EnemyAnimationHandler>();
		List<Behavior> tmp = new List<Behavior>(GetComponents<Behavior>());
		for (int i = 0; i < tmp.Count; ++i)
		{
			InsertBehavior(behaviorsLoaded, tmp[i]);
		}
		if (behaviorsLoaded.Count == 0)
		{
			Debug.Log("No behaviors loaded on " + transform.name + ", shutting the agent down.");
			this.enabled = false;
			return;
		}
		for (int i = 0; i < behaviorsLoaded.Count; ++i)
		{
			if (behaviorsLoaded[i].StartAI())
			{
				behaviorsLoaded[i].Paused = !behaviorsLoaded[i].ActiveOnStart;
				behaviorsActive.Add(behaviorsLoaded[i]);
			}
		}
		if (behaviorsActive.Count == 0)
		{
			Debug.Log("No active behaviors on start.");
		}
		targetRotation = transform.rotation;
	}

	void Update()
	{
		if (behaviorsActive.Count == 0)
		{
			return;
		}
		Behavior.AIResult result;
		for (int i = 0; i < behaviorsActive.Count; ++i)
		{
			bool exclusive = behaviorsActive[i].Exclusive;
			if (behaviorsActive[i].Paused)
				continue;
			result = behaviorsActive[i].UpdateAI();
			if (result != Behavior.AIResult.Continue)
			{
				Behavior next = behaviorsActive[i].NextBehavior;
				if (result == Behavior.AIResult.Stop)
					behaviorsActive.RemoveAt(i);
				else
					behaviorsActive[i].Paused = true;
				if (next)
				{
					if (CheckDuplicate(behaviorsActive, next))
					{
						next.Paused = !next.ResumeAI();
					}
					else if (next.StartAI())
						InsertBehavior(behaviorsActive, next);
				}
			}
			if (exclusive)
				break;
		}
		CheckQueues();
		UpdateRotation();
	}

	private void CheckQueues()
	{
		if (ReadyForNewDestination() && queuedPositions.Count > 0)
		{
			navMeshAgent.SetDestination(queuedPositions[0].position);
			queuedPositions.RemoveAt(0);
		}
		if (queuedAnimStates.Count > 0)
		{
			if (!HasQueuedRotations() && animationHandler.IsIdle())
			{
				queuedRotation = false;
			}
			if (queuedAnimStates[0].state == EnemyAnimationHandler.AnimationState.RotateLeft || queuedAnimStates[0].state == EnemyAnimationHandler.AnimationState.RotateRight)
				if (!animationHandler.IsIdle() || !ConfirmNeedRotation())
				{
					RemoveQueuedRotationAnims();
					return;
				}
			if (animationHandler.IsIdle() && animationHandler.SetTrigger(queuedAnimStates[0].state))
				queuedAnimStates.RemoveAt(0);
			else
				animationHandler.SetRotationBoolsFalse();
		}
		else
			queuedRotation = false;
		RemoveQueuedRotationAnims();
	}
	private void UpdateRotation()
	{
		if (queuedRotation)
			return;
        if (animationHandler.IsIdle())
		{
			float angle = Quaternion.Angle(transform.rotation, targetRotation);
			if (angle > closeEnoughRotation)
			{
				Quaternion checker = transform.rotation * Quaternion.AngleAxis(2.0f, new Vector3(0, 1, 0));
				if (Quaternion.Angle(checker, targetRotation) > angle)
				{
					if (!queuedAnimStates.Contains(leftRotation))
						queuedAnimStates.Add(leftRotation);
				}
				else
				{
					if (!queuedAnimStates.Contains(rightRotation))
						queuedAnimStates.Add(rightRotation);
				}
				queuedRotation = true;
            }
			else
			{
				RemoveQueuedRotationAnims();
			}
		}
		if (animationHandler.IsRotating() || animationHandler.GetCurrentState() == EnemyAnimationHandler.AnimationState.Locomotion)
		{
			float speed = rotationSpeed;
			speed = animationHandler.GetCurrentState() == EnemyAnimationHandler.AnimationState.Locomotion ? speed * 0.5f : speed;
			transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, speed * Time.deltaTime);
			RemoveQueuedRotationAnims();
		}
	}
	public void SetAgentMovespeed(float newSpeed)
	{
		navMeshAgent.speed = newSpeed;
	}
	public void SetAgentDefaultMovespeed()
	{
		navMeshAgent.speed = defaultAgentSpeed;
	}
	/// <summary>
	/// Forces a new destination on the navmeshagent. The bool decides if it also clears the queued positions-list. Ignore bool if unsure.
	/// </summary>
	/// <param name="pos"></param>
	/// <param name="clearQueue"></param>
	public void ForceSetDestination(Vector3 pos, bool clearQueue = false)
	{
		navMeshAgent.SetDestination(pos);
		if (clearQueue)
			queuedPositions.Clear();
	}
	public void ForceClearDestination(bool clearQueue = false)
	{
		navMeshAgent.SetDestination(transform.position);
		if (clearQueue)
			queuedPositions.Clear();
	}
	public void QueuePosition(Vector3 newPos, uint priority)
	{
		queuedPositions.Add(new PositionQueue(newPos, priority));
		Sort(queuedPositions);
	}
	public void SetTargetRotation(Vector3 lookDirForward)
	{
		targetRotation = Quaternion.LookRotation(lookDirForward);
	}
	public void QueueAnimation(EnemyAnimationHandler.AnimationState state, uint priority)
	{
		if (!animationHandler.SetTrigger(state))
		{
			queuedAnimStates.Add(new AnimationStateQueue(state, priority));
			Sort(queuedAnimStates);
		}
	}
	private bool HasQueuedRotations()
	{
		for (int i = 0; i < queuedAnimStates.Count; ++i)
		{
			if (queuedAnimStates[i].state == EnemyAnimationHandler.AnimationState.RotateLeft || queuedAnimStates[i].state == EnemyAnimationHandler.AnimationState.RotateRight)
				return true;
		}
		return false;
	}
	private void RemoveQueuedRotationAnims()
	{
		for (int i = 0; i < queuedAnimStates.Count; ++i)
		{
			if (queuedAnimStates[i] == leftRotation)
			{
				queuedAnimStates.RemoveAt(i);
			}
			else if (queuedAnimStates[i] == rightRotation)
			{
				queuedAnimStates.RemoveAt(i);
			}
		}
	}
	private bool ConfirmNeedRotation()
	{
		return Quaternion.Angle(transform.rotation, targetRotation) > closeEnoughRotation;
	}
	private void ClearQueues()
	{
		queuedAnimStates.Clear();
		queuedPositions.Clear();
	}
	private void ResumeActiveBehaviors()
	{
		for (int i = 0; i < behaviorsActive.Count; ++i)
		{
			if (behaviorsActive[i].Paused)
			{
				if (!behaviorsActive[i].ResumeAI())
					behaviorsActive.RemoveAt(i);
				else
					behaviorsActive[i].Paused = false;
			}
		}
	}
	private bool ReadyForNewDestination()
	{
		return Quaternion.Angle(transform.rotation, targetRotation) <= closeEnoughRotation && !animationHandler.IsRotating() && !animationHandler.IsOnCooldown() && queuedAnimStates.Count == 0;
	}
	private void Sort(List<AnimationStateQueue> list)
	{
		list.Sort(delegate (AnimationStateQueue a, AnimationStateQueue b) { return a.priority.CompareTo(b.priority); });
	}
	private void Sort(List<PositionQueue> list)
	{
		list.Sort(delegate (PositionQueue a, PositionQueue b) { return a.priority.CompareTo(b.priority); });
	}
	private void InsertBehavior(List<Behavior> list, Behavior behavior)
	{
		if (list.Count == 0)
		{
			list.Add(behavior);
			return;
		}
		if (behavior.Exclusive)
		{
			for (int i = 0; i < list.Count; ++i)
			{
				if (!list[i].Exclusive)
				{
					list.Insert(i, behavior);
					return;
				}
				else
				{
					if (list[i].Priority < behavior.Priority)
					{
						continue;
					}
					else
					{
						list.Insert(i, behavior);
						return;
					}
				}
			}
		}
		else
		{
			for (int i = 0; i < list.Count; ++i)
			{
				if (list[i].Exclusive)
				{
					continue;
				}
				else
				{
					if (list[i].Priority < behavior.Priority)
					{
						continue;
					}
					else
					{
						list.Insert(i, behavior);
						return;
					}
				}
			}
		}
		list.Add(behavior);
	}
	private bool CheckDuplicate(List<Behavior> list, Behavior behavior)
	{
		return list.Contains(behavior);
	}
}
