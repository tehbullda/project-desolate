﻿using UnityEngine;
using System.Collections;

public class EnemyWeapon : MonoBehaviour
{

    public EnemyAttacks enemyAttacks;
    public float damage;
    private Renderer currentRenderer;
    private WeaponHitList list;
    // Use this for initialization
    void Start()
    {
        currentRenderer = GetComponent<Renderer>();
        list = GetComponent<WeaponHitList>();
    }

    // Update is called once per frame
    void Update()
    {
        if (enemyAttacks.IsAttacking())
            currentRenderer.material.color = Color.red;
        else
            currentRenderer.material.color = Color.white;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            if (enemyAttacks.IsAttacking() && list.CanHit(other.gameObject) && !other.GetComponent<RollInvincibility>().IsInvincible())
            {
                if (transform.tag == "BossWeapon")
                    AnalyticsSubmitter.UpdateVariableAdd(AnalyticsSubmitter.AnalyticsType.DamageTakenFromBoss, (int)damage);
                else
                    AnalyticsSubmitter.UpdateVariableAdd(AnalyticsSubmitter.AnalyticsType.DamageTaken, (int)damage);
                other.GetComponent<Health>().ReduceHealth(damage);
                CameraShake.StartShake();
                //TimeScale.PlayFreeze();
            }
        }
    }
}
