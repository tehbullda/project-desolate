﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(GenericEnemyDamageChecks))]

public class GenericEnemyStats : MonoBehaviour
{
    public ParticleSystem disolveParticles;
    public Animator animator;
    public float moveSpeed;
    public float health;
    public float stamina;
    public float armor;
    public float timeBeforeDestroy = 3.0f;

    private bool dead = false;

    public float timeInDeathSequence = 1.4f;
    public Transform weapon;

    public InWorldHPBar HPBar;

    private float maxHealth;
    // Use this for initialization
    void Start()
    {
        if(transform.FindChild("InWorldHPBar"))
        HPBar = transform.FindChild("InWorldHPBar").GetComponent<InWorldHPBar>();
        maxHealth = health;
    }

    // Update is called once per frame
    void Update()
    {
        CheckIfDead();
        if (HPBar)
            HPBar.SetPercentageFilled(Mathf.Clamp(health / maxHealth, 0.0f, 1.0f));
    }
    void CheckIfDead()
    {
        if (health <= 0.0f)
        {
            if (!dead)
            {
                AnalyticsSubmitter.UpdateVariableAdd(AnalyticsSubmitter.AnalyticsType.KillCount, 1);
                if (!animator || !disolveParticles)
                    DestroyObject(gameObject, 1);
                else
                    StartCoroutine(DeathSequence());
            }


        }
    }
    public bool IsDead()
    {
        return dead;
    }
    IEnumerator DeathSequence()
    {
        dead = true;
        if (weapon)
        {
            weapon.parent = null;
            weapon.gameObject.AddComponent<Rigidbody>();
            weapon.gameObject.GetComponent<BoxCollider>().isTrigger = false;
        }
        Destroy(GetComponent<Rigidbody>());
        GetComponent<CapsuleCollider>().enabled = false;

        animator.SetBool("Dead", true);
        disolveParticles.Play();
        yield return new WaitForSeconds(timeInDeathSequence);
        disolveParticles.Stop();
        animator.SetBool("Dead", false); //So it stops animating
        while (disolveParticles.particleCount > 0)
        {
            yield return null;
        }
        DestroyObject(gameObject);
    }

}
