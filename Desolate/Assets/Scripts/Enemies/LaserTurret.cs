﻿using UnityEngine;
using System.Collections;

public class LaserTurret : MonoBehaviour
{
    private GameObject player;
    private float visionRange = 7.0f;
    private float visionConeDegrees = 90.0f;

    private float rotationSpeed = 3.0f;

    Quaternion targetRotation;
    // Use this for initialization
    void Start()
    {
        player = GameObject.Find("RemadePlayer");
    
    }

    // Update is called once per frame
    void Update()
    {
        if (PlayerInRange() && PlayerInVisibleAngle())
            targetRotation.SetLookRotation(player.transform.position - transform.position);

        transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, rotationSpeed * Time.deltaTime);
    }
    bool PlayerInRange()
    {
        return (Vector3.Distance(transform.position, player.transform.position) < visionRange); 
    }
    bool PlayerInVisibleAngle()
    {
        return (Vector3.Angle(player.transform.position - transform.position, transform.forward) < visionConeDegrees / 2);
    }
}
