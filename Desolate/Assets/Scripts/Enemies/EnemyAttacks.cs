﻿using UnityEngine;
using System.Collections;

public class EnemyAttacks : MonoBehaviour {


    public Animator animator;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    bool IsInAnimation()
    {
        if (!animator)
            return false;
        if (animator.GetCurrentAnimatorStateInfo(0).IsName("LightAttack") || animator.GetCurrentAnimatorStateInfo(0).IsName("HeavyAttack") || animator.GetCurrentAnimatorStateInfo(0).IsName("SweepAttack"))
        {
            return true;
        }
        return false;
    }
    public bool IsAttacking()
    {
        if(IsInAnimation())
        {
            float normTime = animator.GetCurrentAnimatorStateInfo(0).normalizedTime;
            if (normTime > 0.2f && normTime < 0.6f)
                return true;
        }
        return false;
    }
}
