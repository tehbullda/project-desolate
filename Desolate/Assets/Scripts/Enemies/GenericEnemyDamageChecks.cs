﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(GenericEnemyStats))]

public class GenericEnemyDamageChecks : MonoBehaviour
{
    public Animator animator;
    GenericEnemyStats stats;
    [SerializeField]
    private ParticleSystem particles;

    // Use this for initialization
    void Start()
    {
        stats = GetComponent<GenericEnemyStats>();
    }

    // Update is called once per frame
    void Update()
    {
        AnimatorStateInfo nextState = animator.GetCurrentAnimatorStateInfo(0);
        if (nextState.IsName("Base Layer.Staggered"))
        {
            animator.SetBool("Staggered", false);
        }
    }
    //void OnTriggerEnter(Collider other)
    //{
    //    if (other.tag == "PlayerWeapon")
    //    {
    //        WeaponBaseScript weaponBaseScript = other.GetComponent<WeaponBaseScript>();
    //        if (weaponBaseScript.weaponHitDamage.IsAttacking())
    //        {
    //            if (!weaponBaseScript.hitList.CanHit(gameObject))
    //                return;
    //            Camera.main.GetComponent<CameraShake>().StartShake();
    //            AnalyticsSubmitter.UpdateVariableAdd(AnalyticsSubmitter.AnalyticsType.DamageDealt, (int)weaponBaseScript.GetDamage());
    //            stats.health -= weaponBaseScript.GetDamage();
    //            Debug.Log("Enemy " + transform.name + " took " + weaponBaseScript.GetDamage() + " damage");
    //            if (particles)
    //                particles.Play();
    //            if (transform.tag != "Boss")
    //                animator.SetBool("Staggered", true);
    //        }
    //    }
    //}
    //void OnTriggerEnter(Collider other)
    //{
    //    if (other.tag == "PlayerWeapon")
    //    {
    //        WeaponBaseScript weaponBaseScript = other.GetComponent<WeaponBaseScript>();
    //        if (weaponBaseScript.weaponHitDamage.IsAttacking())
    //        {
    //            if (!weaponBaseScript.hitList.CanHit(gameObject))
    //                return;
    //            CameraShake.StartShake();
    //            AnalyticsSubmitter.UpdateVariableAdd(AnalyticsSubmitter.AnalyticsType.DamageDealt, (int)weaponBaseScript.GetDamage());
    //            stats.health -= weaponBaseScript.GetDamage();
    //            Debug.Log("Enemy " + transform.name + " took " + weaponBaseScript.GetDamage() + " damage");
    //            if (particles)
    //                particles.Play();
    //            if (transform.tag != "Boss")
    //                animator.SetBool("Staggered", true);
    //        }
    //    }
    //}

}
