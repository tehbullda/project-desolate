﻿using UnityEngine;
using System.Collections;

public class RotationScript : MonoBehaviour {
    public float m_MoveSpeed;
    public Vector3 m_Rotation;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(m_Rotation * m_MoveSpeed * Time.deltaTime);
    }
    public void SetRotationSpeed(float p_MoveSpeed)
    {
        m_MoveSpeed = p_MoveSpeed;
    }
    public float GetRotationSpeed()
    {
        return m_MoveSpeed;
    }
}
