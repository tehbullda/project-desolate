﻿using UnityEngine;
using System.Collections;
using UnityEditor;

public class FaceTowardsCamera : MonoBehaviour {
	public bool lockX = false;
	public bool lockY = false;
	public bool lockZ = false;
	private Vector3 defaultRotation;
	// Use this for initialization
	void Start () {
		defaultRotation = transform.rotation.eulerAngles;
	}
	
	void Update () {
		transform.up = -Camera.main.transform.forward;
		Vector3 rotation = transform.rotation.eulerAngles;
		if (lockX)
			rotation.x = defaultRotation.x;
		if (lockY)
			rotation.y = defaultRotation.y;
		if (lockZ)
			rotation.z = defaultRotation.z;
		//transform.rotation = Quaternion.Euler(rotation);
	}
}

