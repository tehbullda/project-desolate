﻿using UnityEngine;
using System.Collections;

public class HealthRefill : MonoBehaviour {

    private float refillAmount = 30.0f;

    private float attractRadius = 10.0f;
    private float baseMoveSpeed = 2.0f;
    Transform player;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        player = CheckForPlayer();
        if (player)
        {
            transform.position = Vector3.MoveTowards(transform.position, player.position, GetMoveSpeed() * Time.deltaTime);
        }
	}
    Transform CheckForPlayer()
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, attractRadius);
        for(int i = 0; i < colliders.Length; ++i)
        {
            if(colliders[i].tag == "Player")
            {
                return colliders[i].transform;
            }
        }
        return null;
    }
    float GetMoveSpeed()
    {
        float distance = Vector3.Distance(transform.position, player.position);
        return baseMoveSpeed * (1.0f - Mathf.Clamp(distance / attractRadius, 0.2f, 1));
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            RefillHealing(other.GetComponent<Healing>());
            Destroy(gameObject);
        }
    }
    void RefillHealing(Healing healing)
    {
        healing.RefillHealing(refillAmount);
    }

    
}
