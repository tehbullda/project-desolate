﻿using UnityEngine;
using System.Collections;
using System;

public class Bonfire : MonoBehaviour, IInteractable {
	[Serializable]
	public struct BonfireInfo
	{
		public int bonfireID;
		public int sceneID;
		public Vector3 position;
	}

	[SerializeField] private int interactRadius;

	public int interactionRadius { get { return interactRadius; } }
	public GameObject gameObj { get { return gameObject; } }
	[HideInInspector] public BonfireInfo info;

	private static int numBonfiresInScene = 0;

	void Awake()
	{
		SphereCollider sp = gameObject.AddComponent<SphereCollider>();
		sp.radius = interactRadius / transform.localScale.y;
		sp.center = new Vector3(0, -0.5f, 0);
		sp.isTrigger = true;

		info.sceneID = gameObject.scene.buildIndex;
		info.position = transform.position;

		info.bonfireID |= (int)1 << (16 + info.sceneID);
		info.bonfireID |= (int)1 << (numBonfiresInScene);
		Debug.LogFormat("{0}", Convert.ToString(info.bonfireID, 2));

		++numBonfiresInScene;
	}

	void Start()
	{
		numBonfiresInScene = 0;		//I'm so proud of this. Or something. I don't even know.
	}

	void OnTriggerEnter(Collider p_other)
	{
		var interactionManager = p_other.GetComponent<InteractionManager>();
		if (interactionManager != null)
		{
			interactionManager.AddToList(this);
		}
	}

	void OnTriggerExit(Collider p_other)
	{
		var interactionManager = p_other.GetComponent<InteractionManager>();
		if (interactionManager != null)
		{
			interactionManager.RemoveFromList(this);
		}
	}

	public void Interact()
	{
		Debug.LogFormat("Interacting! ID: {0}", info.bonfireID);
	}

#if UNITY_EDITOR
	private MeshRenderer mr = null;
	private Vector3 spherePosition = new Vector3(0, 0, 0);

	void OnDrawGizmosSelected()
	{
		if (mr == null)
		{
			mr = GetComponent<MeshRenderer>();
		}

		spherePosition = transform.position;
		spherePosition.y = mr.bounds.min.y;
		Gizmos.DrawWireSphere(spherePosition, interactRadius);
	}
#endif
}
