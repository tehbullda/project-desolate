﻿using UnityEngine;
using System.Collections;

public interface IInteractable {
	int interactionRadius { get; }
	GameObject gameObj { get; }		//Workaround, to be able to get other components and stuff
	void Interact();
}
