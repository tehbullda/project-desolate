﻿using UnityEngine;
using System.Collections;

public class ProgressStatics : MonoBehaviour
{
    public static bool resetPlayer = false;
    public static bool resetEnemies = false;
    public static bool bossdefeated = false;
    public static bool gameComplete = false;
    public static bool bossEngaged = false;
    public static bool gamePaused = false;
    private bool oldbossdefeated = bossdefeated;
    private const string firstbossname = "boss 1";
    private const string gameCompleteString = "gameComplete";
    // Use this for initialization
    void Start()
    {
        //bossEngaged = false;
        //if (SaveManager.CheckTagExists(firstbossname))
        //{
        //    bossdefeated = SaveManager.ReadData<bool>(firstbossname);
        //}
        //else
        //{
        //    bossdefeated = false;
        //}
        //if (SaveManager.CheckTagExists(gameCompleteString))
        //{
        //    gameComplete = SaveManager.ReadData<bool>(gameCompleteString);
        //}
        //else
        //{
        //    gameComplete = false;
        //}
    }

    // Update is called once per frame
    void Update()
    {
        if (oldbossdefeated != bossdefeated)
        {
            //SaveManager.SaveData<bool>(bossdefeated, firstbossname);
            oldbossdefeated = bossdefeated;
        }
    }
}
