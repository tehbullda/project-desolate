﻿using UnityEngine;
using System.Collections;
using System;

public class ButtonCredits : ButtonBase {

    public CameraMover CameraScript;
    public Vector3 to;
    public GameObject ObjectToEnable;
    public override void Activate()
    {
        CameraScript.StartTransition(to, ObjectToEnable);
        transform.parent.parent.gameObject.SetActive(false);
    }
}
