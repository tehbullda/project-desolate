﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ButtonSlider : MonoBehaviour
{

    private Slider slider;
    private Text currentText;
    private int oldValue;
    bool active = false;
    // Use this for initialization
    void Start()
    {
        slider = GetComponentInChildren<Slider>();
        currentText = transform.GetChild(1).GetComponent<Text>();
        slider.value = slider.maxValue;
        //if (SaveManager.CheckOptionTagExists(gameObject.name))
        //{
        //    slider.value = SaveManager.ReadOption<int>(gameObject.name);
        //}
        oldValue = (int)slider.value;
        UpdateText();
    }
    void OnDisable()
    {
        //if (slider && oldValue != (int)slider.value)
        //    SaveManager.SaveOption<int>((int)slider.value, gameObject.name);
    }
    // Update is called once per frame
    void Update()
    {
        if (active)
        {
            if (Keybindings.GetBoolInput(Keybindings.BoolAction.Left))
            {
                slider.value = slider.value - 1 < slider.minValue ? 0 : slider.value - 1;
                UpdateText();
            }
            else if (Keybindings.GetBoolInput(Keybindings.BoolAction.Right))
            {
                slider.value = slider.value + 1 > slider.maxValue ? slider.maxValue : slider.value + 1;
                UpdateText();
            }
        }
    }
    //void UpdateSlider()
    //{
    //    slider.value = currentValue;
    //}
    public void UpdateText()
    {
        currentText.text = slider.value.ToString();
    }
    public void SetActive(bool state)
    {
        active = state;
    }
}
