﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonStart : ButtonBase {
    public override void Activate()
    {
		SceneManager.LoadScene(1);
    }
}
