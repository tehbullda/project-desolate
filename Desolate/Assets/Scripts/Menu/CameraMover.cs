﻿using UnityEngine;
using System.Collections;

public class CameraMover : MonoBehaviour {
    //Vector3 origPos;
	// Use this for initialization
	void Start () {
        //origPos = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    public void StartTransition(Vector3 to, GameObject CanvasToEnable)
    {
        StartCoroutine(Transition(to, CanvasToEnable));
    }
    public IEnumerator Transition(Vector3 to, GameObject CanvasToEnable)
    {
        float time = 0.5f;
        float current = 0.0f;
        while (current < time)
        {
            current += Time.deltaTime;
            transform.position = Vector3.Lerp(transform.position, to, current/time);
            yield return new WaitForEndOfFrame();
        }
        CanvasToEnable.SetActive(true);
    }
}
