﻿using UnityEngine;
using System.Collections;

public abstract class ButtonBase : MonoBehaviour {

    public abstract void Activate();
}
