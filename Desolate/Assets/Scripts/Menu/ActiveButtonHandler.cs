﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ActiveButtonHandler : MonoBehaviour
{

    public GameObject buttonparent;
    public int activeSize, inactiveSize;
    public float activeAlpha, inactiveAlpha;
    private Text[] texts;
    private int currentlyActiveIndex;
    private bool initiated = false;

    void Start()
    {
        currentlyActiveIndex = 0;
        texts = new Text[buttonparent.transform.childCount];
        for (int i = 0; i < texts.Length; ++i)
        {
            texts[i] = buttonparent.transform.GetChild(i).GetComponentInChildren<Text>();
        }
        initiated = true;
        UpdateButtons();
        
    }
    void OnEnable()
    {
        if (!initiated)
            return;
        currentlyActiveIndex = 0;
        UpdateButtons();
    }
    // Update is called once per frame
    void Update()
    {
        if (Keybindings.GetBoolInputDown(Keybindings.BoolAction.Interact))
        {
            Activate();
        }
        if (Keybindings.GetBoolInputDown(Keybindings.BoolAction.Down))
        {
            currentlyActiveIndex = currentlyActiveIndex < (texts.Length - 1) ? ++currentlyActiveIndex : 0;
            UpdateButtons();
        }
        else   if (Keybindings.GetBoolInputDown(Keybindings.BoolAction.Up))
            {
            currentlyActiveIndex = currentlyActiveIndex > 0 ? --currentlyActiveIndex : texts.Length - 1;
            UpdateButtons();
        }

    }

    void UpdateButtons()
    {
        Color newcolor = texts[0].color;
        for (int i = 0; i < texts.Length; ++i)
        {
            if (i == currentlyActiveIndex)
            {
                if (texts[i].transform.parent.GetComponent<ButtonSlider>())
                {
                    texts[i].transform.parent.GetComponent<ButtonSlider>().SetActive(true);
                }
                texts[i].fontSize = activeSize;
                newcolor.a = activeAlpha;
            }
            else
            {
                if (texts[i].transform.parent.GetComponent<ButtonSlider>())
                {
                    texts[i].transform.parent.GetComponent<ButtonSlider>().SetActive(false);
                }
                texts[i].fontSize = inactiveSize;
                newcolor.a = inactiveAlpha;
            }
            texts[i].color = newcolor;
        }
    }
    public void UpdateSelection(int index)
    {
        if (index < 0 || index > texts.Length)
            return;
        currentlyActiveIndex = index;
        UpdateButtons();
    }
    public void Activate()
    {
        if (texts[currentlyActiveIndex].transform.GetComponent<ButtonBase>())
            texts[currentlyActiveIndex].transform.GetComponent<ButtonBase>().Activate();
        if (texts[currentlyActiveIndex].transform.parent.GetComponent<ButtonBase>())
            texts[currentlyActiveIndex].transform.parent.GetComponent<ButtonBase>().Activate();
    }
}
