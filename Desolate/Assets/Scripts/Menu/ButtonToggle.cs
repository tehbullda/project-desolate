﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class ButtonToggle : ButtonBase
{

    private Text currentText;
    public string[] options;
    private int currentIndex, oldSaveIndex;
    void Start()
    {
        currentText = transform.GetChild(1).GetComponent<Text>();
        currentIndex = 0;
        //if (SaveManager.CheckOptionTagExists(gameObject.name))
        //{
        //    string current = SaveManager.ReadOption<string>(gameObject.name);
        //    for (int i = 0; i < options.Length; ++i)
        //    {
        //        if (options[i] == current)
        //        {
        //            oldSaveIndex = currentIndex = i;
        //            break;
        //        }
        //    }
        //}
        UpdateText();
    }
    void OnDisable()
    {
        //if (oldSaveIndex != currentIndex)
        //{
        //    SaveManager.SaveOption<string>(options[currentIndex], gameObject.name);
        //}
    }
    public override void Activate()
    {
        currentIndex = currentIndex + 1 >= options.Length ? 0 : currentIndex + 1;
        UpdateText();
    }
    private void UpdateText()
    {
        if (options.Length > 0)
            currentText.text = options[currentIndex];
    }
}
