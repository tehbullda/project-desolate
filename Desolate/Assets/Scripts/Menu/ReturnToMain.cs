﻿using UnityEngine;
using System.Collections;

public class ReturnToMain : MonoBehaviour
{

    public CameraMover CameraScript;
    private Vector3 origPos;
    public GameObject mainObj;
    // Use this for initialization
    void Start()
    {
        origPos = CameraScript.transform.position;
        gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (Keybindings.GetBoolInputDown(Keybindings.BoolAction.RollAndRun) || Input.GetKeyDown(KeyCode.Escape))
        {
            CameraScript.StartTransition(origPos, mainObj);
            gameObject.SetActive(false);
        }
    }
}
