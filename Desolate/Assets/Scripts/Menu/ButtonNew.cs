﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System;

public class ButtonNew : ButtonBase {
    public override void Activate()
    {
        if (System.IO.File.Exists(UnityEngine.Application.persistentDataPath + "/savefile.sav"))
            System.IO.File.Delete(UnityEngine.Application.persistentDataPath + "/savefile.sav");
		SceneManager.LoadScene(1);
    }
}
