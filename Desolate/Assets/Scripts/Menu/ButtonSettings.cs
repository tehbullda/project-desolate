﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class ButtonSettings : ButtonBase {
    public CameraMover CameraScript;
    public Vector3 to;
    public GameObject ObjectToEnable;
    public override void Activate()
    {
        CameraScript.StartTransition(to, ObjectToEnable);
        transform.parent.parent.gameObject.SetActive(false);
    }
}
