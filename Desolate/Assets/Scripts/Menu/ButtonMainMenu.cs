﻿using UnityEngine.SceneManagement;

public class ButtonMainMenu : ButtonBase {
    public override void Activate()
    {
		SceneManager.LoadScene(0);
    }
}
