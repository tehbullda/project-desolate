﻿using System;

public class Artifact_NoEffects : ArtifactBase {
	public override int ID {
		get {
			return 1;
		}
	}

	public override string ToString() {
		return "No effect";
	}
}