﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ProtoBuf;


public class TestArtifactSlotOne : ArtifactBase {
	public GameObject objectToSpawn;
	public const int numSpawns = 3;
	public override int ID {
		get {
			return 0;
		}
	}

	private List<GameObject> spawns = new List<GameObject>(numSpawns);

	// Use this for initialization
	void Awake() {
		slot = EquipSlot.SlotOne;
		objectToSpawn = (GameObject)Resources.Load(@"Upgrades/HPExpansion");

	}

	public override IEnumerator OnAttackEnd(AnimationEvent p_animEvent) {

		for (int i = 0; i < spawns.Count; ++i) {
			Destroy(spawns[i]);
		}
		
		yield return null;
	}

	public override IEnumerator OnAttackStart(AnimationEvent p_animEvent) {

		float animationDuration = p_animEvent.animatorClipInfo.clip.length;
		float durationBetweenSpawns = animationDuration / numSpawns;

		for (int i = 0; i < numSpawns; ++i) {
			GameObject temp = (GameObject)Instantiate(objectToSpawn, new Vector3(i, 1, 0), Quaternion.identity);
			spawns.Add(temp);

			yield return new WaitForSeconds(durationBetweenSpawns);
		}

	}

	public override string ToString() {
		return "[DEBUG] TestSlotOne";
	}
}
