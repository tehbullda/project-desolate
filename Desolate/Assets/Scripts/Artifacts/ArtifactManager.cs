﻿using UnityEngine;
using System;
using ProtoBuf;

[ProtoContract(AsReferenceDefault = true)]
public class ArtifactManager : MonoBehaviour {
	private Type[] availableArtifacts;
	[ProtoMember(1)] private ArtifactBase[] equippedArtifacts = new ArtifactBase[3];     //Can be changed later to allow for more equipped artifacts

	void Start() {
		availableArtifacts = TypeFinder.FindInheritingTypesSharingAssembly(typeof(ArtifactBase));

		for (int i = 0; i < equippedArtifacts.Length; ++i) {
			equippedArtifacts[i] = ScriptableObject.CreateInstance<Artifact_NoEffects>();
		}
	}

	public void OnEquip(int p_newArtifactIndex) {
		ArtifactBase temp = (ArtifactBase)System.Activator.CreateInstance(availableArtifacts[p_newArtifactIndex]);      //No clue if this'll work, but oh well
		equippedArtifacts[(int)temp.GetSlot()] = temp;

		Debug.Log(p_newArtifactIndex);
	}
	
	public void OnAttackStart(AnimationEvent p_animEvent) {
		for (int i = 0; i < equippedArtifacts.Length; ++i) {
			StartCoroutine(equippedArtifacts[i].OnAttackStart(p_animEvent));

		}
	}

	public void OnAttackEnd(AnimationEvent p_animEvent) {
		for (int i = 0; i < equippedArtifacts.Length; ++i) {
			StartCoroutine(equippedArtifacts[i].OnAttackEnd(p_animEvent));
		}
	}
}
