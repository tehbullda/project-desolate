﻿using System;
using System.Collections.Generic;
using System.Reflection;

public static class TypeFinder {
    private static Dictionary<Type, Type[]> foundTypes = new Dictionary<Type, Type[]>(32);

    public static Type[] FindInheritingTypesSharingAssembly(Type p_baseType) {
        if (foundTypes.ContainsKey(p_baseType)) {
            return foundTypes[p_baseType];
        }

        Assembly assembly = p_baseType.Assembly;
        Type[] typesInAssembly = assembly.GetTypes();
        List<Type> inheritingTypes = new List<Type>(typesInAssembly.Length);

        for (int i = 0; i < typesInAssembly.Length; ++i) {
            if (typesInAssembly[i].IsSubclassOf(p_baseType)
#if !UNITY_EDITOR
                && typesInAssembly[i].GetCustomAttributes(typeof(DebugAttribute), true).Length == 0
#endif
                ) {
                inheritingTypes.Add(typesInAssembly[i]);
            }
        }

        Type[] returnArray = inheritingTypes.ToArray();
        foundTypes.Add(p_baseType, returnArray);

        return returnArray;
    }
}
