﻿using UnityEngine;
using System.Collections;

[ProtoBuf.ProtoContract]
[ProtoBuf.ProtoInclude(1, typeof(Artifact_NoEffects))]
[ProtoBuf.ProtoInclude(2, typeof(TestArtifactSlotOne))]
public abstract class ArtifactBase : ScriptableObject {
	public enum EquipSlot {        //TODO: Make these have actual names, such as AttackSlot or DefenseSlot or whatever
		SlotOne,
		SlotTwo,
		SlotThree
	}

	protected EquipSlot slot;
	public abstract int ID { get; }

	public EquipSlot GetSlot() {
		return slot;
	}

	public virtual IEnumerator OnEquip() { yield return null; }
	public virtual IEnumerator OnUnequip() { yield return null; }

	public virtual IEnumerator OnAttackStart(AnimationEvent p_animEvent) { yield return null; }
	public virtual IEnumerator OnAttackEnd(AnimationEvent p_animEvent) { yield return null; }

	public virtual IEnumerator OnDamageDealt() { yield return null; }
	public virtual IEnumerator OnDamageTaken() { yield return null; }

	public override string ToString() {     //Used for tooltip text
		return "Artifact Base - This should never be shown!";
	}
}