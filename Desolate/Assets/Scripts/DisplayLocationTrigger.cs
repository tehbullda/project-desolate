﻿using UnityEngine;
using System.Collections;

public class DisplayLocationTrigger : MonoBehaviour {
	public string locationName;
	// Use this for initialization
	void Start () {
	
	}
	
void OnTriggerEnter(Collider other)
	{
		if (other.tag == "Player")
			LocationIndicator.Get().Display(locationName);
	}
}
