﻿using UnityEngine;
using System.Collections;

public class BossTrigger : MonoBehaviour
{

    public Agent bossAgentscript;
    //public GameObject[] objectsToEnable;
    public Component[] componentsToDisable;
    public Component[] componentsToEnable;
    private float distanceToBossOnEnter;
    public GameObject victoryScreen;
    private float timeToShowVictoryScreen = 5.0f;
    private float deadtime = 1.0f;
    // Use this for initialization
    void Start()
    {
        if (bossAgentscript && !ProgressStatics.bossdefeated)
        {
			bossAgentscript.enabled = false;
        }
        //for (int i = 0; i < objectsToEnable.Length; ++i)
        //{
        //    objectsToEnable[i].SetActive(false);
        //}
    }

    // Update is called once per frame
    void Update()
    {
        //if (bossAgentscript)
        //{
        //    if (bossAgentscript.GetComponent<GenericEnemyStats>().health <= 0.0f)
        //    {
        //        deadtime -= Time.deltaTime;
        //        if (deadtime <= 0.0f)
        //        {
        //            victoryScreen.SetActive(true);
        //        }
        //    }
        //}
        //if (victoryScreen && victoryScreen.activeSelf)
        //{
        //    if (timeToShowVictoryScreen <= 0.0f)
        //    {
        //        victoryScreen.SetActive(false);
        //    }
        //    else
        //    {
        //        timeToShowVictoryScreen -= Time.deltaTime;
        //    }
        //}
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            if (bossAgentscript)
            {
                distanceToBossOnEnter = Vector3.Distance(other.transform.position, bossAgentscript.transform.position);
            }
        }
    }
    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            if (distanceToBossOnEnter - 1.0f > Vector3.Distance(other.transform.position, bossAgentscript.transform.position))
            {
                transform.GetComponent<Collider>().isTrigger = false;
                TriggerActivations(other.gameObject);
            }
        }
    }
    void TriggerActivations(GameObject playerObj)
    {
        if (bossAgentscript)
        {
            ProgressStatics.bossEngaged = true;
			bossAgentscript.enabled = true;
        }
        MonoBehaviour[] scripts;
        MeshRenderer[] renderers;
        Light[] lights;
        for (int i = 0; i < componentsToDisable.Length; ++i)
        {
            scripts = componentsToDisable[i].gameObject.GetComponents<MonoBehaviour>();
            for (int j = 0; j < scripts.Length; ++j)
            {
                if (scripts[i].name == componentsToDisable[i].name)
                {
                    scripts[i].enabled = false;
                }
            }
            renderers = componentsToDisable[i].gameObject.GetComponents<MeshRenderer>();
            for (int j = 0; j < renderers.Length; ++j)
            {
                if (renderers[i].name == componentsToDisable[i].name)
                {
                    renderers[i].enabled = false;
                }
            }
            lights = componentsToDisable[i].gameObject.GetComponents<Light>();
            for (int j = 0; j < lights.Length; ++j)
            {
                if (lights[i].name == componentsToDisable[i].name)
                {
                    lights[i].enabled = false;
                }
            }
        }

        for (int i = 0; i < componentsToEnable.Length; ++i)
        {
            scripts = componentsToEnable[i].gameObject.GetComponents<MonoBehaviour>();
            for (int j = 0; j < scripts.Length; ++j)
            {
                if (scripts[j].name == componentsToEnable[i].name)
                {
                    scripts[j].enabled = true;
                }
            }
            renderers = componentsToEnable[i].gameObject.GetComponents<MeshRenderer>();
            for (int j = 0; j < renderers.Length; ++j)
            {
                if (renderers[j].name == componentsToEnable[i].name)
                {
                    renderers[j].enabled = true;
                }
            }
            lights = componentsToEnable[i].gameObject.GetComponents<Light>();
            for (int j = 0; j < lights.Length; ++j)
            {
                if (lights[j].name == componentsToEnable[i].name)
                {
                    lights[j].enabled = true;
                }
            }
        }
        //for (int i = 0; i < objectsToEnable.Length; ++i)
        //{
        //    objectsToEnable[i].SetActive(true);
        //}
    }
}
