﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class Messages : MonoBehaviour
{


    public struct Message
    {
        public string message;
        public Transform sender;
        public Transform reciever;

        public float timer;


        public Message(string message, Transform to, Transform from)
        {
            this.message = message;
            this.reciever = to;
            this.sender = from;
            this.timer = 0.0f;
        }
        public void TickTime(float deltatime)
        {
            timer += deltatime;
        }
    }
    public string defautMessage = "NoMessage";
    public float timeOutTime;
    public List<Message> messages;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        CheckTimeout();
    }
    public void SendMessage(string message, Transform to, Transform from)
    {
        Message mess = new Message(message, to, from);
        messages.Add(mess);
    }
    public Message CheckMessages(Transform reciever)
    {
        for(int i = 0; i < messages.Count; ++i)
        {
            if(reciever == messages[i].reciever)
            {
                //string temp = messages[i].message;
                RemoveMessage(i);
                return messages[i];
            }
        }
        return new Message(defautMessage, null, null);
    }
    private void RemoveMessage(int id)
    {
        messages.RemoveAt(id);
    }
    private void CheckTimeout()
    {
        for(int i = 0; i < messages.Count; ++i)
        {
            messages[i].TickTime(Time.deltaTime);
            if(messages[i].timer >= timeOutTime)
            {
                RemoveMessage(i);
            }
        }
    }
}
