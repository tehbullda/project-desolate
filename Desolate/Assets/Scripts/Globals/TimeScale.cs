﻿using UnityEngine;
using System.Collections;

public class TimeScale : MonoBehaviour
{
    private float freezeFrameTime = 0.25f;
    private float timer = 0.0f;

    private static float prevTimeSinceStart;
    private static float timeSinceStart;

    private static bool inFreeze = false;

    private static bool pausedGame = false;
    private static float currentTimeScale = 1.0f;
    // Use this for initialization
    void Start()
    {
        Time.timeScale = 1.0f;
    }

    // Update is called once per frame
    void Update()
    {
        prevTimeSinceStart = timeSinceStart;
        timeSinceStart = Time.realtimeSinceStartup;
        if (inFreeze && !pausedGame)
            UnFreeze();

    }
    public static void PlayFreeze()
    {
        inFreeze = true;
        Time.timeScale = 0.0f;
    }
    public static void SetTimeScale(float timeScale)
    {
        currentTimeScale = timeScale;
        Time.timeScale = currentTimeScale;
    }
    public static void ReturnToDefaultTimeScale()
    {
        currentTimeScale = 1.0f;
        Time.timeScale = currentTimeScale;
    }

    void UnFreeze()
    {
        timer += GetAbsoluteDeltaTime();
        if (timer >= freezeFrameTime)
        {
            timer = 0.0f;
            Time.timeScale = currentTimeScale;
            inFreeze = false;
        }
    }
    public static void PauseGame(bool paused)
    {
        pausedGame = paused;
        Time.timeScale = pausedGame ?  0.0f : currentTimeScale;
    }

    public static float GetAbsoluteDeltaTime()
    {
        return timeSinceStart - prevTimeSinceStart;
    }
}
