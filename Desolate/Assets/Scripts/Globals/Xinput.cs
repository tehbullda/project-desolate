﻿using UnityEngine;
using System.Collections;
using XInputDotNetPure;
using System.Text.RegularExpressions;


public class Xinput : MonoBehaviour
{

    private static XInputDotNetPure.GamePadState StateCurrent, StatePrevious;

    public enum ControllerButton
    {
        A,
        B,
        X,
        Y,
        Start,
        Back,
        RightBumber,
        LeftBumber,
        RightTrigger,
        LeftTrigger,
        RightStrick,
        LeftStick,
        DPadRight,
        DPadLeft,
        DPadUp,
        DPadDown
    }
    public enum ControllerAxis
    {
        LeftX,
        LeftY,
        RightX,
        RightY,
        LeftTrigger,
        RightTrigger
    }
    public enum ControllerSticks
    {
        Left,
        Right
    }

    private static float triggerDeadzone = 0.1f;

    XInputDotNetPure.PlayerIndex index;
    // Use this for initialization
    void Start()
    {
        for (PlayerIndex i = 0; i <= PlayerIndex.Four; ++i)
        {
            if (GamePad.GetState(i).IsConnected)
            {
                index = i;
                Debug.Log("Found Controller: " + i);
                break;
            }
        }
    }   // Update is called once per frame
    void Update()
    {
        StatePrevious = StateCurrent;
        StateCurrent = GamePad.GetState(index);

    }

    private static bool GetControllerButton(XInputDotNetPure.GamePadState state, ControllerButton button)
    {
        if (button == ControllerButton.A)
            return (state.Buttons.A == ButtonState.Pressed);
        if (button == ControllerButton.B)
            return (state.Buttons.B == ButtonState.Pressed);
        if (button == ControllerButton.X)
            return (state.Buttons.X == ButtonState.Pressed);
        if (button == ControllerButton.Y)
            return (state.Buttons.Y == ButtonState.Pressed);

        if (button == ControllerButton.Start)
            return (state.Buttons.Start == ButtonState.Pressed);
        if (button == ControllerButton.Back)
            return (state.Buttons.Back == ButtonState.Pressed);

        if (button == ControllerButton.RightBumber)
            return (state.Buttons.RightShoulder == ButtonState.Pressed);
        if (button == ControllerButton.LeftBumber)
            return (state.Buttons.LeftShoulder == ButtonState.Pressed);

        if (button == ControllerButton.RightTrigger)
            return (state.Triggers.Right >= triggerDeadzone);
        if (button == ControllerButton.LeftTrigger)
            return (state.Triggers.Left >= triggerDeadzone);

        if (button == ControllerButton.RightStrick)
            return (state.Buttons.RightStick == ButtonState.Pressed);
        if (button == ControllerButton.LeftStick)
            return (state.Buttons.LeftStick == ButtonState.Pressed);

        if (button == ControllerButton.DPadLeft)
            return (state.DPad.Left == ButtonState.Pressed);
        if (button == ControllerButton.DPadRight)
            return (state.DPad.Right == ButtonState.Pressed);
        if (button == ControllerButton.DPadUp)
            return (state.DPad.Up == ButtonState.Pressed);
        if (button == ControllerButton.DPadDown)
            return (state.DPad.Down == ButtonState.Pressed);
        return false;
    }
    public static bool GetButtonDown(ControllerButton button)
    {
        return (!GetControllerButton(StatePrevious, button) && GetControllerButton(StateCurrent, button));
    }
    public static bool GetButtonUp(ControllerButton button)
    {
        return (GetControllerButton(StatePrevious, button) && !GetControllerButton(StateCurrent, button));
    }
    public static bool GetButton(ControllerButton button)
    {
        return (GetControllerButton(StateCurrent, button));
    }
    public static float GetAxis(ControllerAxis axis)
    {
        switch (axis)
        {
            case ControllerAxis.LeftX:
                return StateCurrent.ThumbSticks.Left.X;
            case ControllerAxis.LeftY:
                return StateCurrent.ThumbSticks.Left.Y;
            case ControllerAxis.RightX:
                return StateCurrent.ThumbSticks.Right.X;
            case ControllerAxis.RightY:
                return StateCurrent.ThumbSticks.Right.Y;
            case ControllerAxis.LeftTrigger:
                return StateCurrent.Triggers.Left;
            case ControllerAxis.RightTrigger:
                return StateCurrent.Triggers.Right;
            default:
                return 0.0f;
        }
    }
    public static Vector2 GetStick(ControllerSticks stick)
    {
        if(stick == ControllerSticks.Left)
        return new Vector2(GetAxis(ControllerAxis.LeftX), GetAxis(ControllerAxis.LeftY));
        if (stick == ControllerSticks.Right)
            return new Vector2(GetAxis(ControllerAxis.RightX), GetAxis(ControllerAxis.RightY));

        Debug.Log("Input vector could not be created");
        return new Vector2();
    }


}
