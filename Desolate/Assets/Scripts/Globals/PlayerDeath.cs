﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class PlayerDeath : MonoBehaviour {

    public DeactivateGameplayHUD hideGameplayHud;
    private static bool playerDead;
    private static float deathSlowmotionTimescale = 0.3f;
	private Stats stats;
	private Transform player;
	private GameObject playerWeapon;
	// Use this for initialization
	void Start () {
        playerDead = false;
		stats = GameObject.Find("RemadePlayer").GetComponent<Stats>();
		player = GameObject.Find("RemadePlayer").transform;
		playerWeapon = player.GetComponent<Player>().PlayerWeapon;
				
	}
	
	// Update is called once per frame
	void Update () {
		if (stats.health.Current <= 0)
			SetPlayerDead(true);
		if (Input.GetKeyDown(KeyCode.Space))
			SetPlayerDead(true);
	}
    public void SetPlayerDead(bool dead)
    {
        if (!PlayerIsDead())
        {
			playerDead = dead;
			ActivateRagdoll();
            StartCoroutine(DeathCoroutine());

        }
    }
    public static bool PlayerIsDead()
    {
        return playerDead;
    }
    IEnumerator DeathCoroutine()
    {
        //Debug.Log("Death coroutine started");

        //TimeScale.SetTimeScale(deathSlowmotionTimescale);

        hideGameplayHud.SetActiveState(false);
        float time = 1.0f;
        AnalyticsSubmitter.UpdateVariableAdd(AnalyticsSubmitter.AnalyticsType.DeathCount, 1);
        GameObject.Find("BlackFadeScreen").GetComponent<FadeScript>().FadeElement(Color.black, time, 0);
        yield return new WaitForSeconds(time);
        ProgressStatics.bossEngaged = false;
        TimeScale.ReturnToDefaultTimeScale();
		SceneManager.LoadScene(0);
		SceneManager.LoadScene(1, LoadSceneMode.Additive);
        //Debug.Log("Death coroutine ended");
    }
	private void ActivateRagdoll()
	{
		MonoBehaviour[] components = GetComponents<MonoBehaviour>();
		for(int i = 0; i < components.Length; ++i)
		{
			components[i].enabled = false;
		}
		playerWeapon.transform.SetParent(null);
		playerWeapon.GetComponent<Rigidbody>().isKinematic = false;
		playerWeapon.GetComponent<BoxCollider>().isTrigger = false;
		player.FindChild("PlayerModel").GetComponent<Animator>().enabled = false;
		player.FindChild("PlayerModel").GetComponent<AnimationEventHandler>().enabled = false;
		player.FindChild("PlayerModel").transform.parent = null;

		player.GetComponent<RagdollManager>().enabled = true;
		player.GetComponent<RagdollManager>().SetActive(true);
	}
	

}
