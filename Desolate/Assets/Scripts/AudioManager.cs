﻿using UnityEngine;
using System.Collections;

public class AudioManager : MonoBehaviour {


    private AudioClip attackSwing;
    private AudioClip attackHit;

    public enum Sounds
    {
        attackSwing,
        attackHit
    }

	// Use this for initialization
	void Start () {
        attackHit = Resources.Load("Audio/Placeholders/attack_hit") as AudioClip;
	}

    // Update is called once per frame
    void Update()
    {

        //if (Input.GetKeyDown(KeyCode.U))
           // AudioSource.PlayClipAtPoint(attackHit, transform.position);
        
    }
    public void PlayClipAtPoint(Sounds sound, Vector3 pos)
    {
        switch(sound)
        {
            case Sounds.attackSwing:
                PlayClipAtPoint(attackSwing, pos);
                break;
            case Sounds.attackHit:
                PlayClipAtPoint(attackHit, pos);
                break;
        }
    }
    void PlayClipAtPoint(AudioClip clip, Vector3 pos)
    {
        AudioSource.PlayClipAtPoint(clip, transform.position);
    }
}
