﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TriggerArea : MonoBehaviour
{

    public string triggeringTag;
    public MonoBehaviour script;
    public string methodName;
    private GameObject interactObject;
    // Use this for initialization
    void Start()
    {
        interactObject = FindInteractObject();
    }
    GameObject FindInteractObject()
    {
        GameObject ret = GameObject.Find("UI");
        if (ret)
        {
            for (int i = 0; i < ret.transform.childCount; ++i)
            {
                if (ret.transform.GetChild(i).name == "Interact")
                {
                    ret = ret.transform.GetChild(i).gameObject;
                }
            }
        }
        return ret;
    }
    void Update()
    {

    }
    void OnTriggerStay(Collider other)
    {
        if (other.tag == triggeringTag)
        {
            if (interactObject && !interactObject.activeSelf)
            {
                interactObject.SetActive(true);
                interactObject.GetComponentInChildren<Text>().text = "Interact";
            }
            if (Xinput.GetButtonUp(Xinput.ControllerButton.A))
                script.SendMessage(methodName);
        }
    }
    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            if (interactObject && interactObject.activeSelf)
                interactObject.SetActive(false);
        }
    }
}
