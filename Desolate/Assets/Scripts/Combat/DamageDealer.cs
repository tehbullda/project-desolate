﻿using UnityEngine;
using System.Collections;
using System;

#if UNITY_EDITOR
using UnityEditor;
#endif

public interface IDamage
{
	int damageAmount { get; set; }
	DamageDealer.DamageElement element { get; set; }

	GameObject source { get; set; }
	GameObject destination { get; set; }

	void Effect(Stats p_stats);
}
public class DamageDealer : Vexe.Runtime.Types.BetterBehaviour
{
	public enum DamageElement : int
	{
		None = 0x00000000,
		Physical = 0x00000001,
		Fire = 0x00000010,
		Frost = 0x00000100,
		Poison = 0x00001000,
		Electric = 0x00010000
	}

	private new Collider collider;
	public bool usesExternalCollisionCheck = false;
	public IDamage damage;
	public GameObject source;
	[SerializeField]
	private WeaponHitList hitList;
	void Start()
	{
		collider = GetComponent<Collider>();
		//if (!hitList)
		//	hitList = GetComponent<WeaponHitList>();

#if UNITY_EDITOR
		if (collider == null)
		{
			UnityEngine.Object prefab = PrefabUtility.GetPrefabObject(this);
			string path = AssetDatabase.GetAssetPath(prefab);
			Debug.LogErrorFormat("IDamageDealer has no collider! Object: \"{0}\", prefab \"{1}\"", name, path);
		}
#endif
	}

	public void DealDamage(GameObject p_target)
	{
		DamageReceiver receiver = p_target.GetComponent<DamageReceiver>();
		if (receiver == null || !hitList.CanHit(p_target)) return;

		Type damageType = damage.GetType();
		IDamage damageObject = System.Activator.CreateInstance(damageType) as IDamage;

		damageObject.source = source;
		damageObject.destination = p_target;
		damageObject.damageAmount = damage.damageAmount;

		receiver.ReceiveDamage(damageObject);
	}

	public void DealDamage(IDamage p_damage)
	{
		DamageReceiver receiver = p_damage.destination.GetComponent<DamageReceiver>();
		if (receiver == null) return;

		receiver.ReceiveDamage(p_damage);
	}
	void OnCollisionEnter(Collision col)
	{
		if (!usesExternalCollisionCheck/* && hitList.CanHit(col.gameObject)*/)
			DealDamage(col.gameObject);
	}
	void OnTriggerEnter(Collider other)
	{
		if (!usesExternalCollisionCheck)
			DealDamage(other.gameObject);
	}
}

public class FrostDamage : IDamage
{
	public int damageAmount { get; set; }

	public DamageDealer.DamageElement element { get; set; } = DamageDealer.DamageElement.Frost;
	[HideInInspector]
	public GameObject source { get; set; }
	[HideInInspector]
	public GameObject destination { get; set; }

	public void Effect(Stats p_stats)
	{
		p_stats.health.Current -= damageAmount;
		Debug.Log("FrostDamage!");
	}
}

public class LightningDamage : IDamage
{
	public int damageAmount { get; set; }

	public DamageDealer.DamageElement element { get; set; } = DamageDealer.DamageElement.Electric;
	[HideInInspector]
	public GameObject source { get; set; }
	[HideInInspector]
	public GameObject destination { get; set; }

	public void Effect(Stats p_stats)
	{
		p_stats.health.Current -= damageAmount;
		Debug.Log(damageAmount + " LightningDamage from " + source);
	}
}
