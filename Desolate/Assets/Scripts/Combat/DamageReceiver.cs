﻿using UnityEngine;
using System.Collections;

public class DamageReceiver : Vexe.Runtime.Types.BetterBehaviour {
	public Stats stats;

	public void ReceiveDamage(IDamage p_damage)
	{
		p_damage.Effect(stats);
	}
}
