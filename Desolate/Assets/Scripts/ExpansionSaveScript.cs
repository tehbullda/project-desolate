﻿using UnityEngine;
using System.Collections;

public class ExpansionSaveScript : MonoBehaviour
{

    private enum expansionType
    {
        Health,
        Stamina,
    }
    private expansionType type;
    public int index;
    //private Health healthScript;
    //private Stamina staminaScript;
    private bool started = false;
    // Use this for initialization
    bool Start()
    {
        started = true;
        //GameObject tmp = GameObject.Find("RemadePlayer");
        //healthScript = tmp.GetComponent<Health>();
        //staminaScript = tmp.GetComponent<Stamina>();
        if (transform.name == "HealthExpansion")
        {
            type = expansionType.Health;
        }
        else
        {
            type = expansionType.Stamina;
        }
        //if (SaveManager.CheckTagExists(type.ToString() + index))
        //{
        //    if (SaveManager.ReadData<bool>(type.ToString() + index))
        //    {
        //        //if (type == expansionType.Health)
        //        //    healthScript.AddExpansion(true);
        //        //else
        //        //    staminaScript.AddExpansion(true);
        //        Destroy(transform.gameObject);
        //        //transform.gameObject.SetActive(false);
        //    }
        //}
        return true;
    }

    // Update is called once per frame
    void Update()
    {
        if (!started)
            Start();
    }
    public void Save()
    {
        //SaveManager.SaveData<bool>(true, type.ToString() + index);
    }
}
