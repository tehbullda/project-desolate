﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RagdollManager : MonoBehaviour
{
	private List<Rigidbody> rigidbodies = new List<Rigidbody>();
	public Transform addForceableObject;
	// Use this for initialization
	void Start()
	{
		List<Transform> allChildren = GetAllChildren();
		Rigidbody rb;
		Debug.Log(allChildren.Count);

		for (int i = 0; i < allChildren.Count; ++i)
		{
			if (HasRigidbody(allChildren[i], out rb) && allChildren[i].GetComponent<CharacterJoint>())
			{
				if (rb != null)
				{
					rigidbodies.Add(rb);
			
				}
			}
		}
		Debug.Log(rigidbodies.Count);
		SetActive(false);
	}
	void Update()
	{


	}
	public void SetActive(bool activeState)
	{
		for (int i = 0; i < rigidbodies.Count; ++i)
		{
			rigidbodies[i].isKinematic = !activeState;
		}
	}
	public void AddForce(Vector3 force)
	{
		addForceableObject.GetComponent<Rigidbody>().AddForce(force, ForceMode.Impulse);	
	}

	private List<Transform> GetAllChildren()
	{
		List<Transform> allChildren = new List<Transform>();
		Transform start = transform;
		AddChildrenToList(start, allChildren);
		return allChildren;
	}
	private void AddChildrenToList(Transform current, List<Transform> list)
	{
		for (int i = 0; i < current.childCount; ++i)
		{
			if (current.GetChild(i).childCount > 0)
			{
				AddChildrenToList(current.GetChild(i), list);
			}
			else
			{
				list.Add(current.GetChild(i));
			}
			list.Add(current);
		}
	}

	private bool HasRigidbody(Transform child, out Rigidbody rb)
	{
		if (child.GetComponent<Rigidbody>())
		{
			rb = child.GetComponent<Rigidbody>();
			return true;
		}
		rb = null;
		return false;
	}
}
