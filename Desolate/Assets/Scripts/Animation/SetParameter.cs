﻿using UnityEngine;

public class SetParameter : StateMachineBehaviour
{
	public enum Type
	{
		Bool,
		Integer,
		Float,
		Trigger
	}
	public Type type;

	public string parameter;
	public bool boolValue;
	public int integerValue;
	public float floatValue;

	int nameId;

	void OnEnable()
	{
		nameId = Animator.StringToHash(parameter);
	}

	public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	{
		switch (type)
		{
			case Type.Bool: animator.SetBool(nameId, boolValue); break;
			case Type.Integer: animator.SetInteger(nameId, integerValue); break;
			case Type.Float: animator.SetFloat(nameId, floatValue); break;
			case Type.Trigger: animator.SetTrigger(nameId); break;
		}
	}
}
