﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;

public class WorldLoadingManager : MonoBehaviour {
    private LinkedList<int> levelLoadHistory = new LinkedList<int>();
    private bool shouldUnloadOldest = false;

    public void LoadLevel(int p_levelID) {
		if (levelLoadHistory.Contains(p_levelID)) {
			levelLoadHistory.Remove(p_levelID);
			levelLoadHistory.AddFirst(p_levelID);
			return;
		}

		if (levelLoadHistory.Count >= 4) {
            shouldUnloadOldest = true;
        }

        StartCoroutine(LoadNew(p_levelID));

        levelLoadHistory.AddFirst(p_levelID);
    }

    void LateUpdate() {
        if (shouldUnloadOldest) {       //Prevents a crash, since I'm apparently not allowed to call UnloadLevel() from a trigger. Thanks Unity.
            shouldUnloadOldest = false;
            int oldestLevelID = levelLoadHistory.Last.Value;
			levelLoadHistory.RemoveLast();
            StartCoroutine(UnloadOldest(oldestLevelID));
        }
    }

    private IEnumerator UnloadOldest(int p_levelID) {
        while (!SceneManager.UnloadScene(p_levelID)) {
            yield return new WaitForEndOfFrame();
        }

        AsyncOperation op = Resources.UnloadUnusedAssets();

        while (!op.isDone) {
            yield return new WaitForEndOfFrame();
        }
    }

    private IEnumerator LoadNew(int p_levelID) {
        AsyncOperation op = SceneManager.LoadSceneAsync(p_levelID, LoadSceneMode.Additive);

        while (!op.isDone) {
            yield return new WaitForEndOfFrame();
        }
    }
}

