﻿using UnityEngine;
//using UnityEditor;
using System.Collections;

public class LoadAreaTrigger : MonoBehaviour {
    public int levelIndex;

    private WorldLoadingManager manager;

    void Awake() {
        manager = FindObjectOfType<WorldLoadingManager>();
    }

    void OnTriggerEnter(Collider p_other) {
        if (levelIndex <= 0) {			//We probably don't want to go to the start menu when loading a world part.
            Debug.LogError("LoadAreaTrigger - levelIndex is <= 0!");
            return;
        }
        else if (p_other.tag == "Player") {
            manager.LoadLevel(levelIndex);
        }
    }
}
