﻿using UnityEngine;
using System.Collections;

public class TestLaser : MonoBehaviour {

    LineRenderer line;
    public ParticleSystem particles;
    
    float offset = -0.2f;

    void Start () {
        line = gameObject.GetComponent<LineRenderer>();
        
	}
	
	// Update is called once per frame
	void Update () {
       // if (Input.GetKey(KeyCode.A))


        Ray ray = new Ray(transform.position, transform.forward);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, 1000))
        {
            line.enabled = true;
            line.SetPosition(0, ray.origin);
            line.SetPosition(1, hit.point);
            particles.transform.LookAt(transform.position);
            particles.transform.position = hit.point + (hit.point - transform.position).normalized * offset ;
            if(!particles.isPlaying)
            particles.Play();
        }
        else
        {
            line.enabled = false;
            if (particles.isPlaying)
                particles.Stop();
        }
	}

}
