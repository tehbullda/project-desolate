﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class InteractTrigger : MonoBehaviour
{

    [SerializeField]
    private string displayText = "Interact";
    [SerializeField]
    private string lockedText = "Key needed";
    [SerializeField]
    private int keyIndex;
    [SerializeField]
    private GameObject interactUI;
    [SerializeField]
    private bool lookForInteractObject = false;
    private Text text;
    public bool isLocked = false;
    public bool oneUse = false;
    public bool canUse = true;
    public bool active = true;

    // Use this for initialization
    void Start()
    {
        if (lookForInteractObject)
        {
            interactUI = FindInteractObject();
        }
        text = interactUI.transform.GetChild(0).GetComponent<Text>();
        //relicList = GameObject.FindObjectOfType<RelicChecklist>();
    }
    GameObject FindInteractObject()
    {
        GameObject ret = GameObject.Find("UI");
        if (ret)
        {
            for (int i = 0; i < ret.transform.childCount; ++i)
            {
                if (ret.transform.GetChild(i).name == "Interact")
                {
                    ret = ret.transform.GetChild(i).gameObject;
                }
            }
        }
        return ret;
    }
    void Update()
    {
        if (isLocked && canUse)
            if (RelicChecklist.HasCollected(RelicChecklist.Relics.Key, keyIndex))
            {
                //canUse = true;
                isLocked = false;
            }
    }
    public void Interact()
    {
    //    if (oneUse)
    //        canUse = false;
        active = !active;
        interactUI.SetActive(false);
    }
    void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player")
        {
            if (interactUI && !interactUI.activeSelf)
            {
                interactUI.SetActive(true);
                text.text = displayText;
                if (isLocked)
                    text.text = lockedText;
            }
            if (!isLocked)
                if (Keybindings.GetBoolInputDown(Keybindings.BoolAction.Interact))
                    Interact();
        }
    }
    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            if (interactUI && interactUI.activeSelf)
                interactUI.SetActive(false);
        }
    }
}
