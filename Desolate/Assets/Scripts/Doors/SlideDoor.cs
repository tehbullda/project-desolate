﻿using UnityEngine;
using System.Collections;
using ProtoBuf;

public class SlideDoor : MonoBehaviour
{

	[SerializeField]
	private InteractTrigger[] triggerScript = new InteractTrigger[0];
	public bool doorState = true;
	public Transform door;
	public Vector3 startPosition;
	public Vector3 endPosition;
	public float moveSpeed;

	private Vector3 targetPosition;

	bool doorSwitchingState = false;

	void Start()
	{
		SetDoorOpen(DoorStateManager.Get().GetDoorState(this));
	}
	
	void Update()
	{
#if UNITY_EDITOR
		if (Input.GetKeyDown(KeyCode.O))
			SetDoorOpen(!doorState);
#endif
		for (int i = 0; i < triggerScript.Length; ++i)
		{
			if (triggerScript[i].active && doorState == true)
			{
				SetDoorOpen(false);
				for (int j = 0; j < triggerScript.Length; ++j)
				{
					triggerScript[j].gameObject.SetActive(false);
				}
			}
		}
	}
	IEnumerator SwitchState()
	{

		//Debug.Log("Door Coroutine Started");
		doorSwitchingState = true;
		yield return new WaitForSeconds(0.5f);
		targetPosition = getTargetPosition(doorState);
		while (door.localPosition != targetPosition)
		{
			door.localPosition = Vector3.MoveTowards(door.localPosition, targetPosition, moveSpeed * Time.deltaTime);
			yield return new WaitForEndOfFrame();
		}
		doorSwitchingState = false;
		//Debug.Log("Door Coroutine Finished");

	}
	Vector3 getTargetPosition(bool state)
	{
		if (state)
			return startPosition;
		return endPosition;
	}
	public void SetDoorOpen(bool open)
	{
		if (!doorSwitchingState && doorState != open)
		{
			doorState = open;
			DoorStateManager.Get().SetDoorState(this);
			StartCoroutine(SwitchState());
		}
	}
}
