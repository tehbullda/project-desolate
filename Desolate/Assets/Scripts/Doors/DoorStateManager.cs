﻿using UnityEngine;
using System.Collections.Generic;
using ProtoBuf;

[ProtoContract]
public class DoorStateManager : MonoBehaviour
{
	private static DoorStateManager singleton = null;

	[ProtoMember(1)]
	public Dictionary<string, bool> trackedDoors = new Dictionary<string, bool>(32);

	public static DoorStateManager Get()
	{
		if (singleton == null)
			singleton = GameObject.FindObjectOfType<DoorStateManager>();

		return singleton;
	}

	public bool GetDoorState(SlideDoor p_door)
	{
		if (trackedDoors.ContainsKey(p_door.name))
			return trackedDoors[p_door.name];

		trackedDoors.Add(p_door.name, p_door.doorState);    //If the door doesn't exist in the save, add it with its default value.
		return p_door.doorState;
	}

	public void SetDoorState(SlideDoor p_door)
	{
		if (trackedDoors.ContainsKey(p_door.name))
			trackedDoors[p_door.name] = p_door.doorState;
		else
			trackedDoors.Add(p_door.name, p_door.doorState);
	}
}