﻿using UnityEngine;
using System.Collections;

public class AnimatedDoor : MonoBehaviour
{

	private Animation animation;
	private ScanTarget scanTarget;
	private bool doorOpen = false;

	private bool prevScanned = false;
	private bool isScanned = false;
	// Use this for initialization
	void Start()
	{
		scanTarget = GetComponent<ScannableObject>().scanTarget;
		animation = GetComponent<Animation>();
	}

	// Update is called once per frame
	void Update()
	{
		prevScanned = isScanned;
		isScanned = scanTarget == null;
		if (!prevScanned && isScanned)
			OpenDoor();
	}
	private void ToggleDoor()
	{
		if (animation.IsPlaying("Open"))
			return;
		if (doorOpen)
			CloseDoor();
		else
			OpenDoor();
	}
	private void OpenDoor()
	{
		doorOpen = true;
		animation["Open"].speed = 1.0f;
		animation["Open"].time = 0.0f;
		animation.Play();
	}
	private void CloseDoor()
	{
		doorOpen = false;
		animation["Open"].speed = -1.0f;
		animation["Open"].time = animation["Open"].length;
		animation.Play();
	}
}
