﻿using UnityEngine;
using System.Collections;

public class DestructableWall : MonoBehaviour {
	private const string saveName = "DestructableWall";
	private GameObject whole;
	private GameObject destroyed;
	public new ParticleSystem particleSystem;
	[Tooltip("Make sure this number is not like any other number from destructablewalls")]
	public int indexForSave;
	// Use this for initialization
	void Start () {
		whole = transform.FindChild("Whole").gameObject;
		destroyed = transform.FindChild("Destroyed").gameObject;
		//if (SaveManager.CheckTagExists(saveName + indexForSave))
		//{
		//	if (SaveManager.ReadData<bool>(saveName + indexForSave))
		//	{
		//		GetComponent<BoxCollider>().enabled = false;
		//		whole.SetActive(false);
		//		destroyed.SetActive(true);
		//	}
		//}
	}
	
	// Update is called once per frame
	void Update () {

	}
	private void Shatter()
	{
		CameraShake.StartShake();
		particleSystem.Play();
		GetComponent<BoxCollider>().enabled = false;
		whole.SetActive(false);
		destroyed.SetActive(true);
		//SaveManager.SaveData<bool>(true, saveName + indexForSave);
		
	}
	void OnTriggerEnter(Collider other)
	{
		Debug.Log("LABAN FIXA");
		if(other.tag == "PlayerWeapon")
		{
			//if ((other.GetComponent<WeaponBaseScript>().weaponHitDamage.IsAttacking()) && other.GetComponent<WeaponBaseScript>().owner.GetComponent<WeaponChargeAttack>().IsFullyCharged())
			//	Shatter();
		}
	}
}
