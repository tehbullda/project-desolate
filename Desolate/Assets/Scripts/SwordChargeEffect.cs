﻿using UnityEngine;
using System.Collections;

public class SwordChargeEffect : MonoBehaviour
{

	public ParticleSystem chargeParticles;
	public Light light;
	private float lightIntensity;
	private float chargeValue = 0.0f;
	public bool drawGizmos = true;

	private bool fadingDownLight = false;

	private bool fullCharge;
	private bool prevFullCharge;
	private Renderer renderer;
	private Color emissiveColor;

	// Use this for initialization
	void Start()
	{
		renderer = transform.GetChild(0).GetComponent<Renderer>();
		emissiveColor = renderer.material.GetColor("_EmissionColor");
		lightIntensity = light.intensity;
		SetEmissiveValue(0.0f);
        chargeParticles.Stop();
	}

	// Update is called once per frame
	void Update()
	{
		prevFullCharge = fullCharge;
		fullCharge = (chargeValue >= 1.0f);

		if (chargeValue > 0.0f)
		{
				chargeParticles.Play();
		}
		if(fullCharge)
			chargeParticles.Stop();

		if (prevFullCharge && !fullCharge)
			StartCoroutine(AfterEffect());
		if (!fadingDownLight)
		{
			light.intensity = lightIntensity * chargeValue;
			SetEmissiveValue(chargeValue);
		}

	}

	public void SetChargeValue(float amount)
	{
		chargeValue = Mathf.Clamp(amount, 0.0f, 1.0f);

	}
	private void SetEmissiveValue(float value)
	{
		emissiveColor.a = value;
		renderer.material.SetColor("_EmissionColor", emissiveColor);
	}
	private IEnumerator AfterEffect()
	{
		fadingDownLight = true;

		light.intensity = lightIntensity * 1.5f;
		yield return new WaitForSeconds(0.2f);
		float value = 1.0f;
		while (value > 0.0f)
		{

			value -= Time.deltaTime;
			light.intensity = value;
			SetEmissiveValue(value);
			yield return null;
		}
		fadingDownLight = false;
	}
}
