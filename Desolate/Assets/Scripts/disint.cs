﻿using UnityEngine;
using System.Collections;

public class disint : MonoBehaviour
{
    public GenericEnemyStats stats;
    public float blendvalue = 10.0f;
    private bool onoff = false;
    public float disintspeed = 4.0f;
    private float blendbackup;
    // Use this for initialization
    void Start()
    {
        blendbackup = blendvalue;
    }

    // Update is called once per frame
    void Update()
    {

        if (stats.IsDead())
        {
            onoff = true;
        }

        if (Input.GetKeyDown("l"))
        {
            blendvalue = blendbackup;
        }

        //if (blendvalue <= -1.0f)
        //{
        //    blendvalue = blendbackup;
        //}


        if (onoff)
        {
            blendvalue -= (Time.deltaTime * disintspeed);
        }
        this.GetComponent<Renderer>().material.SetFloat("_value", blendvalue);
    }
    
}
