﻿using UnityEngine;
using System.Collections;

public class KeepModelRotatedForward : MonoBehaviour {
	
	void Update () {
        transform.forward = transform.parent.forward;
	}
}
