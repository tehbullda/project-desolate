﻿using UnityEngine;
using System.Collections;

public class Pot : MonoBehaviour
{
	private GameObject healingRefillPrefab;
	public Transform whole;
	public Transform destroyed;

	private const float breakThreshold = 6.3f;
	// Use this for initialization
	void Start()
	{
		whole.gameObject.SetActive(true);
		destroyed.gameObject.SetActive(false);
	}

	// Update is called once per frame
	void Update()
	{
#if UNITY_EDITOR
		if (Input.GetKeyDown(KeyCode.K))
			DestroyPot();
#endif
	}
	void DestroyPot()
	{
		CameraShake.StartShake();
		destroyed.parent = null;
		whole.gameObject.SetActive(false);
		InheritVelocity(whole.GetComponent<Rigidbody>().velocity, new Vector3(3.4f, 3.4f, 3.4f));
		destroyed.gameObject.SetActive(true);
	}
	void OnCollisionEnter(Collision other)
	{

		CheckIfRoll(other);
		if (other.collider.tag == "PlayerWeapon")
		{
			//if (other.collider.GetComponent<WeaponBaseScript>().weaponHitDamage.IsAttacking())
			{
				DestroyPot();
				AddForceToFragments(other);
			}
		}
		else if (other.collider.tag != "Player")
		{
			if (other.relativeVelocity.magnitude > breakThreshold)
				DestroyPot();
		}
	}
	void OnTriggerEnter(Collider other)
	{
		if (other.tag == "PlayerWeapon")
		{
			DestroyPot();
			//AddForceToFragments(other);
		}
	}
	private void AddForceToFragments(Collision other)
	{
		Transform[] fragments = new Transform[destroyed.childCount];
		for (int i = 0; i < destroyed.childCount; ++i)
		{
			fragments[i] = destroyed.GetChild(i).transform;
		}
		Vector3 force = new Vector3(0, 0, 0);
		for (int i = 0; i < fragments.Length; ++i)
		{
			force = (fragments[i].position - other.contacts[0].point);
			fragments[i].GetComponent<Rigidbody>().AddForce(force.normalized * 0.005f, ForceMode.Impulse);
		}
	}
	private void InheritVelocity(Vector3 velocity, Vector3 randomOffset)
	{
		Transform[] fragments = new Transform[destroyed.childCount];
		for (int i = 0; i < destroyed.childCount; ++i)
		{
			fragments[i] = destroyed.GetChild(i).transform;
		}
		for (int i = 0; i < fragments.Length; ++i)
		{
			fragments[i].GetComponent<Rigidbody>().velocity = velocity + new Vector3( 
				Random.Range(-randomOffset.x, randomOffset.x),
				Random.Range(-randomOffset.y, randomOffset.y),
				Random.Range(-randomOffset.z, randomOffset.z));
		}
	}
	void OnCollisionStay(Collision other)
	{
		CheckIfRoll(other);
	}
	void CheckIfRoll(Collision other)
	{
		if (other.collider.tag == "Player")
		{
			if (other.collider.GetComponent<StateChecks>().IsInAnimation(PlayerHashIDs.rollingState))
			{
				DestroyPot();
				AddForceToFragments(other);
			}
		}
	}

}
