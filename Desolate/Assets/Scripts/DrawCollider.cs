﻿using UnityEngine;

[ExecuteInEditMode]
public class DrawCollider : MonoBehaviour {
	private new BoxCollider collider;
	private Collider test;

	void Start() {
		collider = (BoxCollider)GetComponent<Collider>();
	}

	void OnDrawGizmosSelected() {
        if (collider)
		    Gizmos.DrawCube(transform.position, collider.size);
	}
}
