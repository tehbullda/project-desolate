﻿using UnityEngine;
using System.Collections;

public class PlayerStats : MonoBehaviour {
	public BaseStats baseStats;

	//public float health;
	//public float stamina;
	//public float armor;
	//public float stability;
	//public float healingPower;
	//public float baseDamage;

	public int healthExpansions;
	public int staminaExpansions;
	private int oldHealthExpansions = 0;
	private int oldStaminaExpansions = 0;
	private const string healthExpansionSavetag = "NumHealthExpansions";
	private const string staminaExpansionSavetag = "NumStaminaExpansions";


	// Use this for initialization
	void Start () {
		//if (SaveManager.CheckTagExists(healthExpansionSavetag))
		//{
		//    oldHealthExpansions = healthExpansions = SaveManager.ReadData<int>(healthExpansionSavetag);
		//    Health hp = GetComponent<Health>();
		//    for (int i = 0; i < healthExpansions; ++i)
		//    {
		//        hp.AddExpansion(true, true);
		//    }
		//}
		//if (SaveManager.CheckTagExists(staminaExpansionSavetag))
		//{
		//    oldStaminaExpansions = staminaExpansions = SaveManager.ReadData<int>(staminaExpansionSavetag);
		//    Stamina stam = GetComponent<Stamina>();
		//    for (int i = 0; i < staminaExpansions; ++i)
		//    {
		//        stam.AddExpansion(true, true);
		//    }
		//}
	}
	
	// Update is called once per frame
	void Update () {
		if (healthExpansions != oldHealthExpansions)
		{
			//SaveManager.SaveData<int>(healthExpansions, healthExpansionSavetag);
			oldHealthExpansions = healthExpansions;
		}
		if (staminaExpansions != oldStaminaExpansions)
		{
			//SaveManager.SaveData<int>(staminaExpansions, staminaExpansionSavetag);
			oldStaminaExpansions = staminaExpansions;
		}
	}
}
