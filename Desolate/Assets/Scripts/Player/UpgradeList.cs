﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UpgradeList : MonoBehaviour
{
    public UpgradePanel panel;
    public enum Upgrade
    {
        ChargeAttack
    }
    private List<Upgrade> acquiredUpgrades;

    // Use this for initialization
    void Start()
    {
        acquiredUpgrades = new List<Upgrade>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void AddUpgrade(Upgrade upgrade)
    {
        if (HasUpgrade(upgrade))
        {
            Debug.LogError("Upgrade already Acquired");
            return;
        }

        acquiredUpgrades.Add(upgrade);
        ReportChangeInUpgrade();
    }

    private bool HasUpgrade(Upgrade upgrade)
    {
        for (int i = 0; i < acquiredUpgrades.Count; ++i)
        {
            if (acquiredUpgrades[i] == upgrade)
                return true;
        }
        return false;
    }
    private void ReportChangeInUpgrade()
    {
        for (int i = 0; i < acquiredUpgrades.Count; ++i)
        {
            switch (acquiredUpgrades[i])
            {
                case Upgrade.ChargeAttack:
                    GetComponent<WeaponChargeAttack>().SetChargeAttackActive(true);
                    break;
            }
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.name == "UpgradeChargeAttack")
        {
            AddUpgrade(Upgrade.ChargeAttack);
            Destroy(other.gameObject);
            panel.InitializeUpgradePanel("Weapon Charge System Acquired", "Hold Y to charge up your Heavy Attacks");
        }
        
    }
    private void ShowInfoScreen()
    {
        
    }


}
