﻿using UnityEngine;
using System.Collections;

public class WeaponChargeAttack : MonoBehaviour
{
    public SwordChargeEffect chargeEffects;
    private Animator animator;
    private StateChecks stateChecks;

    private bool charging;

    private float timeToMaxCharge = 0.55f;
    private float chargeTimer;
    private bool fullyCharged = false;

    private bool activated;
    // Use this for initialization
    void Start()
    {
        animator = GetComponent<RemadePlayerController>().animator;
        stateChecks = GetComponent<StateChecks>();
    }
    public void SetChargeAttackActive(bool activated)
    {
        this.activated = activated;
    }
    // Update is called once per frame
    void Update()
    {
        if (!RelicChecklist.HasCollected(RelicChecklist.Relics.ChargeAttack, 0))
            return;


        if (Keybindings.GetBoolInput(Keybindings.BoolAction.HeavyAttack) && IsInChargeAnimation() && !fullyCharged)
            charging = true;
        else
            charging = false;

        if (charging)
            chargeTimer += Time.deltaTime;
        else
            chargeTimer = 0.0f;

        if (chargeTimer >= timeToMaxCharge)
            fullyCharged = true;
        //else
        //    fullyCharged = false;
        if (!(stateChecks.IsInAnimation(PlayerHashIDs.heavyAttackStateMid)
            || stateChecks.IsInAnimation(PlayerHashIDs.heavyAttackStateSwing)
            || stateChecks.IsInAnimation(PlayerHashIDs.secondHeavyAttackStateMid)
            || stateChecks.IsInAnimation(PlayerHashIDs.secondHeavyAttackStateSwing)))
        {
            fullyCharged = false;
        }


        chargeEffects.SetChargeValue(Mathf.Clamp(chargeTimer / timeToMaxCharge, 0.0f, 1.0f));
        animator.SetBool("Charging", charging);
    }
    bool IsInChargeAnimation()
    {
        return (stateChecks.IsInAnimation(PlayerHashIDs.heavyAttackStateMid) || stateChecks.IsInAnimation(PlayerHashIDs.secondHeavyAttackStateMid));
    }
    public bool IsFullyCharged()
    {
        return fullyCharged;
    }

    //void OnGUI()
    //{
    //    GUI.Label(new Rect(0, 500, 500, 500), "Charged: " + fullyCharged);
    //}

}
