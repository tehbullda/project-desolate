﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TargetFinder : MonoBehaviour {

    public Transform player;

    public static List<Transform> targetsInView;
    GameObject[] targets;

    private float refreshRate = 0.5f;
    private float timer = 0.0f;



    public float fieldOfViewAngle = 90.0f;
    public float visionRange = 100.0f;
    public float angleBias = 0.0f;

	// Use this for initialization
	void Start () {
	targetsInView = new List<Transform>();
	}
	
	// Update is called once per frame
	void Update () {
        //target = findOptimalTarget();
        timer += Time.deltaTime;
        if(timer >= refreshRate)
        {
            timer = 0.0f;
            CheckTargets();
        }

	}

    public void CheckTargets()
    {
        targetsInView.Clear();
        targets = GameObject.FindGameObjectsWithTag("Target");
        if (targets.Length <= 0)
            return;
        for(uint i = 0; i <targets.Length; ++i)
        {
            if(RendererExtensions.IsVisibleFrom(targets[i].GetComponent<Renderer>(), Camera.main))
                targetsInView.Add(targets[i].transform);
        }
    }
    public Transform FindOptimalTarget()
    {
        Transform optimal = null;
        float highestScore = -1.0f;
        //for (uint i = 0; i < TargetFinder.targetsInView.Count; ++i)
        foreach (Transform t in targetsInView)
        {
            if (!t) continue;

            float score = CalculateScore(t, player.position, player.forward);

            if (highestScore < score && score > 0.0f)
            {
                highestScore = score;
                optimal = t;
            }
        }
        return optimal;
    }
    public Transform FindCustomTarget(Vector3 origin, Vector3 direction)
    {
        Transform optimal = null;
        float highestScore = -1.0f;
        foreach (Transform t in targetsInView)
        {
            if (!t) continue;


            float score = CalculateScore(t, origin, direction);

            if (highestScore < score && score > 0.0f)
            {
                highestScore = score;
                optimal = t;
            }
        }
        return optimal;
    }
    float CalculateScore(Transform p_target, Vector3 p_position, Vector3 p_direction)
    {
        float distance = Vector3.Distance(p_position, p_target.position);
        float angle = Vector3.Angle(p_direction, (p_target.position - p_position).normalized);

        //Outside FOV and range
        if (angle > fieldOfViewAngle / 2 || distance > visionRange)
            return -2.0f;

        float disScore = (1.0f - (distance / visionRange));
        float angleScore = ((angle / (fieldOfViewAngle / 2)));


        return disScore + angleScore * (1.0f + disScore);
    }
}
