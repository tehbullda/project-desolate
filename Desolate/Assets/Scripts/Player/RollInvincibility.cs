﻿using UnityEngine;
using System.Collections;

public class RollInvincibility : MonoBehaviour {
	private StateChecks stateChecks;
    public GameObject playerMesh;
    private Renderer playerMeshRenderer;

    private Color standardColor;

    [Range(0, 1)]
    public float invincibilityStartTime = 0.0f;
    public float invincibilityTime = 0.25f;

    private bool invincible = false;
	// Use this for initialization
	void Start () {
        stateChecks = GetComponent<StateChecks>();
        playerMeshRenderer = playerMesh.GetComponent<Renderer>();
        standardColor = playerMeshRenderer.material.color;
	}
	
	// Update is called once per frame
	void Update () {
        if (stateChecks.IsInAnimation(PlayerHashIDs.rollingState) && stateChecks.stateInfo.normalizedTime > invincibilityStartTime && stateChecks.stateInfo.normalizedTime < invincibilityStartTime + invincibilityTime)
        {
            
            playerMeshRenderer.material.color = Color.blue;
            invincible = true;
        }
        else
        {
            playerMeshRenderer.material.color = standardColor;
            invincible = false;
        }
    }
    public bool IsInvincible()
    {
        return invincible;
    }

}
