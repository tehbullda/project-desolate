﻿using UnityEngine;
using System.Collections;

public class StateChecks : MonoBehaviour
{

	public Animator anim;
	public AnimatorStateInfo stateInfo;
	public AnimatorStateInfo nextStateInfo;
	private int currentStateHash;
  
	private float queueWindowInNormalizedTime = 0.2f;
	
	void Update()
	{
		stateInfo = anim.GetCurrentAnimatorStateInfo(0);
		nextStateInfo = anim.GetNextAnimatorStateInfo(0);
	}
	public bool IsInAnimation(int hashName)
	{
		return (stateInfo.fullPathHash == hashName);
	}
	public int GetNextAnimation()
	{
		return nextStateInfo.fullPathHash;
	}
	public bool IsNextAnimation(int hashName)
	{
		return (stateInfo.fullPathHash == hashName);
	}
	public int GetCurrentStateHash()
	{
		return stateInfo.fullPathHash;
	}
	public bool QueueWindowOpen()
	{
		if (InNeutralState())
			return true;
		return (stateInfo.normalizedTime > queueWindowInNormalizedTime);
	}
	bool InNeutralState()
	{
		return (IsInAnimation(PlayerHashIDs.idleState) || IsInAnimation(PlayerHashIDs.locomotionState) || IsInAnimation(PlayerHashIDs.heavyAttackState) || IsInAnimation(PlayerHashIDs.heavyAttackStateSwing));
	}
	
	//void OnGUI()
	//{
	//    GUI.Label(new Rect(0, 100, 300, 300), "Neutral: " + InNeutralState() + " WindowOpen: " + QueueWindowOpen());
	//}
	
}
