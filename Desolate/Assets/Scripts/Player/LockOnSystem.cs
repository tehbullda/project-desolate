﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LockOnSystem : MonoBehaviour
{
	private static float breakDistance = 40.0f;


	public TargetFinder finder;

	private Transform target;
	Vector2 stickDirCurrent;
	Vector2 stickDirPrevious;


	
	// Use this for initialization
	void Start()
	{

	}

	// Update is called once per frame
	void Update()
	{
		if (target != null)
			if (Vector3.Distance(target.position, transform.position) > breakDistance)
			{
				BreakLockOn();
			}
		//LockOn toggle
		if (Keybindings.GetBoolInputDown(Keybindings.BoolAction.LockOn))
		{
			if (target)
				target = null;
			else
				LockOnTarget();
		}
	}
	void LockOnTarget()
	{
		target = finder.FindOptimalTarget();
	}
	public void Unlock()
	{
		target = null;
	}
	public Vector3 GetRotationToTarget()
	{
		return (target.position - transform.position).normalized;
	}
	public Transform GetTarget()
	{
		return target;
	}
	public bool IsLockedOn()
	{
		return target;
	}
	public void BreakLockOn()
	{
		target = null;
	}
	public void SetTarget(Transform target)
	{
		this.target = target;
	}
}
