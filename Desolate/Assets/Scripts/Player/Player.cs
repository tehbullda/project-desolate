﻿using UnityEngine;
using System.Collections;
using ProtoBuf;

//Utility class for keeping track of the player's stuff
public class Player : Vexe.Runtime.Types.BetterBehaviour {
	Player()
	{
		player = this;
	}

	[SerializeField] private DamageDealer damageDealer = null;
	[SerializeField] private DamageReceiver damageReceiver = null;
	[SerializeField] private GameObject playerObject = null;
	[SerializeField] private Stats stats = null;
	[SerializeField] private GameObject playerWeapon = null;

	private static Player player;

	public DamageDealer DamageDealer
	{ get { return damageDealer; } }

	public DamageReceiver DamageReceiver
	{ get { return damageReceiver; } }

	public GameObject PlayerObject
	{ get { return playerObject; } }

	public Stats Stats
	{ get { return stats; } }

	public static Player GetPlayer
	{ get { return player; } }

	public GameObject PlayerWeapon
	{ get { return playerWeapon; } }
}
