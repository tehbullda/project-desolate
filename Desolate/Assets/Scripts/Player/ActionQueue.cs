﻿using UnityEngine;
using System.Collections;

public class ActionQueue : MonoBehaviour
{

	public enum QueueableActions
	{
		None,
		LightAttack,
		HeavyAttack,
		Rolling

	}

	private RemadePlayerController playerController;
	private QueueableActions queued;
	private float queueStay = 0.7f;
	private float queueTimer = 0.0f;

	// Use this for initialization
	void Start()
	{
		playerController = GetComponent<RemadePlayerController>();
	}

	// Update is called once per frame
	void Update()
	{
		if (Keybindings.GetBoolInputDown(Keybindings.BoolAction.LightAttack))
			SetQueuedAction(QueueableActions.LightAttack);
		if (Keybindings.GetBoolInputDown(Keybindings.BoolAction.HeavyAttack))
			SetQueuedAction(QueueableActions.HeavyAttack);
		if (Keybindings.GetBoolInputUp(Keybindings.BoolAction.RollAndRun))
			if (!GetComponent<StateChecks>().IsInAnimation(PlayerHashIDs.sprintState))
				if (playerController.GetMovement().magnitude != 0)
					SetQueuedAction(QueueableActions.Rolling);

		if (queued != QueueableActions.None)
			queueTimer += Time.deltaTime;
		if (queueTimer >= queueStay)
			SetQueuedAction(QueueableActions.None);
	}
	private void SetQueuedAction(QueueableActions action)
	{
		queued = action;
		queueTimer = 0.0f;
	}
	public QueueableActions GetQueuedActionAndPurge()
	{
		QueueableActions temp = queued;
		queued = QueueableActions.None;
		return temp;
	}
	public QueueableActions GetQueuedAction()
	{
		return queued;
	}
	public void PurgeQueue()
	{
		queued = QueueableActions.None;
	}
}
