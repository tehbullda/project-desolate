﻿using UnityEngine;
using System.Collections;

public class Stamina : MonoBehaviour
{

    public UIBar staminaBar;
    public CircularStaminaTest circleBar;
    public UpgradePanel panel;
    private Stats playerStats;
    private float maxStamina;
    private float currentStamina;
    private int expansions;
    private float staminaFromExpansion = 15f;

    public float MaxStamina
    {
        get
        {
            return maxStamina;
        }
        set
        {
            float percentDifference = (value / maxStamina);
            maxStamina = value;
            currentStamina *= percentDifference;
            playerStats.stamina.Max = value;
        }
    }

    public float CurrentStamina
    {
        get
        {
            return currentStamina;
        }
        set
        {
            currentStamina = Mathf.Clamp(value, 0.0f, maxStamina);
            staminaBar.SetPercentage(currentStamina / maxStamina);
            circleBar.SetPercentage(currentStamina / maxStamina);

            timer = 0.0f;
        }
    }

    private float staminaRegen = 40.0f;

    private float timeBeforeRegen = 1.0f;
    private float timer = 0.0f;

    // Use this for initialization
    void Start()
    {
        playerStats = GetComponent<Stats>();
        MaxStamina = playerStats.stamina.Max;
        currentStamina = maxStamina;

    }

    // Update is called once per frame
    void Update()
    {
        if (CanRegenStamina())
            Regen();
#if UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.G))
            UseStamina(25);
#endif

        staminaBar.SetPercentage(currentStamina / maxStamina);
        circleBar.SetPercentage(currentStamina / maxStamina);

    }
    bool CanRegenStamina()
    {
        timer += Time.deltaTime;
		return (timer >= timeBeforeRegen);
    }
    void Regen()
    {
        if (currentStamina < maxStamina)
            currentStamina += staminaRegen * Time.deltaTime;
        else
            currentStamina = maxStamina;
    }
    public void UseStamina(float staminaUsage)
    {
        currentStamina = Mathf.Clamp(currentStamina - staminaUsage, 0.0f, maxStamina);
        timer = 0.0f;

    }
    public float GetStaminaPercentage()
    {
        return currentStamina / maxStamina;
    }
    public bool StaminaAvaliable()
    {
        return (currentStamina > 0.0f);
    }
    private void UpdateMaxStamina()
    {
        maxStamina = playerStats.stamina.BaseValue + staminaFromExpansion * expansions;
    }
    private void AddStamina(float stamina)
    {
        CurrentStamina += stamina;
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.name == "StaminaExpansion")
        {
            AddExpansion();
            other.GetComponent<ExpansionSaveScript>().Save();
            AnalyticsSubmitter.UpdateVariableAdd(AnalyticsSubmitter.AnalyticsType.StaminaExpansions, 1);
            Destroy(other.gameObject);
        }
    }
    public void AddExpansion(bool nonotif = false, bool startUpExpansion = false)
    {
        if (!nonotif)
            panel.InitializeUpgradePanel("Stamina Expansion Acquired", "Stamina increased by a small amount");
        expansions += 1;
        UpdateMaxStamina();
        AddStamina(staminaFromExpansion);
        staminaBar.Expand(staminaFromExpansion);
        //if (!startUpExpansion)
        //    playerStats.staminaExpansions++;
    }
}
