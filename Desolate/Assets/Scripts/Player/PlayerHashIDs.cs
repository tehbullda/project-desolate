﻿using UnityEngine;
using System.Collections;


public static class PlayerHashIDs
{
	const int numberOfStateHashes = 10;

	public static int idleState = Animator.StringToHash("Base Layer.Idle");
	public static int locomotionState = Animator.StringToHash("Base Layer.Locomotion");
	public static int rollingState = Animator.StringToHash("Base Layer.Roll");
	public static int lightAttackState = Animator.StringToHash("Base Layer.Attacks.LightAttack");
	public static int secondLightAttackState = Animator.StringToHash("Base Layer.Attacks.LightAttack2");
	public static int heavyAttackState = Animator.StringToHash("Base Layer.Attacks.HeavyAttack");
	public static int heavyAttackStateMid = Animator.StringToHash("Base Layer.Attacks.HeavyAttackMid");
	public static int heavyAttackStateSwing = Animator.StringToHash("Base Layer.Attacks.HeavyAttackSwing");
	public static int secondHeavyAttackState = Animator.StringToHash("Base Layer.Attacks.HeavyAttack2");
	public static int secondHeavyAttackStateMid = Animator.StringToHash("Base Layer.Attacks.HeavyAttack2Mid");
	public static int secondHeavyAttackStateSwing = Animator.StringToHash("Base Layer.Attacks.HeavyAttack2Swing");
	public static int runAttackState = Animator.StringToHash("Base Layer.RunAttack");
	public static int sprintState = Animator.StringToHash("Base Layer.Sprint");
	public static int interactState = Animator.StringToHash("Base Layer.Interact");
	public static int speedFloat = Animator.StringToHash("Speed");
	public static int rollingBool = Animator.StringToHash("Rolling");
	public static int lightAttackingBool = Animator.StringToHash("AttackingLight");
	public static int heavyAttackingBool = Animator.StringToHash("AttackingHeavy");
	public static int staggeredBool = Animator.StringToHash("Staggered");
	public static int sprintingBool = Animator.StringToHash("Sprinting");
	public static int interactingBool = Animator.StringToHash("Interacting");


#if (UNITY_EDITOR || DEVELOPMENT_BUILD)
	public static string HashToStringDebug(int hash)
	{
		//Only use for debugging
		if (idleState == hash)
			return "Idle";
		if (locomotionState == hash)
			return "Locomotion";
		if (rollingState == hash)
			return "Rolling";
		if (lightAttackState == hash)
			return "Light";
		if (heavyAttackState == hash)
			return "Heavy";
		if (secondLightAttackState == hash)
			return "SecondLight";
		if (secondHeavyAttackState == hash)
			return "SecondHeavy";

		return "";
	}
}
#endif
