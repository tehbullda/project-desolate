﻿using UnityEngine;
using System.Collections;

public class Health : MonoBehaviour
{

    public UpgradePanel panel;
    public UIBar healthBar;
    private Stats playerStats;
    private PlayerDeath playerDeath;

    private float health;
    private float maxHealth;

    private bool invulnerable = false;

    private int expansions;
    private float healthFromExpansion = 20;

    // Use this for initialization
    void Start()
    {
        playerDeath = GameObject.Find("_Globals").GetComponent<PlayerDeath>();
        playerStats = GetComponent<Stats>();
        maxHealth = playerStats.health.Max;
        //expansions = playerStats.healthExpansions;


        health = maxHealth;
        UpdateHealthBar();

    }

    // Update is called once per frame
    void Update()
    {
#if UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.R))
            invulnerable = !invulnerable;
		if (invulnerable)
			playerStats.health.Current = playerStats.health.Max;
#endif
		UpdateHealthBar();
    }
    void UpdateMaxHealth()
    {
        maxHealth = maxHealth + healthFromExpansion * expansions;
        UpdateHealthBar();
    }

    void UpdateHealthBar()
    {
        healthBar.SetPercentage(playerStats.health.Current / maxHealth);
    }

    public void ReduceHealth(float reduction)
    {
        if (invulnerable)
            return;
        if (health <= 0.0f)
            return;
        health -= reduction;
        health = Mathf.Clamp(health, 0.0f, maxHealth);
        UpdateHealthBar();

        if (health <= 0.0f)
            playerDeath.SetPlayerDead(true);
    }

    public void AddHealth(float adding)
    {
        if (health <= 0.0f)
            return;
        playerStats.health.Current += (int)adding;
        UpdateHealthBar();
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.name == "HealthExpansion")
        {
            AddExpansion();
            other.GetComponent<ExpansionSaveScript>().Save();
            AnalyticsSubmitter.UpdateVariableAdd(AnalyticsSubmitter.AnalyticsType.HealthExpansions, 1);
            Destroy(other.gameObject);
        }
    }
    public void AddExpansion(bool nonotif = false, bool startUpExpansion = false)
    {
        if (!nonotif)
            panel.InitializeUpgradePanel("Health Expansion Acquired", "Health increased by a small amount");
        expansions += 1;
        UpdateMaxHealth();
        AddHealth(healthFromExpansion);
        healthBar.Expand(healthFromExpansion);
        //if (!startUpExpansion)
        //    playerStats.healthExpansions++;
    }
}
