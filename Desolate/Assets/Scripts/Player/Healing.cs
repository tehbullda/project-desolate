﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class Healing : MonoBehaviour
{
    public Text text;
    public UIBar bar;
    public HealingEffect healingEffect;
    private Stats playerStats;
    private Health health;

    private int maxNuberOfUses = 5;
    private int numberOfUsesLeft = 5;

    private float healingPower = 40.0f;



    private float refill = 0.0f;
    private static float refillTreshold = 100.0f;

    private Animator animator;
    private StateChecks stateChecks;

    private bool inHealingSequence;
    // Use this for initialization
    void Start()
    {
        animator = GetComponent<RemadePlayerController>().animator;
        stateChecks = GetComponent<StateChecks>();
        playerStats = GetComponent<Stats>();
        health = GetComponent<Health>();
        healingPower = playerStats.healingPower.Current;
    }

    // Update is called once per frame
    void Update()
    {
        if (Keybindings.GetBoolInputDown(Keybindings.BoolAction.Heal))
            if (!inHealingSequence)
                StartCoroutine(HealingSequence());

        text.text = numberOfUsesLeft + "/" + maxNuberOfUses;
        bar.SetPercentage(refill / refillTreshold);
    }
    private IEnumerator HealingSequence()
    {
        animator.SetBool(PlayerHashIDs.interactingBool, true);
        inHealingSequence = true;
        yield return new WaitForSeconds(0.6f);
        UseHealing();
        yield return new WaitForSeconds(0.2f);
        animator.SetBool(PlayerHashIDs.interactingBool, false);
        inHealingSequence = false;
    }
    public bool InHealingSequence()
    {
        return inHealingSequence;
    }
        

    public void UseHealing()
    {
        if (numberOfUsesLeft <= 0)
        {
            NoHealingFeedback();
            return;
        }

        --numberOfUsesLeft;
        AnalyticsSubmitter.UpdateVariableAdd(AnalyticsSubmitter.AnalyticsType.HealingCount, 1);
        HealingFeedback();
        health.AddHealth(healingPower);
    }

    void NoHealingFeedback()
    {

    }
    void HealingFeedback()
    {
        healingEffect.Play();
    }
    void RefillFeedback()
    {

    }
    void HealingAddedFeedback()
    {

    }

    void AddHealingUse()
    {
        if (numberOfUsesLeft < maxNuberOfUses)
        {
            ++numberOfUsesLeft;
        }
    }
    //100 is max
    public void RefillHealing(float amount)
    {
        refill += amount;
        if (refill >= refillTreshold)
        {
            refill -= refillTreshold;
            AddHealingUse();
            HealingAddedFeedback();
        }
        else
        {
            RefillFeedback();
        }

    }
    
}
