﻿using UnityEngine;
using System.Collections;

public class RemadePlayerController : MonoBehaviour
{

    public Animator animator;
    private StateChecks stateChecks;
    private ActionQueue actionQueue;
    private LockOnSystem lockOnSystem;
    private Stamina stamina;


    private bool lightAttack = false;
    private bool heavyAttack = false;
    private bool rolling = false;

    private Vector3 movement;
    private Vector3 direction;
    private float moveSpeed = 0;
    private const float maxMoveSpeed = 5;
    private const float sprintingMultiplier = 1.5f;
    private const float rollMoveSpeed = 7;
    private Vector3 rollDirection;

    private float acceleration = 10.0f;
    private float maxDeceleration = 0.9f;
    //private bool coroutineRunning = false;
    private float deceleration = 1.0f;
    private float maxTurnSpeed = 8;
    private float turnSpeed = 8;

    //private float inputX;
    //private float inputY;
    private Vector3 input;

    private bool sprinting = false;


    private bool sprintingInitialized = false;
    private float timeBeforeSprinting = 0.3f;
    private float sprintTimer = 0.0f;
    private float sprintingStaminaUsagePerSecond = 15.0f;

    // Use this for initialization
    void Start()
    {
        actionQueue = GetComponent<ActionQueue>();
        stateChecks = GetComponent<StateChecks>();
        lockOnSystem = GetComponent<LockOnSystem>();
        stamina = GetComponent<Stamina>();

        //Fixes the "Looking vector is zero"-spam before you move the character
        direction = transform.forward;
    }

    // Update is called once per frame
    void Update()
    {
        if (ProgressStatics.gameComplete || PlayerDeath.PlayerIsDead() || ProgressStatics.gamePaused)
        {
            return;
        }
        bool isLockedOn = lockOnSystem.IsLockedOn();
        input = new Vector3(Keybindings.GetFloatInput(Keybindings.FloatAction.MoveLeft, Keybindings.FloatAction.MoveRight), 0, Keybindings.GetFloatInput(Keybindings.FloatAction.MoveDown, Keybindings.FloatAction.MoveUp));
        if (input.magnitude >= 1.0f)
            input /= input.magnitude;

        input = AdjustDirectionToCamera(input);
        if (Input.GetMouseButton(0))
        {
            
            Vector3 dir;
            dir = Camera.main.WorldToScreenPoint(transform.position);
            dir = (dir - Input.mousePosition).normalized;
            input.x = dir.x;
            input.z = dir.y;
            input = -AdjustDirectionToCamera(input);
        }


        sprinting = CheckIfPlayerShouldSprint();

        if (sprinting)
        {
            stamina.UseStamina(sprintingStaminaUsagePerSecond * Time.deltaTime);
        }

        if (RecieveAction() && stamina.StaminaAvaliable())
        {
            lightAttack = (ActionQueue.QueueableActions.LightAttack == actionQueue.GetQueuedAction());
            heavyAttack = (ActionQueue.QueueableActions.HeavyAttack == actionQueue.GetQueuedAction());
            rolling = (ActionQueue.QueueableActions.Rolling == actionQueue.GetQueuedAction());
        }
        else
        {
            actionQueue.PurgeQueue();
            lightAttack = heavyAttack = rolling = false;
        }

        UpdateDirection(input, isLockedOn);
        UpdateMovement(input, isLockedOn);

        animator.SetBool(PlayerHashIDs.rollingBool, rolling);
        animator.SetBool(PlayerHashIDs.lightAttackingBool, lightAttack);
        animator.SetBool(PlayerHashIDs.heavyAttackingBool, heavyAttack);
        animator.SetFloat(PlayerHashIDs.speedFloat, (moveSpeed / maxMoveSpeed) * input.magnitude);
        animator.SetBool(PlayerHashIDs.sprintingBool, sprinting);

		ApplyMovement(input);

	}
    void FixedUpdate()
    {
        
    }
    bool CheckIfPlayerShouldSprint()
    {
        if (!sprintingInitialized)
            sprintingInitialized = (Keybindings.GetBoolInputDown(Keybindings.BoolAction.RollAndRun));
        if (!sprintingInitialized)
            return false;

        if (!stamina.StaminaAvaliable())
        {
            sprintingInitialized = false;
            return false;
        }

        if (Keybindings.GetBoolInput(Keybindings.BoolAction.RollAndRun))
            sprintTimer += Time.deltaTime;
        else
            sprintTimer = 0.0f;
        return (sprintTimer >= timeBeforeSprinting);
    }
    public bool IsSprinting()
    {
        return sprinting;
    }
    void ApplyMovement(Vector3 move)
    {
        Vector3 lookdirection = move;
        if (lockOnSystem.IsLockedOn())
        {
            direction = (lockOnSystem.GetTarget().position - transform.position);
            direction.y = 0;
            lookdirection = direction;
            
        }
        else
        {
            float turnDamping = 20.0f;
            if (!InCantMoveState())
                turnSpeed = Mathf.MoveTowards(turnSpeed, maxTurnSpeed, Time.deltaTime * turnDamping);
            else
                turnSpeed = Mathf.MoveTowards(turnSpeed, maxTurnSpeed / 10, Time.deltaTime * turnDamping);
        }
        if (stateChecks.IsInAnimation(PlayerHashIDs.rollingState))
            lookdirection = rollDirection;

        transform.forward = Vector3.Slerp(transform.forward, lookdirection, turnSpeed * Time.deltaTime);
        GetComponent<CharacterController>().Move(movement * Time.deltaTime);
    }

    void UpdateDirection(Vector3 move, bool isLockedOn)
    {
        Vector3 input = move;
        if (input.magnitude > 0.0f && !stateChecks.IsInAnimation(PlayerHashIDs.rollingState))
            direction = input;
    }
    void UpdateTurnSpeed()
    {
        if (stateChecks.IsInAnimation(PlayerHashIDs.rollingState))
            if (animator.GetCurrentAnimatorStateInfo(0).normalizedTime < 0.7f)
                return;
    }
    void UpdateMovement(Vector3 move, bool isLockedOn)
    {
        Vector3 input = move;

        if (InCantMoveState())
            moveSpeed = Mathf.Clamp((moveSpeed / 1.5f) - 0.1f, 0.0f, maxMoveSpeed);
        else
            UpdateMoveSpeed(input);

        if (stateChecks.IsInAnimation(PlayerHashIDs.rollingState))
        {
            moveSpeed = rollMoveSpeed;
            input = rollDirection;
        }
        else
        {
            rollDirection = input.normalized;
        }
        if (stateChecks.IsInAnimation(PlayerHashIDs.sprintState))
            moveSpeed = maxMoveSpeed * sprintingMultiplier;

        movement = input * moveSpeed;

    }
    void UpdateMoveSpeed(Vector3 input)
    {
        float targetMoveSpeed = (input.magnitude > 0.0f) ? maxMoveSpeed : 0.0f;
        moveSpeed = Mathf.Lerp(moveSpeed, targetMoveSpeed, acceleration * Time.deltaTime);
    }

    Vector3 AdjustDirectionToCamera(Vector3 move)
    {
        Vector3 forward = Camera.main.transform.forward;
        Vector3 right = Camera.main.transform.right;
        forward.y = 0;
        right.y = 0;
        return (move.x * right.normalized + move.z * forward.normalized);
    }

    bool RecieveAction()
    {
        return (stateChecks.QueueWindowOpen());
    }

    bool IsInAttackState()
    {
        if (stateChecks.IsInAnimation(PlayerHashIDs.lightAttackState) ||
            stateChecks.IsInAnimation(PlayerHashIDs.secondLightAttackState) ||
            stateChecks.IsInAnimation(PlayerHashIDs.heavyAttackStateSwing) ||
            stateChecks.IsInAnimation(PlayerHashIDs.secondHeavyAttackStateSwing))
            return true;
        return false;
    }
    bool InCantMoveState()
    {
        return (IsInAttackState() ||
            stateChecks.IsInAnimation(PlayerHashIDs.heavyAttackStateMid) ||
            stateChecks.IsInAnimation(PlayerHashIDs.secondHeavyAttackStateMid) ||
            stateChecks.IsInAnimation(PlayerHashIDs.interactState));
    }
	public float GetMoveSpeedPercentage()
	{
		return moveSpeed / maxMoveSpeed;
	}
	public Vector3 GetMovement()
	{
		return movement;
	}
}
