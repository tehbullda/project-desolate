﻿using UnityEngine;
using System.Collections;

public class WeaponSocket : MonoBehaviour
{
    public WeaponSlots HUDSlots;
    public Transform bone;
    private Transform equippedWeapon;
    

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    public void AttachWeapon(Transform weapon)
    {
        if (equippedWeapon)
            equippedWeapon.parent = null;
        equippedWeapon = weapon;
        if (!weapon.GetComponent<WeaponBaseScript>())
        {
            //Not a weapon
            Debug.LogError("Tried to equip a non-weapon");
            return;
        }

        equippedWeapon.parent = bone;
        equippedWeapon.localPosition = equippedWeapon.GetComponent<WeaponBaseScript>().positionInHand;
        equippedWeapon.localRotation = equippedWeapon.GetComponent<WeaponBaseScript>().rotationInHand;

        //if (HUDSlots)
        //    HUDSlots.ChangeImage(equippedWeapon.GetComponent<WeaponBaseScript>().weaponIcon);
    }
    public Transform GetWeapon()
    {
        return equippedWeapon;
    }
}
