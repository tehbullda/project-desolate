﻿using UnityEngine;
using System.Collections;

public class WeaponSystem : MonoBehaviour
{
    
    private WeaponSocket socket;


    public Transform primary;
    public Transform secondary;

    public bool primaryEquiped = true;

    public Animator animator;
    private Transform equipedWeapon;

    // Use this for initialization
    void Start()
    {
        socket = GetComponent<WeaponSocket>();
        socket.AttachWeapon(primary);
        EquipWeapon(primary);

    }

    // Update is called once per frame
    void Update()
    {
        //if (Xinput.GetButtonDown(Xinput.ControllerButton.DPadRight))
        //    ToggleWeapon();
    }
    public void ToggleWeapon()
    {
        if (primaryEquiped)
        {
            EquipWeapon(secondary);
            primaryEquiped = false;
        }
        else
        {
            EquipWeapon(primary);
            primaryEquiped = true;
        }
       
    }
    void EquipWeapon(Transform weapon)
    {
        //animator.runtimeAnimatorController = weapon.GetComponent<WeaponBaseScript>().overrideController;
        socket.AttachWeapon(weapon);
        equipedWeapon = weapon;
    }
    public Transform GetWeapon()
    {
        return equipedWeapon;
    }
}
