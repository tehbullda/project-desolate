﻿using UnityEngine;
using System.Collections;

public class WeaponHitDamage : MonoBehaviour
{

    public Animator playerAnim;
    private WeaponSocket weaponSocket;
    private bool doingDamage;
    private WeaponBaseScript currentWeapon;
    public WeaponChargeAttack chargeAttack;
    public bool debugDisplayRedBox = true;

    private bool attacking;
    private bool prevAttacking;

    private Renderer weaponRenderer;
    // Use this for initialization
    void Start()
    {
        weaponSocket = GetComponent<WeaponSocket>();
    }

    // Update is called once per frame
    void Update()
    {

        //prevAttacking = attacking;
        currentWeapon = weaponSocket.GetWeapon().GetComponent<WeaponBaseScript>();
        weaponRenderer = currentWeapon.transform.GetChild(0).GetComponent<Renderer>();
        if (IsAttacking())
        {
            //attacking = true;
            if (debugDisplayRedBox)
                weaponRenderer.material.color = Color.red;
        }
        else
        {
            //attacking = false;
            weaponRenderer.material.color = Color.white;
        }
    }

    public bool IsAttacking()
    {
        weaponRenderer.material.color = Color.white;
        //if (currentWeapon.InDamageWindow(playerAnim.GetCurrentAnimatorStateInfo(0).fullPathHash, playerAnim.GetCurrentAnimatorStateInfo(0).normalizedTime))
        {
            if (debugDisplayRedBox)
                weaponRenderer.material.color = Color.red;
            return true;
        }
        //else
            weaponRenderer.material.color = Color.white;
        return false;
    }
    public bool IsChargedAttack()
    {
        return chargeAttack.IsFullyCharged();
    }
}
