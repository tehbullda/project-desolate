﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WeaponHitList : MonoBehaviour
{

    private List<GameObject> hits;

    // Use this for initialization
    void Start()
    {
        hits = new List<GameObject>();
    }

    // Update is called once per frame
    void Update()
    {

    }
    public bool CanHit(GameObject other)
    {
        for (int i = 0; i < hits.Count; ++i)
        {
            if (hits[i] == other)
                return false;
        }
        hits.Add(other);
        return true;
    }
    public void Clear()
    {
        hits.Clear();
    }
}
