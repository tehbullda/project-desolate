﻿using UnityEngine;
using System.Collections;

public class AnimationEventHandler : MonoBehaviour
{
	private Transform rootObject, modelObject;
	private Stamina stamina;
	[SerializeField]
	private WeaponHitList hitList;
	//private float timer = 0.75f;
	//private float currentTime = 0.0f;
	//private Collider playerWeapon = null;
	[SerializeField]
	private Collider weaponCollider;

	void Start()
	{
		rootObject = transform.parent;
		modelObject = transform;

		stamina = GetComponentInParent<Stamina>();
		if (!hitList)
			hitList = transform.GetComponentInChildren<WeaponHitList>();
	}
	//void Update()
	//{
	//	if (transform.name == "PlayerModel")
	//		return;

	//	currentTime -= Time.deltaTime;
	//	if (currentTime < 0.0f)
	//	{
	//		GetComponent<Rigidbody>().isKinematic = false;
	//		GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
	//		playerWeapon.enabled = true;
	//       }
	//}
	void MovePlayerObject(float p_offset)
	{
		Debug.Log("Moving player...");

		rootObject.position += (rootObject.forward.normalized * p_offset);
		modelObject.position -= (rootObject.forward.normalized * p_offset);
	}

	void SpendStamina(float p_amount)
	{
		if (stamina)
		{
			stamina.CurrentStamina -= p_amount;
		}
	}

	void ClearHitList()
	{
		if (hitList)
		{
			hitList.Clear();
		}
	}

	void EnableWeaponHitbox()
	{
		if (weaponCollider != null)
			weaponCollider.enabled = true;
	}

	void DisableWeaponHitbox()
	{
		if (weaponCollider != null)
			weaponCollider.enabled = false;
	}
	//void OnCollisionEnter(Collision collision)
	//{
	//	if (transform.name == "PlayerModel")
	//		return;

	//	if (collision.collider.tag == "PlayerWeapon" && hitList.CanHit(collision.gameObject))
	//	{
	//		playerWeapon = collision.collider;
	//		GetComponent<Rigidbody>().isKinematic = true;
	//		playerWeapon.enabled = true;
	//		currentTime = timer;
	//		GetComponent<Rigidbody>().velocity = default(Vector3);
	//		GetComponent<Rigidbody>().angularVelocity = default(Vector3);
	//		GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePosition;
	//		transform.GetComponentInChildren<ParticleSystem>().transform.position = collision.contacts[0].point;
	//		transform.GetComponentInChildren<ParticleSystem>().Play();
	//		GetComponent<EnemyAnimationHandler>().SetTrigger(EnemyAnimationHandler.AnimationState.Staggered);
	//       }
	//}
}
