﻿using UnityEngine;
using System.Collections;

public class RotateHead : MonoBehaviour
{

    public Transform head;
    private Transform target;
    public TargetFinder targetFinder;

    Quaternion currentRot;
    Quaternion realRot;
    Quaternion targetRot;

    // Use this for initialization
    void Start()
    {

    }
    void Update()
    {
        
    }

    // Update is called once per frame
    void LateUpdate()
    {
        realRot = head.rotation;
        //Debug.DrawRay(head.position, head.forward);
        target = targetFinder.FindOptimalTarget();
        if (!target)
        {
            targetRot = realRot;
        }
        else
        {
            targetRot = Quaternion.LookRotation(target.position - head.position, transform.up);
        }

        currentRot = Quaternion.Slerp(currentRot, targetRot, 5 * Time.deltaTime);
        head.rotation = currentRot;
    }
}
