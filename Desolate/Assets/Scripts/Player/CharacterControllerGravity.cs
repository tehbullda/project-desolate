﻿using UnityEngine;
using System.Collections;

public class CharacterControllerGravity : MonoBehaviour
{
	private const float gravityAcceleration = 8.1f;
	private const float startGravity = 1.0f;
	private const float maxGravity = 20.0f;
	private const float groundCheckRange = 1.1f;


	private float airTime = 0.0f;

	private bool prevGrounded = false;
	private bool grounded = false;

	public float current;
	private CharacterController characterController;
	public LayerMask layerMask;


	// Use this for initialization
	void Start()
	{
		characterController = GetComponent<CharacterController>();
	}

	// Update is called once per frame
	void Update()
	{
		prevGrounded = grounded;
		grounded = CheckIfGrounded();
		if (!grounded)
		{
			airTime += Time.deltaTime;
			ApplyGravity();
		}
		else
			airTime = 0.0f;
	}
	void ApplyGravity()
	{
		characterController.Move(Vector3.down * GetGravity() * Time.deltaTime);
	}
	private float GetGravity()
	{
		float gravity = startGravity + airTime * gravityAcceleration;
		current = Mathf.Clamp(gravity, startGravity, maxGravity);
		return Mathf.Clamp(gravity, startGravity, maxGravity);

	}
	bool CheckIfGrounded()
	{
		Ray ray = new Ray(transform.position, Vector3.down);
		//Debug.DrawRay(ray.origin, ray.direction * groundCheckRange);
		RaycastHit hit;
		if (Physics.Raycast(ray, out hit, groundCheckRange, layerMask))
		{
			if (hit.collider.isTrigger)
				return false;
			if (hit.transform.tag == "InheritMovement")
			{
				transform.parent = hit.transform;
				transform.position = hit.point + Vector3.up;
			}
			return true;
		}
		transform.parent = null;
		return false;
	}
	public float GetAirTime()
	{
		return airTime;
	}
	public bool HitGround()
	{
		return (prevGrounded == false && grounded == true);
	}

}
