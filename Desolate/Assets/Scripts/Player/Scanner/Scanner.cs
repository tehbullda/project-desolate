﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
//	TODO
//	Must release lockon when exiting scanmode
//	Track scandata
//	Finished Scan stuff
[RequireComponent(typeof(LockOnSystem))]
public class Scanner : MonoBehaviour
{

	private const float scanTime = 3.0f;
	private float timer = 0.0f;

	private ScanTarget[] scannables;

	private bool prevInScanMode = false;
	private static bool inScanMode = false;
	private bool isScanning = false;

	private LockOnSystem lockOnSystem;

	private ScanTarget currentScannable;

	private bool prevInput = false;
	private bool input = false;

	[HideInInspector]
	public List<ScanTarget> scannablesInView;
	[HideInInspector]
	public ScanTarget bestScannable;



	void Start()
	{
		lockOnSystem = GetComponent<LockOnSystem>();
		ScanDataManager.CreateFile();
		ScanDataManager.LoadLoreDictionary();
		ScanDataManager.SetScanDataAvailablity("Watcher", true);
		ScanDataManager.SetScanDataAvailablity("Monument", true);

		ScanData data;

		if (ScanDataManager.GetAcquiredScanData("Watcher", out data))
		{
			Debug.Log(data.subTitle);
		}
		if (ScanDataManager.GetAcquiredScanData("Monument", out data))
		{
			Debug.Log(data.subTitle);
		}

	}

	void Update()
	{
		prevInScanMode = inScanMode;
		if (Xinput.GetButtonUp(Xinput.ControllerButton.LeftTrigger))
			EnterScanMode();
		if ((Xinput.GetButtonUp(Xinput.ControllerButton.B)))
			ExitScanMode();


		prevInput = input;
		input = Xinput.GetButton(Xinput.ControllerButton.LeftTrigger);

		if (inScanMode)
		{
			UpdateScanMode();
		}
		else
		{
			EndScan();
		}

	}
	private void UpdateScanMode()
	{
		scannablesInView = GetScannablesInView();
		bestScannable = GetBestScannable(scannablesInView);
		if (!prevInput && input)
			StartScan();
		if (prevInput && !input)
			EndScan();
		if (currentScannable != null)
		{

			if (currentScannable.ScanCompleted())
				EndScan();
		}
		ScanTarget();
	}
	private void EnterScanMode()
	{
		inScanMode = true;
		RefreshScannables();
	}
	private void ExitScanMode()
	{
		inScanMode = false;
		if (currentScannable != null)
			EndScan();
	}

	private void StartScan()
	{
		currentScannable = bestScannable;
		if (currentScannable)
		{
			lockOnSystem.SetTarget(currentScannable.transform);
			currentScannable.OnStartScan();
		}
	}

	private void ScanTarget()
	{
		if (currentScannable)
		{
			currentScannable.Scan();
		}
	}

	private void EndScan()
	{
		currentScannable = null;
		lockOnSystem.SetTarget(null);
	}

	private void RefreshScannables()
	{
		scannables = FindObjectsOfType<ScanTarget>();
	}

	private List<ScanTarget> GetScannablesInView()
	{
		Profiler.BeginSample("FindingScannablesInView");
		List<ScanTarget> visible = new List<ScanTarget>();

		for (int i = 0; i < scannables.Length; ++i)
		{
			if (scannables[i] == null)
				continue;
			if (scannables[i].GetComponent<Renderer>().isVisible)
			{
				visible.Add(scannables[i]);
			}
		}

		Profiler.EndSample();
		return visible;
	}

	private ScanTarget GetBestScannable(List<ScanTarget> potentialScannables)
	{
		Profiler.BeginSample("DeterminBestScannable");
		ScanTarget bestScannable = null;
		float minDistance = float.MaxValue;
		float rangeFromCenter = Screen.height * 0.5f;
		Vector3 center = new Vector3(Screen.width / 2, Screen.height / 2, 0);
		Vector3 objectPos;

		for (int i = 0; i < potentialScannables.Count; ++i)
		{
			potentialScannables[i].GetComponent<Renderer>().material.color = Color.white; //DebugThings

			objectPos = Camera.main.WorldToScreenPoint(potentialScannables[i].transform.position);
			objectPos.z = 0;
			float distance = Vector3.Distance(objectPos, center);
			if (minDistance > distance)
			{
				minDistance = distance;
				bestScannable = potentialScannables[i];
			}
		}

		if (minDistance > rangeFromCenter)
			bestScannable = null;

		if (bestScannable != null)
			bestScannable.GetComponent<Renderer>().material.color = Color.red; //DebugThings
		Profiler.EndSample();
		return bestScannable;
	}
	public static bool InScanMode()
	{
		return inScanMode;
	}


}
