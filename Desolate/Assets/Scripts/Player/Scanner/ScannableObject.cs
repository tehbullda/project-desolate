﻿using UnityEngine;
using System.Collections;

public class ScannableObject : MonoBehaviour
{
	public string scanDataName = "";
	public ScanTarget.ScanType scanType = ScanTarget.ScanType.Boss;
	public Vector3 offset;
	public Vector3 rotation;

	public bool faceCamera = false;

	


	private GameObject iconPrefab;

	public ScanTarget scanTarget;
	private Vector3 direction;
	// Use this for initialization
	void Awake()
	{

		iconPrefab = Resources.Load("ScanIcon") as GameObject;
		scanTarget = Instantiate(iconPrefab).GetComponent<ScanTarget>();
		scanTarget.transform.parent = transform;
		scanTarget.scanType = scanType;
		scanTarget.SetScanDataName(scanDataName);
	}

	// Update is called once per frame
	void Update()
	{
		scanTarget.transform.position = transform.position + offset;
		UpdateRotation();
	}
	void UpdateRotation()
	{
		if (faceCamera)
		{
			direction = (scanTarget.transform.position - Camera.main.transform.position).normalized;
			//direction.x = 0.0f;
			direction.y = 0.0f;
			//direction.z = 0.0f;
			scanTarget.transform.up = -direction;
		}
		else
			scanTarget.transform.up = rotation = (rotation * 0.3f).normalized;

	}
	void OnDrawGizmos()
	{
		Gizmos.DrawRay(transform.position + offset, direction * 5.0f);


		Gizmos.DrawSphere(transform.position + offset, 0.1f);
		rotation = (rotation * 0.3f).normalized;
		Gizmos.DrawRay(transform.position + offset, -(rotation * 0.3f));
	}
}

