﻿using UnityEngine;
using System.Collections;

public class ScanTarget : MonoBehaviour
{
	private ScannableTargetInfo info = ScannableTargetInfo.Get();
	private MeshRenderer meshRenderer;
	private Transform ring;
	private bool currentScanCompleted = false;

	public enum ScanType
	{
		Scanned,
		Object,
		Enemy,
		Boss,
		Interactable
	}

	public ScanType scanType = ScanType.Object;

	private float timer = 0.0f;
	private string scanDataName = "";

	public void Start()
	{
		ring = transform.GetChild(0);
		ring.gameObject.SetActive(false);


		meshRenderer = GetComponent<MeshRenderer>();
		meshRenderer.enabled = false;

	}
	public void Update()
	{
		meshRenderer.enabled = Scanner.InScanMode();
		meshRenderer.material.SetColor("_TintColor", info.GetColor(scanType));
	}

	public void OnStartScan()
	{
		timer = 0.0f;
		currentScanCompleted = false;

	}
	public void Scan()
	{
		timer += Time.deltaTime;
		if (timer >= info.GetTime(scanType))
		{
			if (!currentScanCompleted)
				OnScanCompleted();
		}
	}
	public void OnScanCompleted()
	{
		if (scanType != ScanType.Scanned)
			OnFirstTimeScan();
		currentScanCompleted = true;
	}
	public void OnFirstTimeScan()
	{
		if (scanType == ScanType.Interactable)
		{
			RemoveScannable();
			return;	
		}
		scanType = ScanType.Scanned;
	
		ScanDataManager.SetScanDataAvailablity(scanDataName, true);
		ScanData data;
		if (ScanDataManager.GetAcquiredScanData(scanDataName, out data))
			Debug.Log(data.subTitle);
		ScanDataIndicatorText.Display(data);
	}

	public bool IsScanned()
	{
		return (scanType == ScanType.Scanned);
	}
	public bool ScanCompleted()
	{
		return currentScanCompleted;
	}
	public void SetScanDataName(string name)
	{
		scanDataName = name;
	}
	public void RemoveScannable()
	{
		Destroy(transform.parent.GetComponent<ScannableObject>());
		Destroy(gameObject);
	}


}

public class ScannableTargetInfo
{
	private Color scannedColor;
	private Color objectColor;
	private Color enemyColor;
	private Color bossColor;
	private Color interactableColor;

	public const float scannedTime = 0.75f;
	public const float objectTime = 1.5f;
	public const float enemyTime = 2.5f;
	public const float bossTime = 3.5f;

	private ScannableTargetInfo()
	{
		ColorUtility.TryParseHtmlString("#3A3A3A", out scannedColor);
		ColorUtility.TryParseHtmlString("#AC3500", out objectColor);
		ColorUtility.TryParseHtmlString("#AC0A00", out enemyColor);
		ColorUtility.TryParseHtmlString("#3F00AC", out bossColor);
		ColorUtility.TryParseHtmlString("#008FAC", out interactableColor);
	}
	private static ScannableTargetInfo scannableTargetInfo;
	public static ScannableTargetInfo Get()
	{
		if (scannableTargetInfo == null)
			scannableTargetInfo = new ScannableTargetInfo();
		return scannableTargetInfo;
	}

	public float GetTime(ScanTarget.ScanType scanType)
	{
		switch (scanType)
		{
			case ScanTarget.ScanType.Scanned:
				return scannedTime;
			case ScanTarget.ScanType.Object:
				return objectTime;
			case ScanTarget.ScanType.Enemy:
				return enemyTime;
			case ScanTarget.ScanType.Boss:
				return bossTime;
			case ScanTarget.ScanType.Interactable:
				return enemyTime;
			default:
				return objectTime;
		}
	}

	public Color GetColor(ScanTarget.ScanType type)
	{
		switch (type)
		{
			case ScanTarget.ScanType.Scanned:
				return scannedColor;
			case ScanTarget.ScanType.Object:
				return objectColor;
			case ScanTarget.ScanType.Enemy:
				return enemyColor;
			case ScanTarget.ScanType.Boss:
				return bossColor;
			case ScanTarget.ScanType.Interactable:
				return interactableColor;
			default:
				return bossColor;
		}
	}


}
