﻿//	TODO
//	CreateFile() needs to me remade
//	need good way to create a file;

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System;
[Serializable]
public enum ScanDataType
{
	Enemy,
	Log,
	Outskirts,
	Unknown
}
[Serializable]
public class ScanData
{
	public ScanData(string title, ScanDataType type, string subTitle, string content, string imageFilePath)
	{
		this.title = title;
		this.type = type;
		this.subTitle = subTitle;
		this.content = content;
		this.imageFilePath = imageFilePath;

		acquired = false;
	}
	public ScanData(ScanData data)
	{
		this.title = data.title;
		this.type = data.type;
		this.subTitle = data.subTitle;
		this.content = data.content;
		this.imageFilePath = data.imageFilePath;
		this.acquired = data.acquired;
	}
	public string title;
	public ScanDataType type;
	public string subTitle;
	public string content;
	public string imageFilePath;
	public bool acquired;
}

public static class ScanDataManager
{
	private const string path = "Assets/TextFiles/Lore/Lore.txt";
	private static ScanData unknownData = new ScanData("Unknown", ScanDataType.Unknown, "ScanData Corrupted", "Error: Failed to load ScanData", "");

	private static StringBuilder builder = new StringBuilder();
	private static SortedDictionary<string, ScanData> loreDictionary;

	public static ScanData CreateScanData(string json)
	{
		return JsonUtility.FromJson<ScanData>(json);
	}

	private static string CreateJson(ScanData data)
	{
		return JsonUtility.ToJson(data);
	}

	public static void CreateFile()
	{
		Debug.Log("Creating Fake Parsethingy");
		List<ScanData> dataList = new List<ScanData>();
		dataList.Add(new ScanData("Gnaarg", ScanDataType.Enemy, "Scavanger of the Old Worlds", "HE HAS VERY DEEP LORE", "../Data/Goes/Here/Image/YAY.png"));
		dataList.Add(new ScanData("Gnaarg Commander", ScanDataType.Enemy, "Big Gnaarg", "HE HAS VERY DEEPER LORE THAN GNARG", "../Data/Goes/Here/Image/YAY.png"));
		dataList.Add(new ScanData("Watcher", ScanDataType.Enemy, "Remnant of the old world", "HE HAS VERY DEEPESTLORETHANALL LORE (very secret)", "../Data/Goes/Here/Image/YAY.png"));
		dataList.Add(new ScanData("Monument", ScanDataType.Outskirts, "Very pretty monument", "very fast lore go quick yo", "../Data/Goes/Here/Image/YAY.png"));
		for (int i = 0; i < dataList.Count; ++i)
		{
			builder.AppendLine(CreateJson(dataList[i]));
		}

		System.IO.File.WriteAllText(path, builder.ToString());
		builder.Length = 0;
	}
	public static void SaveScanData(SortedDictionary<string, ScanData> dic)
	{
		List<string> keys = new List<string>(dic.Keys);
		for (int i = 0; i < keys.Count; ++i)
		{
			builder.AppendLine(CreateJson(dic[keys[i]]));
		}
		System.IO.File.WriteAllText(path, builder.ToString());
		builder.Length = 0;
	}

	public static void LoadLoreDictionary()
	{
		loreDictionary = new SortedDictionary<string, ScanData>();
		List<string> allContent = new List<string>();
		System.IO.StreamReader reader = new System.IO.StreamReader(path);

		while (!reader.EndOfStream)
		{
			allContent.Add(reader.ReadLine());
		}

		for (int i = 0; i < allContent.Count; ++i)
		{
			ScanData data = CreateScanData(allContent[i]);
			loreDictionary.Add(data.title, data);
		}
	}
	public static bool GetAcquiredScanData(string dataName, out ScanData data)
	{
		if (loreDictionary.ContainsKey(dataName))
			if (loreDictionary[dataName].acquired)
			{
				data = loreDictionary[dataName];
				return true;
			}
		data = unknownData;
		return false;

	}
	/// <summary>
	/// Should not use this
	/// </summary>
	/// <param name="dataName"></param>
	/// <param name="data"></param>
	/// <returns></returns>
	public static bool GetScanData(string dataName, out ScanData data)
	{
		if (loreDictionary.ContainsKey(dataName))
		{
			data = loreDictionary[dataName];
			return true;
		}
		data = unknownData;
		return false;
	}
	public static SortedDictionary<string, ScanData> GetLoreDictionary()
	{
		return loreDictionary;
	}
	public static void SetScanDataAvailablity(string dataName, bool avaliable)
	{
		if (loreDictionary.ContainsKey(dataName))
			if (loreDictionary[dataName] != null)
				loreDictionary[dataName].acquired = avaliable;
	}
}

//public class ScanDataManager
//{

//	private static ScanDataManager instance;
//	public static ScanDataManager Get()
//	{
//		if (instance == null)
//			instance = new ScanDataManager();
//		return instance;
//	}

//	private ScanDataParser parser = new ScanDataParser();
//	public SortedDictionary<int, bool> dataAvaliable = new SortedDictionary<int, bool>();
//	private SortedDictionary<string, ScanData> loreDictionary;

//	private ScanDataManager()
//	{
//		loreDictionary = parser.LoadLoreDictionary();
//		for(int i = 0; i < loreDictionary.Count; ++i)
//		{
//			dataAvaliable.Add(loreDictionary[])
//		}
//	}

//	public void SetScanDataAcquired(int id, bool acquired)
//	{
//		dataAvaliable[id] = acquired;
//	}

//	public ScanData GetScanData(string title)
//	{
//		return loreDictionary[title];
//	}
//	public SortedDictionary<string, ScanData> GetLoreDictionary()
//	{
//		return loreDictionary;
//	}

//}