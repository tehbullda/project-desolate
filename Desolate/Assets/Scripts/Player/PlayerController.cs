﻿using UnityEngine;
using System.Collections;


public class PlayerController : MonoBehaviour
{
    private ActionQueue actionQueue;
    private LockOnSystem lockOnSystem;


    public float moveSpeed = 5.0f;
    public float turnSmoothing = 20.0f;
    public float speedDampTime = 0.5f;


    public Animator animator;
    private StateChecks stateChecks;


    private Vector3 movement;
    private Vector3 direction;

    private float currMoveSpeed = 0.0f;
    private float initzialRollBost = 1.2f;

    void Awake()
    {
        stateChecks = GetComponent<StateChecks>();
        actionQueue = GetComponent<ActionQueue>();
        lockOnSystem = GetComponent<LockOnSystem>();
    }

    void FixedUpdate()
    {
        float h = Xinput.GetAxis(Xinput.ControllerAxis.LeftX);
        float v = Xinput.GetAxis(Xinput.ControllerAxis.LeftY);
        MovementManagment(h, v);
    }

    void Update()
    {


        bool roll = false;
        bool attack = false;
        if (CanReceiveQueuedAction())
        {
            roll = (actionQueue.GetQueuedAction() == ActionQueue.QueueableActions.Rolling);
            attack = (actionQueue.GetQueuedAction() == ActionQueue.QueueableActions.LightAttack);
        }
        ActionManagment(roll, attack);

        UpdateCurrentState();

    }

    void MovementManagment(float horizontal, float vertical)
    {
        bool isRolling = stateChecks.IsInAnimation(PlayerHashIDs.rollingState);

        if (ShouldStopMovement())
        {
            horizontal = 0.0f;
            vertical = 0.0f;
        }
        if (horizontal != 0.0f || vertical != 0.0f)
        {
            Vector3 forward = Camera.main.transform.forward;
            Vector3 right = Camera.main.transform.right;
            forward.y = 0;
            right.y = 0;
            direction = (horizontal * right.normalized + vertical * forward.normalized).normalized;
            currMoveSpeed = Mathf.Lerp(currMoveSpeed, moveSpeed * direction.magnitude, speedDampTime);

            if (isRolling)
            {
                currMoveSpeed = moveSpeed * initzialRollBost;
            }
        }
        else
        {
            if (!isRolling && (horizontal == 0f || vertical == 0f))
                currMoveSpeed = Mathf.Lerp(currMoveSpeed, 0.0f, speedDampTime);
        }



        RotatePlayer(direction);
        if (lockOnSystem.IsLockedOn())
            GetComponent<Rigidbody>().MovePosition(GetComponent<Rigidbody>().position + direction * currMoveSpeed * Time.deltaTime);
        else
            GetComponent<Rigidbody>().MovePosition(GetComponent<Rigidbody>().position + transform.forward * currMoveSpeed * Time.deltaTime);


        animator.SetFloat("Speed", currMoveSpeed / moveSpeed);
    }

    void ActionManagment(bool rolling, bool lightAttack)
    {
        animator.SetBool("AttackingLight", lightAttack);
        animator.SetBool("Rolling", rolling);
    }

    void RotatePlayer(Vector3 dir)
    {
        if (stateChecks.IsInAnimation(PlayerHashIDs.rollingState))
            return;

        Vector3 target;
        if (lockOnSystem.IsLockedOn())
        {
            target = (lockOnSystem.GetTarget().position - transform.position).normalized;
            target.y = 0;
            target = target.normalized;
        }
        else
        {
            target = dir.normalized;
        }
        transform.forward = Vector3.Slerp(transform.forward, target, turnSmoothing * Time.deltaTime);
    }
    private void UpdateCurrentState()
    {
        
        if (stateChecks.IsInAnimation(PlayerHashIDs.idleState))
        {
            IdleStateUpdate();
        }
        if (stateChecks.IsInAnimation(PlayerHashIDs.locomotionState))
        {
            IdleStateUpdate();
        }
        if (stateChecks.IsInAnimation(PlayerHashIDs.rollingState))
        {
            IdleStateUpdate();
        }
        if (stateChecks.IsInAnimation(PlayerHashIDs.lightAttackState))
        {
            IdleStateUpdate();
        }
    }

    private void IdleStateUpdate()
    {

    }
    private void MovingStateUpdate()
    {

    }
    private void RollingStateUpdate()
    {

    }
    private void AttackingStateUpdate()
    {
        Debug.Log("Attacking");
    }

    //Move this
    bool CanReceiveQueuedAction()
    {
        if (
            stateChecks.IsInAnimation(PlayerHashIDs.lightAttackState) ||
           stateChecks.IsInAnimation(PlayerHashIDs.rollingState) ||
            stateChecks.IsInAnimation(PlayerHashIDs.heavyAttackState) ||
            stateChecks.IsInAnimation(PlayerHashIDs.runAttackState))
            return false;
        return true;
    }

    bool ShouldStopMovement()
    {
        if (
            stateChecks.IsInAnimation(PlayerHashIDs.lightAttackState) ||
            stateChecks.IsInAnimation(PlayerHashIDs.runAttackState)  ||
            stateChecks.IsInAnimation(PlayerHashIDs.runAttackState))
            return true;
        return false;
    }
}

