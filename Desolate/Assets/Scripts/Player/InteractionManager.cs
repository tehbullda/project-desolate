﻿using UnityEngine;
using System.Collections.Generic;

public class InteractionManager : MonoBehaviour {
	private const float angleToCheckDistance = 10f;		//When iteractables are within this angle of eachother, sort by distance instead.

	public List<IInteractable> interactablesInRange = new List<IInteractable>(4);

	public void Interact()
	{
		if (interactablesInRange.Count == 0)
			return;

		Profiler.BeginSample("Interact Sorting");
		Transform goTransform = gameObject.transform;

		//Sort the array based on angle between player and the object, to "guess" which object the player wants to interact with
		interactablesInRange.Sort((a, b) =>
		{
			float angleA = Vector3.Angle(a.gameObj.transform.position, goTransform.forward);
			float angleB = Vector3.Angle(b.gameObj.transform.position, goTransform.forward);
			float diff = angleB - angleA;

			if (diff > -angleToCheckDistance && diff < angleToCheckDistance)
			{
				Vector3 distanceA = a.gameObj.transform.position - goTransform.position;
				Vector3 distanceB = b.gameObj.transform.position - goTransform.position;

				return Mathf.RoundToInt(distanceA.sqrMagnitude - distanceB.sqrMagnitude);
			}
			
			return Mathf.RoundToInt(angleB - angleA);
		});
		Profiler.EndSample();

		interactablesInRange[0].Interact();
	}

	void Update()
	{
		if (Input.GetKeyUp(KeyCode.Keypad5))
		{
			Interact();
		}
	}

	public void AddToList(IInteractable p_interactable)
	{
		if (!interactablesInRange.Contains(p_interactable))
			interactablesInRange.Add(p_interactable);
	}

	public void RemoveFromList(IInteractable p_interactable)
	{
		if (interactablesInRange.Contains(p_interactable))
			interactablesInRange.Remove(p_interactable);
	}
}
