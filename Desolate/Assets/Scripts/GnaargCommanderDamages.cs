﻿using UnityEngine;
using System.Collections;

public class GnaargCommanderDamages : Vexe.Runtime.Types.BetterBehaviour
{
	[System.Serializable]
	public struct BossAttack
	{
		public int power;
		public bool percentageBased;
	}
	[SerializeField]
	private IDamage damageType;
	public BossAttack regularAttackDamage, heavyAttackDamage, sweepAttackDamage;
	private DamageDealer dmgDealer;
	[SerializeField]
	private EnemyAnimationHandler enemyAnimationHandler;
	// Use this for initialization
	void Start()
	{
		dmgDealer = GetComponent<DamageDealer>();
	}

	void OnTriggerEnter(Collider p_collision)
	{
		damageType.damageAmount = GetDamageAmountFromAnimation();
		damageType.destination = p_collision.gameObject;
		if (dmgDealer)
		{
			Debug.Log("Dealing damage.");
			dmgDealer.DealDamage(damageType);
		}
	}
	private int GetDamageAmountFromAnimation()
	{
		if (enemyAnimationHandler.GetCurrentState() == EnemyAnimationHandler.AnimationState.Combat_1)
			return regularAttackDamage.power;
		else if (enemyAnimationHandler.GetCurrentState() == EnemyAnimationHandler.AnimationState.Combat_2)
			return heavyAttackDamage.power;
		else if (enemyAnimationHandler.GetCurrentState() == EnemyAnimationHandler.AnimationState.Combat_3)
			return sweepAttackDamage.power;
		else
			return 0;
	}
}
