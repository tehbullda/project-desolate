﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class KeybingIcon : MonoBehaviour
{
    public Keybindings.BoolAction keyBinding;

    private Image controller;
    private Text keyboard;
    // Use this for initialization
    void Start()
    {
        controller = transform.FindChild("Controller").GetComponent<Image>();
        keyboard = transform.FindChild("Keyboard").GetComponent<Text>();
        ApplyNew();
    }

    // Update is called once per frame
    void Update()
    {
        controller.gameObject.SetActive(Keybindings.showControllerIcons);
        keyboard.gameObject.SetActive(!Keybindings.showControllerIcons);
    }
    void ApplyNew()
    {
        controller.sprite = GetKeybindIconOrString.GetControllerInputImage(keyBinding);
        keyboard.text = GetKeybindIconOrString.GetKeyboardInputString(keyBinding);
    }

}
