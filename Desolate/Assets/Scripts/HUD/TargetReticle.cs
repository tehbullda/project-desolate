﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TargetReticle : MonoBehaviour
{

    public LockOnSystem lockOnSystem;
    private RectTransform rect;
    // Use this for initialization
    public RectTransform CanvasRect;//=Canvas.GetComponent<RectTransform>();

    public Vector3 worldlyOffset;
    public Vector2 screenOffset;
    void Start()
    {
        rect = GetComponent<RectTransform>();

    }

    // Update is called once per frame
    void Update()
    {
        if (lockOnSystem.IsLockedOn())
        {
            Transform target = lockOnSystem.GetTarget();
            if (!target)
                return;
            GetComponent<Image>().enabled = true;
            Vector2 ViewportPosition = Camera.current.WorldToViewportPoint(target.position + worldlyOffset);
            Vector2 WorldObject_ScreenPosition = new Vector2(
            ((ViewportPosition.x * CanvasRect.sizeDelta.x) - (CanvasRect.sizeDelta.x * 0.5f)),
            ((ViewportPosition.y * CanvasRect.sizeDelta.y) - (CanvasRect.sizeDelta.y * 0.5f)));

            rect.anchoredPosition = WorldObject_ScreenPosition + screenOffset;

        }
        else
        {
            GetComponent<Image>().enabled = false;
        }
    }
}




