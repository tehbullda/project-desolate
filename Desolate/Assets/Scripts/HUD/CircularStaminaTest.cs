﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CircularStaminaTest : MonoBehaviour {

    public Image staminaBar;
    public Image followBar;

    float backDaming = 3.0f;

    float maxfill;
	// Use this for initialization
	void Start () {
        maxfill = GetComponent<Image>().fillAmount;
	}
    void Update()
    {
        followBar.fillAmount = Mathf.Lerp(followBar.fillAmount, staminaBar.fillAmount, backDaming * Time.deltaTime);
    }
	
    public void SetPercentage(float percentage)
    {
        staminaBar.fillAmount = Mathf.Clamp(maxfill * percentage, 0, maxfill);
        
    }

}
