﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DeactivateGameplayHUD : MonoBehaviour {

    public bool startActive = true;
    List<GameObject> children;
    private bool state = true;
	// Use this for initialization
	void Start () {
        children = new List<GameObject>();
        for(int i = 0; i < transform.childCount; ++i)
        {
            children.Add(transform.GetChild(i).gameObject);
        }

        SetActiveState(startActive);
	}
	
	// Update is called once per frame
	void Update () {
      
	}
    public void SetActiveState(bool activeState)
    {
        state = activeState;
        for(int i = 0; i < children.Count; ++i)
        {
            children[i].gameObject.SetActive(activeState);
        }
    }
    public bool IsActive()
    {
        return state;
    }
}
