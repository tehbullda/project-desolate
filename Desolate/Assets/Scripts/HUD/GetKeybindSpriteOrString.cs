﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GetKeybindIconOrString : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    public static Sprite GetControllerInputImage(Keybindings.BoolAction action)
    {
        string filename = "";
        switch (action)
        {
            case Keybindings.BoolAction.Interact:
                filename = "XboxOne_A";
                break;
            case Keybindings.BoolAction.HeavyAttack:
                filename = "XboxOne_Y";
                break;
            case Keybindings.BoolAction.LightAttack:
                filename = "XboxOne_X";
                break;
            case Keybindings.BoolAction.RollAndRun:
                filename = "XboxOne_B";
                break;
            case Keybindings.BoolAction.Heal:
                filename = "XboxOne_LB";
                break;
            case Keybindings.BoolAction.LockOn:
                filename = "XboxOne_RT";
                break;
        }
        return Resources.Load<Sprite>("Xbox One/" + filename);
    }
    public static string GetKeyboardInputString(Keybindings.BoolAction action)
    {
        KeyCode code;
        switch (action)
        {
            case Keybindings.BoolAction.Interact:
                code = (KeyCode)Keybindings.GetBoolBinding(Keybindings.BoolAction.Interact).keyboardMouseKey;
                break;
            case Keybindings.BoolAction.HeavyAttack:
                code = (KeyCode)Keybindings.GetBoolBinding(Keybindings.BoolAction.HeavyAttack).keyboardMouseKey;
                break;
            case Keybindings.BoolAction.LightAttack:
                code = (KeyCode)Keybindings.GetBoolBinding(Keybindings.BoolAction.LightAttack).keyboardMouseKey;
                break;
            case Keybindings.BoolAction.RollAndRun:
                code = (KeyCode)Keybindings.GetBoolBinding(Keybindings.BoolAction.RollAndRun).keyboardMouseKey;
                break;
            case Keybindings.BoolAction.Heal:
                code = (KeyCode)Keybindings.GetBoolBinding(Keybindings.BoolAction.Heal).keyboardMouseKey;
                break;
            case Keybindings.BoolAction.LockOn:
                code = (KeyCode)Keybindings.GetBoolBinding(Keybindings.BoolAction.LockOn).keyboardMouseKey;
                break;
            default:
                code = 0;
                break;
        }
        return TrimString(code.ToString());
    }
    private static string TrimString(string s)
    {
        if (s.Contains("Alpha"))
            return s[5].ToString();
        else
            return s;
    }
}
