﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UpgradePanel : MonoBehaviour
{


    public DeactivateGameplayHUD gameplayHUD;
    public GameObject panel;
    public Text mainText;
    public Text description;

    public Image icon;

    public bool panelActive = false;

    private float minTimeDisplayed = 0.75f;
    private float timer = 0.0f;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
		if (panelActive)
        {
            if (ContinueInput())
            {
                DisablePanel();
            }
        }

    }
    public void InitializeUpgradePanel(string bigText, string smallText)
    {
        panelActive = true;
        mainText.text = bigText;
        description.text = smallText;
        gameplayHUD.SetActiveState(false);
        panel.SetActive(true);
		TimeScale.PauseGame(true);
        //Camera.main.GetComponent<Blur>().SetBlurActive(true);
    }
    public void DisablePanel()
    {
        timer = 0.0f;
        panelActive = false;
        panel.SetActive(false);
        gameplayHUD.SetActiveState(true);
		TimeScale.PauseGame(false);
		//Camera.main.GetComponent<Blur>().SetBlurActive(false);
	}
    bool ContinueInput()
    {
        if (!CanContinue())
            return false;
        if (Keybindings.GetBoolInputDown(Keybindings.BoolAction.Interact))
            return true;
        return false;
    }
    bool CanContinue()
    {
        timer += TimeScale.GetAbsoluteDeltaTime();
        if (timer >= minTimeDisplayed)
        {
            return true;
        }
        return false;
    }
}
