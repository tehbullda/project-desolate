﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class UIBar : MonoBehaviour
{

    public RectTransform back;
    public RectTransform front;

    private float maxWidth;

    private float percentage;

    float backDamping = 3.0f;
    // Use this for initialization
    void Start()
    {
        maxWidth = GetComponent<RectTransform>().rect.width;
    }

    // Update is called once per frame
    void Update()
    {
        SetFront();
        back.offsetMax = Vector2.Lerp(back.offsetMax, front.offsetMax, backDamping * Time.deltaTime);
    }
    public void SetPercentage(float value)
    {
        percentage = value;
    }
    public void Expand(float expansion)
    {
        maxWidth += expansion;
        //Rect rect = GetComponent<RectTransform>().rect;
        //rect.width = maxWidth;
        //GetComponent<RectTransform>().rect.Set(rect.x, rect.y, rect.width, rect.height);
        //GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, maxWidth);
        //GetComponent<RectTransform>().SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, expansion*3 + 2, maxWidth)/* = new Vector2(maxWidth, GetComponent<RectTransform>().offsetMax.y)*/;
        GetComponent<RectTransform>().sizeDelta = new Vector2(maxWidth, GetComponent<RectTransform>().sizeDelta.y);
        Vector2 pos = GetComponent<RectTransform>().anchoredPosition;
        GetComponent<RectTransform>().anchoredPosition = new Vector2(maxWidth/2, pos.y);
    }
    void SetFront()
    {
        front.offsetMax = new Vector2(maxWidth * percentage - maxWidth, 0);
    }
}
