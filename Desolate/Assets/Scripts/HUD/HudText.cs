﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class HudText : MonoBehaviour
{
    public enum TextPriorityLevel
    {
        Normal,
        High
    }
    private Text text;
    private static string content;
    private static string revealedText = "";

    private static float timer = 0.0f;
    private static float textSpeed = 0.02f;

    private static bool isDone = false;

    private static float symbolPerTick = 1;
    private static TextPriorityLevel currentPriorityLevel;

    private static bool skippable = false;
    private static bool movementLocked = false;
    // Use this for initialization
    void Start()
    {
        text = GetComponent<Text>();
        content = text.text;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
            RequestHudText(TextPriorityLevel.Normal, "THIS WORKS", true, false);
        if (Input.GetKeyDown(KeyCode.Alpha2))
            RequestHudText(TextPriorityLevel.Normal, "THIS WORKSTHIS WORKSTHIS WORKSTHIS WORKSTHIS WORKSTHIS WORKSTHIS WORKSTHIS WORKSTHIS WORKSTHIS WORKSTHIS WORKSTHIS WORKS", true, false);
        if (Input.GetKeyDown(KeyCode.Return))
            RequestHudText(TextPriorityLevel.High, "THIS IS VERY IMPORTANT THIS IS VERY IMPORTANT THIS IS VERY IMPORTANT THIS IS VERY IMPORTANT THIS IS VERY IMPORTANT THIS IS VERY IMPORTANT ", true, false);
        timer += Time.deltaTime;
        if (timer >= textSpeed && !isDone)
        {
            text.text = RevealText();
            timer = 0.0f;
        }
    }
    string RevealText()
    {
        for (int i = 0; i < symbolPerTick; ++i)
        {
            if (revealedText.Length < content.Length)
                revealedText += content[revealedText.Length];
            else
            {
                isDone = true;
                Debug.Log("TextIsDone");
                break;
            }
        }

        return revealedText + "_";
    }
    //Will return true if the request had priority
    public static bool RequestHudText(TextPriorityLevel priorityLevel, string content, bool canSkip, bool lockMovement)
    {
        if (!isDone && currentPriorityLevel > priorityLevel)
            return false;
        else
        {
            isDone = false;
            HudText.content = content;
            HudText.revealedText = "";
            HudText.skippable = canSkip;
            HudText.movementLocked = lockMovement;
            HudText.timer = 0.0f;
            HudText.currentPriorityLevel = priorityLevel;
            return true;
        }
    }

}
