﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LocationIndicator : MonoBehaviour {
	private static LocationIndicator script;
	private FadeGroup fadeGroup;
	private Text locationText;
	private string locationName = "";
	// Use this for initialization
	void Start () {
		script = this;
		fadeGroup = GetComponent<FadeGroup>();
		locationText = transform.FindChild("text").GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void Display(string name)
	{
		if (locationName == name)
			return;
		StartCoroutine(DisplaySequence(name));
	}
	private IEnumerator DisplaySequence(string name)
	{
		locationName = name;
		locationText.text = name;
		fadeGroup.FadeElements(Color.white, 0.2f, 0.0f);
		yield return new WaitForSeconds(3.0f);
		fadeGroup.FadeElements(new Color(1, 1, 1, 0), 1.0f, 0.0f);
	}
	public static LocationIndicator Get()
	{
		return script;
	}
}
