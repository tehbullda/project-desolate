﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScanDataIndicatorText : MonoBehaviour
{



	private static Text title;
	private static Text subTitle;
	private static Text shortcutIndicator;

	private static FadeGroup fadeGroup;

	private static bool displaying = false;
	private bool coroutineRunning = false;
	private const float displayTime = 5.0f;
	private static float timer = 0.0f;

	private const float fadeOutSpeed = 5.0f;

	void Start()
	{
		fadeGroup = GetComponent<FadeGroup>();
		title = transform.FindChild("title").GetComponent<Text>();
		subTitle = transform.FindChild("subTitle").GetComponent<Text>();
		shortcutIndicator = transform.FindChild("shortcutIndicator").GetComponent<Text>();
		fadeGroup.FadeElements(new Color(1, 1, 1, 0), 0.1f, 0);
	}
	void Update()
	{
		if (!displaying)
			return;

		timer += Time.deltaTime;
		if (timer >= displayTime)
		{
			if (!coroutineRunning)
			{
				fadeGroup.FadeElements(new Color(1, 1, 1, 0), 1.0f, 0);
				displaying = false;
			}
		}

	}

	public static void Display(ScanData data)
	{
		displaying = true;
		title.text = data.title;
		subTitle.text = data.subTitle;

		fadeGroup.FadeElements(new Color(1, 1, 1, 1), 0.2f, 0);

		timer = 0.0f;
	}
	

}
