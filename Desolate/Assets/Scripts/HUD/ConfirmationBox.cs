﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class ConfirmationBox : MonoBehaviour
{

    private Text text;
    private Text yes;
    private Text no;

    private bool active = false;
    private bool selectionConfirmed = false;

    private bool yesSelected = false;

    private int baseSize;

    // Use this for initialization
    void Start()
    {
        text = transform.FindChild("Text").GetComponent<Text>();
        yes = transform.FindChild("Yes").GetComponent<Text>();
        no = transform.FindChild("No").GetComponent<Text>();
        baseSize = text.fontSize;
    }

    // Update is called once per frame
    void Update()
    {
        //if (Input.GetKeyDown(KeyCode.Space))
        //    active = !active;
        if (!active)
        {
            text.enabled = false;
            yes.enabled = false;
            no.enabled = false;
            transform.GetComponent<Image>().enabled = false;
            return;
        }
        text.enabled = true;
        yes.enabled = true;
        no.enabled = true;
        transform.GetComponent<Image>().enabled = true;

        if (Keybindings.GetBoolInputDown(Keybindings.BoolAction.Left) || Keybindings.GetBoolInputDown(Keybindings.BoolAction.Right))
            ToggleSelection();
        UpdateVisualSelection();
        if (Keybindings.GetBoolInputDown(Keybindings.BoolAction.Interact))
            Confirm(yesSelected);
        if (Keybindings.GetBoolInputDown(Keybindings.BoolAction.RollAndRun))
            Confirm(false);
    }
    public void InitzializeConfirmationBox(string text)
    {
        this.text.text = text;
        StartCoroutine(ActivateNextFrame()); //To not activate no immediately
        selectionConfirmed = false;
        yesSelected = false;
    }
    private void ToggleSelection()
    {
        yesSelected = !yesSelected;
    }
    private void Confirm(bool confirmation)
    {
        yesSelected = confirmation;
        selectionConfirmed = true;
    }
    void UpdateVisualSelection()
    {
        if(yesSelected)
        {
            no.fontSize = baseSize;
            yes.fontSize = (int)(baseSize * 1.2f);
        }
        else
        {
            yes.fontSize = baseSize;
            no.fontSize = (int)(baseSize * 1.2f);
        }
    }
    public bool HasConfirmed()
    {
        return selectionConfirmed;
    }
    public bool GetConfirmation()
    {
        selectionConfirmed = false;
        active = false;
        return yesSelected;
    }
    public bool BoxIsActive()
    {
        return active;
    }
    private IEnumerator ActivateNextFrame()
    {
        yield return new WaitForEndOfFrame();
        yield return new WaitForEndOfFrame();
        active = true;
    }


}
