﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class PauseMenuSelection : MonoBehaviour
{
    public Text selectionText;
    private PauseScreen pauseScreen;
    private GameObject contentPanel;
    private List<Image> selections;
    private List<GameObject> content;

    private int selected = 0;

    private Color baseColor;
    private Color deselectedColor;

    private int moveLeft = -1;
    private int moveRight = 1;

    private bool inContentPanel = false;
    private ConfirmationBox confirmatioBox;
    // Use this for initialization
    void Awake()
    {
        pauseScreen = transform.parent.GetComponent<PauseScreen>();
        confirmatioBox = GameObject.Find("ConfirmationBox").GetComponent<ConfirmationBox>();
        selections = new List<Image>();
        content = new List<GameObject>();

        contentPanel = GameObject.Find("ContentPanel");

        for (int i = 0; i < transform.childCount; ++i)
        {
            selections.Add(transform.GetChild(i).GetComponent<Image>());
        }
        baseColor = selections[0].color;
        deselectedColor = baseColor;
        deselectedColor.a -= 0.5f;

        for (int i = 0; i < selections.Count; ++i)
        {
            if (selected != i)
                Deselect(i);
        }

        for (int i = 0; i < contentPanel.transform.childCount; ++i)
        {
            content.Add(contentPanel.transform.GetChild(i).gameObject);
            ExitContentPanel(i);
        }
    }
    void OnEnable()
    {
        selected = 0;
        Select(0);
        for (int i = 1; i < selections.Count; ++i)
        {
            Deselect(i);
        }
    }

    // Update is called once per frame
    void Update()
    {
        selectionText.text = selections[selected].transform.name;
        if (confirmatioBox.BoxIsActive())
            return;

        if (Keybindings.GetBoolInputDown(Keybindings.BoolAction.Left))
        {
            MoveSelected(moveLeft);
        }
        if (Keybindings.GetBoolInputDown(Keybindings.BoolAction.Right))
        {
            MoveSelected(moveRight);
        }
        if (Keybindings.GetBoolInputDown(Keybindings.BoolAction.Interact))
        {
            EnterContentPanel(selected);
        }
        if (Keybindings.GetBoolInputDown(Keybindings.BoolAction.RollAndRun))
        {
            if (!inContentPanel)
                pauseScreen.PauseGame(false);
            ExitContentPanel(selected);
        }
    }
    void MoveSelected(int direction)
    {
        if (inContentPanel)
            return;
        Deselect(selected);
        selected += direction;
        if (selected >= selections.Count)
            selected = 0;
        if (selected < 0)
            selected = selections.Count - 1;
        Select(selected);
    }
    void Select(int selection)
    {
        selections[selection].color = baseColor;
    }
    void Deselect(int selection)
    {
        selections[selection].color = deselectedColor;
    }
    void EnterContentPanel(int selection)
    {
        if (inContentPanel)
            return;

        inContentPanel = true;
        content[selection].SetActive(true);
    }
    void ExitContentPanel(int selection)
    {
        inContentPanel = false;
        content[selection].SetActive(false);
    }
}
