﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Events;

[System.Serializable]
//public class myFloatEvent : UnityEvent<float> { }
public class PauseMenuButtonSelections : MonoBehaviour
{
    private ConfirmationBox confirmationBox;
    Text[] elements = new Text[0];
    private int elementSelected;

    private int baseSize;
    private int selectedSize;

    //[SerializeField]
    //public myFloatEvent onChangeEvent;

    void Awake()
    {
        confirmationBox = GameObject.Find("ConfirmationBox").GetComponent<ConfirmationBox>();
        elements = new Text[transform.childCount];
        for (int i = 0; i < transform.childCount; ++i)
        {
            elements[i] = transform.GetChild(i).GetComponent<Text>();
        }

        baseSize = elements[0].fontSize;
        selectedSize = (int)(baseSize * 1.2f);
    }
    void OnEnable()
    {
        elementSelected = 0;
        Select(elementSelected);
    }
    void Update()
    {
        if (confirmationBox.HasConfirmed())
        {
            if (confirmationBox.GetConfirmation() && elements[elementSelected].GetComponent<ButtonBase>())
                elements[elementSelected].GetComponent<ButtonBase>().Activate();
        }
        if (confirmationBox.BoxIsActive())
            return;
        if (Keybindings.GetBoolInputDown(Keybindings.BoolAction.Down))
        {
            SelectDown();
        }
        if (Keybindings.GetBoolInputDown(Keybindings.BoolAction.Up))
        {
            SelectUp();
        }
        if (Keybindings.GetBoolInputDown(Keybindings.BoolAction.Interact))
            ActivateButton();
    }
    void DeselectAll()
    {
        for (int i = 0; i < elements.Length; ++i)
        {
            Deselect(i);
        }
    }
    void Deselect(int elementPosInArray)
    {
        elements[elementPosInArray].fontSize = baseSize;
    }
    void Select(int elementPosInArray)
    {
        DeselectAll();
        elements[elementPosInArray].fontSize = selectedSize;
        elementSelected = elementPosInArray;
    }
    void SelectDown()
    {
        elementSelected++;
        if (elementSelected >= elements.Length)
            elementSelected = 0;
        Select(elementSelected);
    }
    void SelectUp()
    {
        elementSelected--;
        if (elementSelected < 0)
            elementSelected = elements.Length - 1;
        Select(elementSelected);
    }
    void ActivateButton()
    {
        confirmationBox.InitzializeConfirmationBox("Are you sure?");
    }
}
