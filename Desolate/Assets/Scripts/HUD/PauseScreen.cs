﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class PauseScreen : MonoBehaviour {

    bool pauseScreenActive = false;
    private GameObject selection;
    private GameObject contentPanel;
    public GameObject selectionText;

    //private Blur blur;
	// Use this for initialization
	void Start () {
        //blur = Camera.main.GetComponent<Blur>();
        selection = transform.FindChild("Selection").gameObject;
        contentPanel = transform.FindChild("ContentPanel").gameObject;

        GetComponent<Image>().enabled = false;
        selection.SetActive(false);
        contentPanel.SetActive(false);
    }
	
	// Update is called once per frame
	void Update () {
        if (Keybindings.GetBoolInputDown(Keybindings.BoolAction.Pause))
            PauseGame(true);
	}

 
    public void PauseGame(bool paused)
    {
        pauseScreenActive = paused;
        ProgressStatics.gamePaused = paused;
        TimeScale.PauseGame(paused);
        //GetComponent<Image>().enabled = paused;
        selection.SetActive(paused);
        contentPanel.SetActive(paused);
        //blur.SetBlurActive(paused);
        selectionText.SetActive(paused);
    }
    public void ShowUI()
    {
        selection.SetActive(true);
        contentPanel.SetActive(true);
        //blur.SetBlurActive(true);
        selectionText.SetActive(true);
    }
    public void HideUI()
    {
        selection.SetActive(false);
        contentPanel.SetActive(false);
        //blur.SetBlurActive(false);
        selectionText.SetActive(false);
    }
}
