﻿using UnityEngine;
using System.Collections;

public class EndgameTrigger : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player" && ProgressStatics.bossdefeated)
        {
            ProgressStatics.gameComplete = true;
        }
    }
}
