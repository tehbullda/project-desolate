﻿using UnityEngine;
using System.Collections;
using System;

public class TestBuffOne : AuraBase {
    TestBuffOne() {
        duration = 5.0f;
    }

    private Stamina stamina;

    public static new int ID {
        get {
            return 0;
        }
    }

    public override AuraType type {
        get {
            return AuraType.Buff;
        }
    }

    void Awake() {
        stamina = FindObjectOfType<Stamina>();
    }

    public override void OnApply() {
        stamina.MaxStamina += 20f;
    }

    public override void OnRemove() {
        stamina.MaxStamina -= 20f;
    }

    public override void Update () {
        stamina.CurrentStamina -= 1f;

        timer += Time.deltaTime;
	}
}
