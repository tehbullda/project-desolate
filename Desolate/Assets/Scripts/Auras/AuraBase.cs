﻿using UnityEngine;

public abstract class AuraBase : ScriptableObject {
    public enum AuraType {
        Buff,
        Debuff
    }

    //public bool isVisible = false;
    //public Sprite sprite = null;
    public static int ID { get { return -1; } }
    public abstract AuraType type { get; }

    public virtual void OnApply() { }
    public virtual void OnRemove() { }
    public virtual void OnDispel() { }

    public virtual void Update() { }

    protected float timer = 0.0f;
    protected float duration = 0.0f;

    public float RemainingTimer {
        get {
            return duration - timer;
        }
    }
}
    