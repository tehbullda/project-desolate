﻿using UnityEngine;
using System;
using System.Collections.Generic;

public class AuraHandler : MonoBehaviour {
    private List<AuraBase> buffs = new List<AuraBase>(16);
    private List<AuraBase> debuffs = new List<AuraBase>(16);
    private Type[] inheritingTypes;

	void Awake() {
        inheritingTypes = TypeFinder.FindInheritingTypesSharingAssembly(typeof(AuraBase));
    }
	
	void Update() {
#if UNITY_EDITOR
		if (Input.GetKeyUp(KeyCode.Period)) {
            ApplyAura(0);
        }
#endif
		for (int i = 0; i < buffs.Count; ++i) {
            buffs[i].Update();
        }

        for (int i = 0; i < debuffs.Count; ++i) {
            debuffs[i].Update();
        }
	}

    void LateUpdate() {
        RemoveTimedOutAuras();
    }

    public void ApplyAura(int p_auraNumber) {
        for (int i = 0; i < inheritingTypes.Length; ++i) {
            int ID = (int)inheritingTypes[i].GetProperty("ID").GetValue(null, null);

            if (p_auraNumber == ID) {
                AuraBase temp = (AuraBase)ScriptableObject.CreateInstance(inheritingTypes[i]);

                if (temp.type == AuraBase.AuraType.Buff) {
                    buffs.Add(temp);
                }
                else {
                    debuffs.Add(temp);
                }
            }
        }
    }

    public void RemoveAura(object p_obj) {

    }

    private void RemoveTimedOutAuras() {
        for (int i = 0; i < buffs.Count; ++i) {
            if (buffs[i].RemainingTimer <= 0.0005f) {
                buffs.RemoveAt(i);
            }
        }

        for (int i = 0; i < debuffs.Count; ++i) {
            if (debuffs[i].RemainingTimer <= 0.0005f) {
                debuffs[i].OnRemove();
                debuffs.RemoveAt(i);
            }
        }
    }

    private bool Approximately(float lhs, float rhs) {
        float retVal = Mathf.Abs(lhs - rhs);
        return (retVal <= 0.0005);
    }
}
