﻿using UnityEngine;
using System.Collections;

public class TargetIndicator : MonoBehaviour
{
    public LockOnSystem lockOnSystem;
    public Vector3 idlePosition;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (lockOnSystem.IsLockedOn())
        {
            Transform targ = lockOnSystem.GetTarget();
            transform.position = targ.position + Vector3.up * 0.7f;
        }
        else
        {
            transform.position = idlePosition;
        }
    }
}
