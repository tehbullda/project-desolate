﻿using UnityEngine;
using System.Collections;

public class InWorldProgressBar : MonoBehaviour {
    private Transform bar;
    private float yScaleMax;

    private float percentageFilled = 1.0f;
	// Use this for initialization
	void Start () {

        bar = transform.GetChild(0);
        yScaleMax = bar.localScale.y;

        
	}
	
	// Update is called once per frame
	void Update () {
        bar.localScale = new Vector3(bar.localScale.x, yScaleMax * percentageFilled, bar.localScale.z);

        Vector3 direction = -Camera.main.transform.forward;
        direction.y = 0;
        direction = direction.normalized;
        transform.forward = direction;

	}
    public void SetPercentageFilled(float value)
    {
        percentageFilled = value;
    }
    public void SetActive(bool active)
    {
        transform.GetComponent<MeshRenderer>().enabled = active;
        bar.transform.GetChild(0).GetComponent<MeshRenderer>().enabled = active;
    }
    
}
