﻿using UnityEngine;
using System.Collections;

public class InWorldHPBar : MonoBehaviour
{
    private Transform bar;
    private float yScaleMax;

    private float percentageFilled = 1.0f;
	[SerializeField]
	private Stats stats;
    // Use this for initialization
    void Start()
    {

        bar = transform.GetChild(0);
        yScaleMax = bar.localScale.y;
        

    }

    // Update is called once per frame
    void Update()
    {
		SetPercentageFilled((float)stats.health.Current / (float)stats.health.Max);
        bar.localScale = new Vector3(bar.localScale.x, yScaleMax * percentageFilled, bar.localScale.z);

        Vector3 direction = -Camera.main.transform.forward;
        direction.y = 0;
        direction = direction.normalized;
        transform.forward = direction;

        if(percentageFilled <= 0.0f)
        {
            FadeOut();
        }

    }
    public void SetPercentageFilled(float value)
    {
        percentageFilled = value;
    }
    public void SetActive(bool active)
    {
        transform.GetComponent<MeshRenderer>().enabled = active;
        bar.transform.GetChild(0).GetComponent<MeshRenderer>().enabled = active;
    }
    void FadeOut()
    {
        Color col = transform.GetComponent<Renderer>().material.color;
        col.a = Mathf.Lerp(col.a, 0, Time.deltaTime * 4);
        transform.GetComponent<Renderer>().material.color = col;
    }

}
