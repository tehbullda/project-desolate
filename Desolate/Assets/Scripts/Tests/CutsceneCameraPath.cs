﻿using UnityEngine;
using UnityEditor;
using System.Collections;

public class CutsceneCameraPath : MonoBehaviour {
	[SerializeField] private GameObject startPoint;
	[SerializeField] private GameObject endPoint;
	[SerializeField] private CutsceneCameraPath next;

	[Tooltip("If no camera is specified, the main camera will be used")]
	[SerializeField] private new Camera camera;

	[Tooltip("How long does it take to follow this path")]
	[SerializeField] private float duration;

	private bool moving = false;
	private float timeSpent = 0f;

	public void StartMovement()
	{
		timeSpent = 0f;
		moving = true;

		if (camera == null)
			camera = Camera.main;

		camera.gameObject.SetActive(true);
		camera.transform.position = startPoint.transform.position;
		camera.transform.rotation = startPoint.transform.rotation;
	}

	void Update()
	{
		if (!moving)
			return;

		float lerp = Mathf.InverseLerp(0, duration, timeSpent);
		camera.transform.position = Vector3.Lerp(startPoint.transform.position, endPoint.transform.position, lerp);
		camera.transform.rotation = Quaternion.Slerp(startPoint.transform.rotation, endPoint.transform.rotation, lerp);

		timeSpent += Time.deltaTime;
		float distance = Vector3.Distance(camera.transform.position, endPoint.transform.position);

		if (distance <= 0.0005f)
			FinishMovement();
	}

	public void FinishMovement()
	{
		moving = false;

		if (next != null)
			next.StartMovement();

		//If done, unlock movement etc
	}

#if (UNITY_EDITOR)
	[DrawGizmo(GizmoType.Selected | GizmoType.Active | GizmoType.NotInSelectionHierarchy)]
	private static void OnDrawGizmos(CutsceneCameraPath script, GizmoType gizmoType)
	{
		if ((gizmoType & GizmoType.NotInSelectionHierarchy) != 0 && !script.transform.IsChildSelected())
			return;

		Gizmos.DrawSphere(script.startPoint.transform.position, 0.1f);
		Gizmos.DrawSphere(script.endPoint.transform.position, 0.1f);

		Gizmos.DrawLine(script.startPoint.transform.position, script.endPoint.transform.position);
	}
#endif
}
