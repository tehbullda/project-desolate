﻿using UnityEngine;
using System.Collections;

public class GnaargEyes : MonoBehaviour
{
	private float frequency = 0.05f;
	private Renderer renderer;
	bool activeState = true;

	float timer = 0.0f;

	private Color activeColor;
	private Color inactiveColor;

	public enum State
	{
		Active,
		Inactive,
		Flickering,
		Activating,
		Deactivating
	}
	private State currentState = State.Active;

	// Use this for initialization
	void Start()
	{
		renderer = GetComponent<Renderer>();
		activeColor = renderer.material.GetColor("_EmissionColor");
		inactiveColor = renderer.material.GetColor("_EmissionColor") * (0.3f * Color.white);
	}

	// Update is called once per frame
	void Update()
	{
		if (Input.GetKeyDown(KeyCode.Alpha7))
			StartCoroutine(SetActive(!activeState));
	}
	public void SetState(State state)
	{
		currentState = state;
		switch (currentState)
		{
			case State.Active:

				break;
			case State.Inactive:

				break;
			case State.Flickering:
				break;
			case State.Activating:
				StartCoroutine(SetActive(true));
				break;
			case State.Deactivating:
				StartCoroutine(SetActive(false));
				break;
			default:
				break;
		}
	}
	public IEnumerator SetActive(bool active)
	{
		float timer = 1.0f;
		while (timer > 0.0f)
		{
			timer -= Time.deltaTime;
			Flicker();
			yield return new WaitForEndOfFrame();
		}

		activeState = active;
		currentState = activeState ? State.Activating : State.Inactive;
		SetColor(activeState);
	}
	public void Flicker()
	{
		timer += Time.deltaTime;
		if (timer > frequency)
		{
			timer = 0.0f;
			float value = Random.Range(0, 2);
			activeState = value == 0 ? false : true;
			SetColor(activeState);
		}

	}
	private void SetColor(bool active)
	{
		renderer.material.SetColor("_EmissionColor", active == true ? activeColor : inactiveColor);
	}
}
