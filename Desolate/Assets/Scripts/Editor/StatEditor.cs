﻿using UnityEngine;
using UnityEditor;
using System.IO;
using System.Collections.Generic;
using System.Reflection;

public class StatEditor : EditorWindow
{
	private Vector2 scrollPosition = Vector2.zero;
	private Vector2 buttonSize = new Vector2(0, 40);
	public const string ASSET_PATH = "Assets/Prefabs/Balancing/Stats/";

	private static StatEditor window;
	private static Dictionary<string, Object> stats = new Dictionary<string, Object>(32);
	private static Dictionary<Object, string> inverseStats = new Dictionary<Object, string>(32);
	private static Object currentObject = null;
	private static int selectedIndex = 0;
	private static StatCreatorWindow scw;
	private static string filter;

	[MenuItem("Window/Stat Editor")]
	public static void ShowWindow()
	{
		stats.Clear();
		inverseStats.Clear();
		filter = "";

		window = GetWindow<StatEditor>();
		GetExistingAssets();

	}

	void OnGUI()
	{
		if (window == null)
			ShowWindow();

		buttonSize.Set(position.width / 3f - 20, 40);

		if (GUI.Button(new Rect(10, 3, 20, 15), "+"))
		{
			if (scw == null)
			{
				scw = ScriptableObject.CreateInstance<StatCreatorWindow>();
				scw.ShowUtility();
			}
			else
			{
				scw.Close();
				scw = null;
			}
		}

		if (GUI.Button(new Rect(32, 3, 20, 15), "-"))
		{
			string path = inverseStats[currentObject];
			AssetDatabase.DeleteAsset(path);
			UpdateScrollView();

			if (selectedIndex == 0)
				selectedIndex = 0;
			else if (selectedIndex >= stats.Count)
				selectedIndex = selectedIndex - 1;
			//selectedIndex = selectedIndex == 0 ? 0 : selectedIndex - 1;
		}

		filter = GUI.TextField(new Rect(55, 3, buttonSize.x - 45, 15), filter);

		PopulateScrollView();

		if (currentObject != null)
			PopulateFieldView();

	}

	static void GetExistingAssets()
	{
		string[] files = Directory.GetFiles(ASSET_PATH, "*.asset");

		for (int i = 0; i < files.Length; ++i)
		{
			if (!stats.ContainsKey(files[i]))
			{
				stats.Add(files[i], AssetDatabase.LoadAssetAtPath<BaseStats>(files[i]));
				inverseStats.Add(stats[files[i]], files[i]);
			}
		}
	}

	public void UpdateScrollView()
	{
		stats.Clear();
		inverseStats.Clear();

		GetExistingAssets();
	}

	void PopulateScrollView()
	{
		List<string> keys = new List<string>(stats.Keys);
		List<string> filteredKeys = keys.FindAll(s => s.Contains(filter));

		scrollPosition = GUI.BeginScrollView(new Rect(10, 20, position.width / 3f, position.height), scrollPosition, new Rect(0, 0, buttonSize.x + 20, stats.Count * buttonSize.y));

		string[] texts = filteredKeys.ToArray().RemoveFromAll(ASSET_PATH).RemoveFromAll(".asset");

		selectedIndex = GUI.SelectionGrid(new Rect(0, 0, buttonSize.x, filteredKeys.Count * buttonSize.y), selectedIndex, texts, 1);
		currentObject = stats[keys[selectedIndex]];

		GUI.EndScrollView();
	}

	void PopulateFieldView()
	{
		System.Type type = currentObject.GetType();
		FieldInfo[] fields = type.GetFields();
		List<FieldInfo> unhandledTypes = new List<FieldInfo>(fields.Length);
		float yPosition = 20f;

		for (int i = 0; i < fields.Length; ++i)
		{
			switch (fields[i].FieldType.Name)
			{
				case "Int32":
				{

					int value = EditorGUI.IntField(new Rect(position.width / 3f + 40, yPosition, ((position.width / 3f) * 2 - 60), 16), fields[i].Name.FormatCamelCase(), (int)fields[i].GetValue(currentObject));
					fields[i].SetValue(currentObject, value);	
					yPosition += 20;
					break;
				}

				case "Single":
				{
					float value = EditorGUI.FloatField(new Rect(position.width / 3f + 40, yPosition, ((position.width / 3f) * 2 - 60), 16), fields[i].Name.FormatCamelCase(), (float)fields[i].GetValue(currentObject));
					fields[i].SetValue(currentObject, value);
					yPosition += 20;
					break;
				}

				default:
				{
					unhandledTypes.Add(fields[i]);
					break;
				}
			}
		}

		if (unhandledTypes.Count != 0)
		{
			yPosition += 20f;
			GUI.Label(new Rect(position.width / 3f + 40, yPosition, (position.width / 3f) * 2 - 40, 16), "The following variables are not handled - inform Jonas!");
			yPosition += 20f;

			for (int i = 0; i < unhandledTypes.Count; ++i)
			{
				GUI.Label(new Rect(position.width / 3f + 40, yPosition, (position.width / 3f) * 2 - 40, 16), string.Format("{0} - {1}", unhandledTypes[i].Name, unhandledTypes[i].FieldType.ToString()));
				yPosition += 20f;
			}
		}
	}
}

public class StatCreatorWindow : EditorWindow
{
	public static string assetName = "";

	void OnGUI()
	{
		assetName = EditorGUILayout.TextField("Name: ", assetName);

		if (GUILayout.Button("Create"))
		{
			string filePath = StatEditor.ASSET_PATH + assetName + ".asset";

			if (!File.Exists(filePath))
				AssetDatabase.CreateAsset(CreateInstance<BaseStats>(), StatEditor.ASSET_PATH + assetName + ".asset");
			else
				Debug.LogErrorFormat("Stat \"{0}\" already exists! Choose a new name!", assetName);

			assetName = "";
			GetWindow<StatEditor>().UpdateScrollView();
		}
	}

	void OnInspectorUpdate()
	{
		Repaint();
	}
}