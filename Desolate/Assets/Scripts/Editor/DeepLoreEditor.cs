﻿using UnityEngine;
using UnityEditor;
using System.IO;
using System.Collections.Generic;

public class DeepLoreEditor : EditorWindow
{
	public const string ASSET_PATH = "Assets/TextFiles/Lore/Lore.txt";

	public static DeepLoreEditor window;
	public static SortedDictionary<string, ScanData> uneditedLoreDictionary;
	public static SortedDictionary<string, ScanData> loreDictionary;
	public static List<string> keys;

	private Vector2 scrollPosition = Vector2.zero;
	private Vector2 buttonSize = new Vector2(0, 40);
	private static ScanData currentObject = null;
	private static int selectedIndex = 0;

	private static Color defaultColor;

	private static GUIStyle guiStyle;

	private float edgePadding = 30;

	[MenuItem("Window/DeepLore™ Editor")]
	public static void ShowWindow()
	{
		window = (DeepLoreEditor)EditorWindow.GetWindow<DeepLoreEditor>();
		LoadLoreDictionary();
		guiStyle = new GUIStyle();
		guiStyle.richText = true;
	}
	public void OnGUI()
	{
		if (window == null)
			ShowWindow();

		buttonSize.Set(position.width / 3f - 20, 40);

		PopulateScrollView();
		if (currentObject != null)
			PopulateFieldView();

		PopulateSaveLoadView();
	}

	static void LoadLoreDictionary()
	{
		defaultColor = GUI.color;
		loreDictionary = new SortedDictionary<string, ScanData>();
		List<string> allContent = new List<string>();
		System.IO.StreamReader reader = new System.IO.StreamReader(ASSET_PATH);

		while (!reader.EndOfStream)
		{
			allContent.Add(reader.ReadLine());
		}

		for (int i = 0; i < allContent.Count; ++i)
		{
			ScanData data = ScanDataManager.CreateScanData(allContent[i]);
			loreDictionary.Add(data.title, data);
		}
		uneditedLoreDictionary = GetNewDictionaryWithCopiedValues(loreDictionary);
	}

	void PopulateScrollView()
	{
		scrollPosition = GUI.BeginScrollView(new Rect(10, 20, position.width / 3f, position.height), scrollPosition, new Rect(0, 0, buttonSize.x + 20, loreDictionary.Count * buttonSize.y));
		keys = new List<string>(loreDictionary.Keys);

		selectedIndex = GUI.SelectionGrid(new Rect(0, 0, buttonSize.x, loreDictionary.Count * buttonSize.y), selectedIndex, keys.ToArray(), 1);
		currentObject = loreDictionary[keys[selectedIndex]];

		GUI.EndScrollView();
	}

	void PopulateFieldView()
	{

		float width = (((position.width / 3)) * 2) - edgePadding * 2;
		float height = (position.height / 4) * 3;

		GUI.BeginGroup(new Rect((int)(position.width / 3f) + edgePadding, 0, width, (int)height));

		EditorGUILayout.LabelField("Title", EditorStyles.boldLabel);
		currentObject.title = EditorGUILayout.TextField(currentObject.title, GUILayout.Width(width - 5));
		EditorGUILayout.LabelField("Subtitle", EditorStyles.boldLabel);
		currentObject.subTitle = EditorGUILayout.TextField(currentObject.subTitle, GUILayout.Width(width - 5));
		EditorGUILayout.LabelField("Content", EditorStyles.boldLabel);
		currentObject.content = EditorGUILayout.TextArea(currentObject.content, GUILayout.Width(width - 5), GUILayout.MinHeight(64));

		GUILayout.BeginHorizontal();
		string acquired = currentObject.acquired ? "<b><color=green>ScanData Acquired</color></b>" : "<b><color=maroon>ScanData not Acquired</color></b>";
		currentObject.acquired = EditorGUILayout.Toggle(currentObject.acquired, GUILayout.MaxWidth(20));
		EditorGUILayout.LabelField(acquired, guiStyle, GUILayout.MaxWidth(300));
		GUILayout.EndHorizontal();


		EditorGUILayout.LabelField("\nSaving", EditorStyles.boldLabel);
		if (GUILayout.Button("Save", GUILayout.Width(width / 2), GUILayout.MaxWidth(120)))
			SaveCurrentData(keys[selectedIndex]);
		if (GUILayout.Button("Reset", GUILayout.Width(width / 2), GUILayout.MaxWidth(120)))
			ResetCurrentData(keys[selectedIndex]);

		GUI.EndGroup();
	}
	void PopulateSaveLoadView()
	{
		Vector2 labelSize = new Vector2(150, 50);
		Vector2 saveButtonSize = new Vector2(90, 16);
		int padding = -5;

		EditorGUI.LabelField(new Rect(position.width - labelSize.x, position.height - labelSize.y, labelSize.x, labelSize.y), "Saving and Loading", EditorStyles.boldLabel);
		if (GUI.Button(new Rect(position.width - saveButtonSize.x, position.height - saveButtonSize.y, saveButtonSize.x, saveButtonSize.y), "Save All"))
			SaveAllData();
		if (GUI.Button(new Rect(position.width - saveButtonSize.x * 2 + padding, position.height - saveButtonSize.y, saveButtonSize.x, saveButtonSize.y), "Reset All"))
			ResetAll();

		//GUI.EndGroup();
	}
	void SaveCurrentData(string key)
	{
		Dictionary<string, ScanData> tempDictionary = new Dictionary<string, ScanData>(uneditedLoreDictionary);
		tempDictionary[key] = currentObject;
	}
	void ResetCurrentData(string key)
	{
		loreDictionary[key] = uneditedLoreDictionary[key];
	}
	void ResetAll()
	{
		loreDictionary = uneditedLoreDictionary;
	}
	void SaveAllData()
	{
		ScanDataManager.SaveScanData(loreDictionary);
		LoadLoreDictionary();
	}
	public static SortedDictionary<string, ScanData> GetNewDictionaryWithCopiedValues(SortedDictionary<string, ScanData> dic)
	{
		SortedDictionary<string, ScanData> ret = new SortedDictionary<string, ScanData>();
		List<string> k = new List<string>(dic.Keys);
		for(int i = 0; i < k.Count; ++i)
		{
			ret.Add(k[i], new ScanData(dic[k[i]]));
		}
		return ret;
	}
}
