﻿using UnityEngine;
using System.Collections;

public class LeverController : MonoBehaviour {

    public GameObject leverObject;
    public Vector3 inactivePosition;
    public Vector3 activePosition;
    public bool activated;
    private float transitionSpeed = 1.0f;
	// Use this for initialization
	void Start () {
        activated = false;
        leverObject.transform.localRotation = Quaternion.Euler(inactivePosition);
	}
	
	// Update is called once per frame
	void Update () {
	    if (activated)
        {
            leverObject.transform.localRotation = Quaternion.Lerp(leverObject.transform.localRotation, Quaternion.Euler(activePosition), transitionSpeed * Time.deltaTime);
            if (Quaternion.Angle(leverObject.transform.localRotation, Quaternion.Euler(activePosition)) < 5.0f) //close enough
            {
                activated = false;
            }
        }
        else
        {
            leverObject.transform.localRotation = Quaternion.Lerp(leverObject.transform.localRotation, Quaternion.Euler(inactivePosition), transitionSpeed * Time.deltaTime);
        }
    }
}
