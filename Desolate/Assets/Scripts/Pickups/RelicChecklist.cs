﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class RelicChecklist : MonoBehaviour {

	public enum Relics
	{
		Test,
		ChargeAttack,
		Key,
		Scanner,
		None
	}
	public struct StoredRelic
	{
		public Relics type;
		public int index;
	}
	private static List<StoredRelic> relicsfound = new List<StoredRelic>();
	private const string saveTagTotal = "Total Relics Found";
	private const string saveTagRelic = "Relic";
	void Awake () {
		//relicsfound = new Relic[GameObject.FindObjectsOfType<Relic>().Length];
		//relicsfound = GameObject.FindObjectsOfType<Relic>();
		//if (SaveManager.CheckTagExists(saveTagTotal))
		//{
		//    int totalrelicsfound = SaveManager.ReadData<int>(saveTagTotal);
		//    for (int i = 0; i < totalrelicsfound; ++i)
		//    {
		//        string readtext = SaveManager.ReadData<string>(saveTagRelic + i);
		//        StoredRelic tmp = new StoredRelic();
		//        string type = readtext.Substring(0, readtext.IndexOf(" "));
		//        string value = readtext.Substring(readtext.IndexOf(" ") + 1, readtext.Length - readtext.IndexOf(" ") - 1);
		//        if (type == "Key")
		//            tmp.type = Relics.Key;
		//        else if (type == "ChargeAttack")
		//            tmp.type = Relics.ChargeAttack;
		//        else
		//            tmp.type = Relics.Test;
		//        tmp.index = System.Convert.ToInt32(value);
		//        relicsfound.Add(tmp);
		//    }
		//}
	}

	// Update is called once per frame
	void Update () {
		
	}
	void OnDestroy()
	{
		//SaveManager.SaveData<int>(relicsfound.Count, saveTagTotal);
		//for (int i = 0; i < relicsfound.Count; ++i)
		//{
		//    string tmp = relicsfound[i].type.ToString() + " " + relicsfound[i].index;
		//    SaveManager.SaveData<string>(tmp, saveTagRelic + i);
		//}
	}
	public static bool HasCollected(Relics type, int value)
	{
		for (int i = 0; i < relicsfound.Count; ++i)
		{
			if (relicsfound[i].type == type)
			{
				if (relicsfound[i].index == value)
				{
					return true;
				}
			}
		}
		return false;
	}
	//public bool HasCollected(Relics type)
	//{
	//    for (int i = 0; i < relicsfound.Count; ++i)
	//    {
	//        if (relicsfound[i].type == type)
	//        {
	//            return relicsfound[i].collected;
	//        }
	//    }
	//    return false;
	//}
	public static void ReportRelic(RelicChecklist.Relics type, int index)
	{
		for (int i = 0; i < relicsfound.Count; ++i)
		{
			if (relicsfound[i].type == type && relicsfound[i].index == index)
			{
				return;
			}
		}
		StoredRelic newRelic = new StoredRelic();
		newRelic.type = type;
		newRelic.index = index;
		relicsfound.Add(newRelic);
	}
}
