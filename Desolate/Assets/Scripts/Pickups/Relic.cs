﻿using UnityEngine;
using System.Collections;

public class Relic : MonoBehaviour {

	bool collected = false;
	public RelicChecklist.Relics type;
	public int value;
	public string mainText;
	public string explanationText;
	private UpgradePanel panelScript;
	// Use this for initialization
	void Start () {
		//if (SaveManager.CheckTagExists(type.ToString() + value))
		//{
		//	collected = SaveManager.ReadData<bool>(type.ToString() + value);
		//	if (collected)
		//	{
		//		gameObject.SetActive(false);
		//	}
		//}
		panelScript = GameObject.Find("UpgradePanel").GetComponent<UpgradePanel>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.tag == "Player" && !collected)
		{
			collected = true;
			//SaveManager.SaveData<bool>(collected, type.ToString() + value);
			panelScript.InitializeUpgradePanel(mainText, explanationText);
			gameObject.SetActive(false);
			RelicChecklist.ReportRelic(type, value);
		}
	}
	//private void CreateCustomPanelMessage()
	//{
	//	if (type == RelicChecklist.Relics.ChargeAttack && value == 1)
	//	{
	//		panelScript.InitializeUpgradePanel("Weapon Charge System Acquired", "Hold Y to charge up your Heavy Attacks");
	//	}
	//	if (type == RelicChecklist.Relics.Key && value == 1)
	//	{
	//		panelScript.InitializeUpgradePanel("Key acquired", "This can be used to open various doors");
	//	}
	//}
	public bool HasBeenCollected()
	{
		return collected;
	}
	public int GetValue()
	{
		return value;
	}
}
