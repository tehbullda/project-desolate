﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LevelEditor : MonoBehaviour
{

    public Vector2 size;
    public Transform templateGround;

    
    // Use this for initialization
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void Build()
    {

        for (int i = 0; i < transform.childCount; ++i)
        {
           if(transform.GetChild(i).tag != "Template")
           {
               GameObject.DestroyImmediate(transform.GetChild(i));
               Debug.Log(tag);
           }
        }


        
        GameObject go;
        for (int i = 0; i < size.x; ++i)
        {
            for (int j = 0; j < size.y; ++j)
            {

                go = Instantiate(templateGround, new Vector3(size.x * 1.1f, 0, size.y * 1.1f), Quaternion.identity) as GameObject;
                go.transform.rotation = Quaternion.Euler(0, 2, 10);
                //go.tag = "LevelGeometry";
                
            }
        }
    }
}
