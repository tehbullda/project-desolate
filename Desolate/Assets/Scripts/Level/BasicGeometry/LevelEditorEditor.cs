﻿using UnityEngine;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;


[CustomEditor(typeof(LevelEditor))]
public class LevelEditorEditor : Editor
{

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        LevelEditor levelEditor = (LevelEditor)target;
        if(GUILayout.Button("Build"))
        {
            levelEditor.Build();
        }
    }
}
#endif
