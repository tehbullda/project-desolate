﻿using UnityEngine;
using System.Collections;

public class Savepoint : MonoBehaviour {
    public string savepointName;
    public ParticleSystem smoke;
    public ParticleSystem glowEffect;
    public GameObject projector;

    private bool activated = false;
    private Renderer currentRenderer;
    private float emissionColorVal = 0.0f;
    //public GameObject 

	// Use this for initialization
	void Start () {
        currentRenderer = GetComponent<Renderer>();
	}
	
	// Update is called once per frame
	void Update () {
        PingPongEmmission();
	}
    void OnTriggerStay(Collider other)
    {
        if(other.tag == "Player")
        {
            if(Keybindings.GetBoolInputDown(Keybindings.BoolAction.Interact))
            {
                if (activated)
                    Interact();
                else
                    ActivateSavepoint();

            }
        }
    }
    void ActivateSavepoint()
    {
        activated = true;
        glowEffect.Play();
        projector.SetActive(true);
    }
    void Interact()
    {

    }
    void PingPongEmmission()
    {
        emissionColorVal = Mathf.PingPong(Time.time / 3, 0.5f);
        currentRenderer.material.SetColor("_EmissionColor", new Color(emissionColorVal, emissionColorVal, emissionColorVal, 1));
    }
}
