﻿using UnityEngine;
using System.Collections;

public class KillPlane : MonoBehaviour {
	void OnTriggerEnter(Collider p_collider)
	{
		if (p_collider.tag == "Player")
		{
			p_collider.GetComponent<Health>().ReduceHealth(100000.1f);
		}
		else if (!p_collider.tag.Contains("Player"))
		{
			Destroy(p_collider.gameObject);
		}
	}
}
