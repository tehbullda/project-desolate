﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Keybindings : MonoBehaviour
{
	//
	//Controller movement hardcoded to left stick
	//
	private Xinput xinput;

	public struct BoolBinding
	{
		public BoolAction action;
		public int xinputKey;
		public int keyboardMouseKey;

		public BoolBinding(BoolAction Action, int XInputKey, int KeyboardMouseKey)
		{
			action = Action;
			xinputKey = XInputKey;
			keyboardMouseKey = KeyboardMouseKey;
		}
	}
	public struct FloatBinding
	{
		public FloatAction action;
		public int keyboardMouseKey;


		public FloatBinding(FloatAction Action, int KeyboardMouseKey)
		{
			action = Action;
			keyboardMouseKey = KeyboardMouseKey;
		}
	}
	public enum BoolAction
	{
		LightAttack,
		HeavyAttack,
		RollAndRun,
		Heal,
		LockOn,
		Interact,
		Pause,
		Up,
		Down,
		Left,
		Right,
		Scanner
	}
	public enum FloatAction
	{
		MoveUp,
		MoveDown,
		MoveRight,
		MoveLeft
	}

	static private List<BoolBinding> boolBindings = new List<BoolBinding>();
	static private List<FloatBinding> floatBindings = new List<FloatBinding>();

	static public bool showControllerIcons = true;

	void Start()
	{
		if (GameObject.Find("_Globals"))
			xinput = GameObject.Find("_Globals").GetComponent<Xinput>();
		if (!xinput)
			xinput = GetComponent<Xinput>();
		if (!xinput)
		{
			Debug.LogError("Keybindings.cs couldn't find Globals or Xinput");
		}

		SetDefaults();
	}
	void SetDefaults()
	{
		boolBindings.Add(new BoolBinding(BoolAction.LightAttack, (int)Xinput.ControllerButton.X, (int)KeyCode.Alpha3));
		boolBindings.Add(new BoolBinding(BoolAction.HeavyAttack, (int)Xinput.ControllerButton.Y, (int)KeyCode.Alpha4));
		boolBindings.Add(new BoolBinding(BoolAction.Heal, (int)Xinput.ControllerButton.RightBumber, (int)KeyCode.Q));
		boolBindings.Add(new BoolBinding(BoolAction.LockOn, (int)Xinput.ControllerButton.RightTrigger, (int)KeyCode.Tab));
		boolBindings.Add(new BoolBinding(BoolAction.RollAndRun, (int)Xinput.ControllerButton.B, (int)KeyCode.Space));
		boolBindings.Add(new BoolBinding(BoolAction.Interact, (int)Xinput.ControllerButton.A, (int)KeyCode.E));
		boolBindings.Add(new BoolBinding(BoolAction.Pause, (int)Xinput.ControllerButton.Start, (int)KeyCode.Escape));
		boolBindings.Add(new BoolBinding(BoolAction.Scanner, (int)Xinput.ControllerButton.LeftBumber, (int)KeyCode.T));

		boolBindings.Add(new BoolBinding(BoolAction.Up, (int)Xinput.ControllerButton.DPadUp, (int)KeyCode.W));
		boolBindings.Add(new BoolBinding(BoolAction.Down, (int)Xinput.ControllerButton.DPadDown, (int)KeyCode.S));
		boolBindings.Add(new BoolBinding(BoolAction.Left, (int)Xinput.ControllerButton.DPadLeft, (int)KeyCode.A));
		boolBindings.Add(new BoolBinding(BoolAction.Right, (int)Xinput.ControllerButton.DPadRight, (int)KeyCode.D));

		floatBindings.Add(new FloatBinding(FloatAction.MoveUp, (int)KeyCode.W));
		floatBindings.Add(new FloatBinding(FloatAction.MoveDown, (int)KeyCode.S));
		floatBindings.Add(new FloatBinding(FloatAction.MoveLeft, (int)KeyCode.A));
		floatBindings.Add(new FloatBinding(FloatAction.MoveRight, (int)KeyCode.D));


	}

	//void Update()
	//{
	//    if (GetBoolInputUp(BoolAction.LightAttack))
	//        Debug.Log(GetBoolInputUp(BoolAction.LightAttack));

	//    Debug.Log(GetFloatInput(FloatAction.MoveLeft, FloatAction.MoveRight));
	//}

	static public bool GetBoolInput(BoolAction action)
	{
		for (int i = 0; i < boolBindings.Count; ++i)
		{
			if (boolBindings[i].action == action)
			{
				if (Xinput.GetButton((Xinput.ControllerButton)boolBindings[i].xinputKey))
				{
					showControllerIcons = true;
					return true;
				}
				if (Input.GetKey((KeyCode)boolBindings[i].keyboardMouseKey))
				{
					showControllerIcons = false;
					return true;
				}
			}
		}
		return false;
	}
	static public bool GetBoolInputDown(BoolAction action)
	{
		for (int i = 0; i < boolBindings.Count; ++i)
		{
			if (boolBindings[i].action == action)
			{
				if (Xinput.GetButtonDown((Xinput.ControllerButton)boolBindings[i].xinputKey))
				{
					showControllerIcons = true;
					return true;
				}
				if (Input.GetKeyDown((KeyCode)boolBindings[i].keyboardMouseKey))
				{
					showControllerIcons = false;
					return true;
				}
			}
		}
		return false;
	}
	static public bool GetBoolInputUp(BoolAction action)
	{
		for (int i = 0; i < boolBindings.Count; ++i)
		{
			if (boolBindings[i].action == action)
			{
				if (Xinput.GetButtonUp((Xinput.ControllerButton)boolBindings[i].xinputKey))
				{
					showControllerIcons = true;
					return true;
				}
				if (Input.GetKeyUp((KeyCode)boolBindings[i].keyboardMouseKey))
				{
					showControllerIcons = false;
					return true;
				}
			}
		}
		return false;
	}
	static public float GetFloatInput(FloatAction negative, FloatAction positive)
	{
		for (int i = 0; i < floatBindings.Count; ++i)
		{
			if (floatBindings[i].action == negative)
			{
				if (Input.GetKey((KeyCode)floatBindings[i].keyboardMouseKey))
				{
					showControllerIcons = false;
					return -1.0f;
				}
			}
			if (floatBindings[i].action == positive)
			{
				if (Input.GetKey((KeyCode)floatBindings[i].keyboardMouseKey))
				{
					showControllerIcons = false;
					return 1.0f;
				}
			}
		}
		if (negative == FloatAction.MoveLeft || negative == FloatAction.MoveRight)
		{
			float value = Xinput.GetAxis(Xinput.ControllerAxis.LeftX);
			if (value != 0)
				showControllerIcons = true;
			return value;
		}
		if (negative == FloatAction.MoveUp || negative == FloatAction.MoveDown)
		{
			float value = Xinput.GetAxis(Xinput.ControllerAxis.LeftY);
			if (value != 0)
				showControllerIcons = true;
			return value;
		}
		return 0.0f;

	}
	static public BoolBinding GetBoolBinding(BoolAction action)
	{
		for (int i = 0; i < boolBindings.Count; ++i)
		{
			if (boolBindings[i].action == action)
				return boolBindings[i];
		}
		Debug.LogError("Binding not found returning 0");
		return boolBindings[0];
	}
}
