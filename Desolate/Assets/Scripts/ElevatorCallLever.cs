﻿using UnityEngine;
using System.Collections;

public class ElevatorCallLever : MonoBehaviour
{

    public GenericElevatorButton elevatorscript;
    public GenericElevatorButton.State activeOnThisState;
    private LeverController leverScript;
    //private Vector3 startPos;
    //private Vector3 pressedChange = new Vector3(0, -0.5f, 0);
    bool active;
    void Start()
    {
        leverScript = GetComponent<LeverController>();
        //startPos = transform.position;
        active = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (elevatorscript.GetCurrentState() == activeOnThisState && !active)
        {
            active = true;
        }
        else if (elevatorscript.GetCurrentState() != activeOnThisState && active)
        {
            active = false;
        }
    }
    public void Activate()
    {
        if (active)
        {
            elevatorscript.Activate();
            leverScript.activated = true;
        }
    }
}
