﻿using UnityEngine;
using System.Collections;
using UnityEditor;

public class PlayerCamera : MonoBehaviour
{
	private Transform player;
	private Transform childPivot;
	private Transform cameraArm;
	private Transform playerCamera;
	//private Quaternion defaultCameraRotation;
	private Vector3 cameraArmLocalPosition;
	private float cameraOrbitSpeed = 75;

	private float yMinLimit = -20f;
	private float yMaxLimit = 80f;
	private float minDistance = 3;
	private float maxDistance = 10;

	private const float slerpDelay = 0.8f;
	private float timer = 0.0f;

	private LockOnSystem lockOnSystem;
	private Transform lockOnTarget;
	public LayerMask layerMask;

	private const float yAngleRestrictionUp = 55.0f;
	private const float yAngleRestrictionDown = 60.0f;

	void Start()
	{
		childPivot = transform.GetChild(0);

		cameraArm = transform.GetChild(0).GetChild(0);
		playerCamera = transform.GetChild(0).GetChild(0).GetChild(0); ;
		//defaultCameraRotation = cameraArm.rotation;

		player = GameObject.FindGameObjectWithTag("Player").transform;
		lockOnSystem = player.GetComponent<LockOnSystem>();
	}

	void LateUpdate()
	{
		if (IsLockedOn())
		{
			UpdateLockOnCamera();
			return; //Remove When Tested
		}
		else
		{
			playerCamera.rotation = Quaternion.Lerp(playerCamera.rotation, Quaternion.LookRotation(cameraArm.forward), Time.deltaTime * 15.5f);
			transform.position = player.position;

			float X = Xinput.GetAxis(Xinput.ControllerAxis.RightX) * cameraOrbitSpeed * 1.4f * 0.02f;
			float Y = -Xinput.GetAxis(Xinput.ControllerAxis.RightY) * cameraOrbitSpeed * 0.02f;

			transform.Rotate(Vector3.up, X);


			float angle = Vector3.Angle(transform.forward, childPivot.forward);
			float offset = (cameraArm.transform.position.y - transform.position.y);
			angle *= (offset / Mathf.Abs(offset));
			if (angle >= yAngleRestrictionUp)
				if (Y > 0)
					Y = 0;
			if (angle <= -yAngleRestrictionDown)
				if (Y < 0)
					Y = 0;

			childPivot.transform.Rotate(Vector3.right, Y);

			if (X == 0 && Y == 0 && CanSelfAlign())
			{
				timer += Time.deltaTime;
				if (timer > slerpDelay)
					transform.rotation = Quaternion.Slerp(transform.rotation, player.rotation, Time.deltaTime * 0.3f * (player.GetComponent<RemadePlayerController>().GetMoveSpeedPercentage()) * 2);
			}
			else
			{
				timer = 0.0f;
			}
		}
		CheckIfViewIsClear();
	}
	private bool IsLockedOn()
	{
		if (lockOnSystem.IsLockedOn())
		{
			lockOnTarget = lockOnSystem.GetTarget();
			return true;
		}
		return false;
	}
	private float lockOnYOffset = 1.0f;
	private void UpdateLockOnCamera()
	{
		float distance = Vector3.Distance(player.position, lockOnTarget.position);
		float disPercentage = Mathf.Clamp(distance - minDistance / maxDistance, 0, 1);

		transform.position = Vector3.Lerp(transform.position, player.position, Time.deltaTime * 15.5f);

		Vector3 dir = -(player.position - lockOnTarget.position);
		//dir.y = 0;
		dir = dir.normalized;

		Vector3 xForward = Vector3.Lerp(transform.forward, dir, Time.deltaTime * 15.5f);
		xForward.y = 0;
		transform.forward = xForward;


		Vector3 lookDir = -(playerCamera.position - lockOnTarget.position + Vector3.up * -lockOnYOffset);

		playerCamera.rotation = Quaternion.Lerp(playerCamera.rotation, Quaternion.LookRotation(lookDir), Time.deltaTime * 15.5f);
	}

	private bool CanSelfAlign()
	{
		float angle = 135;
		if (Vector3.Angle(transform.forward, player.forward) > angle)
			return false;
		return true;
	}

	private void CheckIfViewIsClear()
	{
		Ray ray = new Ray(player.position, -(player.position - cameraArm.position));
		RaycastHit hit;
		if (Physics.Raycast(ray, out hit, Vector3.Distance(player.position, cameraArm.position) + 0.1f, layerMask))
		{
			playerCamera.position = (hit.point + hit.normal * 0.1f);
		}
		else
			playerCamera.localPosition = new Vector3();
	}
}

//[CustomEditor(typeof(PlayerCamera))]
//public class ObjectBuilderEditor : Editor
//{
//	public override void OnInspectorGUI()
//	{
//		DrawDefaultInspector();

//		PlayerCamera myScript = (PlayerCamera)target;
//		if (GUILayout.Button("Save Position"))
//		{
//			myScript
//		}
//	}
//}
