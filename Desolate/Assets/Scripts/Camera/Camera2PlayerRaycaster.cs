﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Camera2PlayerRaycaster : MonoBehaviour
{

    //private GameObject playerObject;
    //private float distance;
    //private List<GameObject> objectsCurrentlyHidden = new List<GameObject>();
    //private RaycastHit[] raycastHits;
    //private Ray ray;

    //// Use this for initialization
    //void Start()
    //{
    //    playerObject = GameObject.Find("RemadePlayer");
    //}

    //// Update is called once per frame
    //void Update()
    //{
    //    if (!playerObject)
    //    {
    //        return;
    //    }
    //    distance = Vector3.Distance(transform.position, playerObject.transform.position);
    //    ray.origin = transform.position;
    //    ray.direction = playerObject.transform.position - transform.position;
    //    ray.direction = ray.direction.normalized;
    //    raycastHits = Physics.RaycastAll(ray, distance, -5, QueryTriggerInteraction.Ignore);
    //    for (int i = 0; i < raycastHits.Length; ++i)
    //    {
    //        if (raycastHits[i].collider.GetComponent<MeshRenderer>())
    //        {
    //            Material mat = raycastHits[i].collider.GetComponent<Renderer>().material;
    //            Color color = mat.color;
    //            color.a = 0.5f;
    //            mat.color = color;
    //            objectsCurrentlyHidden.Add(raycastHits[i].collider.gameObject);

    //        }
    //    }
    //    for (int i = 0; i < objectsCurrentlyHidden.Count; ++i)
    //    {
    //        bool found = false;
    //        for (int j = 0; j < raycastHits.Length; ++j)
    //        {
    //            if (objectsCurrentlyHidden[i].name == raycastHits[j].collider.name)
    //            {
    //                found = true;
    //            }
    //        }
    //        if (!found) //The collider is no longer in the way of the player and should be reenabled and removed from the list
    //        {
    //            Material mat = objectsCurrentlyHidden[i].GetComponent<Renderer>().material;
    //            Color color = mat.color;
    //            color.a = 1.0f;
    //            mat.color = color;
    //            objectsCurrentlyHidden.RemoveAt(i);
    //        }
    //    }
    //}


    public GameObject player;
    public Shader shaderDifuse;
    public Shader shaderTransparent;
    public float targetAlpha;
    private float time = 0.7f;
    public GameObject o;
    public bool mustFadeBack = false;

    // Use this for initialization
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        shaderDifuse = Shader.Find("Diffuse");
        shaderTransparent = Shader.Find("Transparent/Diffuse");
    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, player.transform.position - transform.position, out hit, 30))
        {
            if (hit.collider.gameObject.layer == LayerMask.NameToLayer("CanHide"))
            {
                mustFadeBack = true;

                if (hit.collider.gameObject != o && o != null)
                {
                    FadeUp(o);
                }

                o = hit.collider.gameObject;

                if (o.GetComponent<Renderer>().material.shader != shaderTransparent)
                {
                    o.GetComponent<Renderer>().material.shader = shaderTransparent;
                    Color k = o.GetComponent<Renderer>().material.color;
                    k.a = 0.5f;
                    o.GetComponent<Renderer>().material.color = k;
                }

                FadeDown(o);
            }
            else
            {
                if (mustFadeBack)
                {
                    mustFadeBack = false;
                    FadeUp(o);
                }
            }
        }
    }

    void FadeUp(GameObject f)
    {
        //iTween.Stop(f);
        iTween.FadeTo(f, iTween.Hash("alpha", 1, "time", time, "oncomplete", "SetDifuseShading", "oncompletetarget", this.gameObject, "oncompleteparams", f));
    }

    void FadeDown(GameObject f)
    {
        //iTween.Stop(f);
        iTween.FadeTo(f, iTween.Hash("alpha", targetAlpha, "time", time));
    }

    void SetDifuseShading(GameObject f)
    {
        if (f.GetComponent<Renderer>().material.color.a == 1)
        {
            f.GetComponent<Renderer>().material.shader = shaderDifuse;
        }
    }

    //void OnDrawGizmos()
    //{
    //    Gizmos.color = Color.red;
    //    Gizmos.DrawRay(transform.position, player.transform.position - transform.position);
    //}

}
