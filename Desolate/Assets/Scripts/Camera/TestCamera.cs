﻿using UnityEngine;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class TestCamera : MonoBehaviour
{
    public Transform target;
    public float moveSpeed = 4;
    public float verticalDistance = 8;
    public float depthOffset = 4;
    private Vector3 targetPos;
    private Quaternion rotation;

    private float surroundingCheckRange = 1.0f;


    private float distance = 4.0f;
    private float lerpToTargetSpeed = 5.0f;
    public Transform point;

    float speed = 15.0f;

    // Use this for initialization
    void Start()
    {
        rotation = transform.rotation;
        
    }

    // Update is called once per frame
    //void Update()
    //{
    //    //transform.rotation = Quaternion.Slerp(transform.rotation, rotation, moveSpeed * Time.deltaTime);

    //    targetPos = target.position + Vector3.up * verticalDistance;
    //    Vector3 forward = transform.forward;
    //    forward.y = 0;
    //    targetPos += forward * -depthOffset; // * GetBackDistance();

    //    transform.position = Vector3.Lerp(transform.position, targetPos, moveSpeed * Time.deltaTime);
    //    //(transform.LookAt(target);


    //}
    //public void Update()
    //{
    //    Vector3 direction = point.position;
    //    direction = direction.normalized;
    //    point.position = direction * distance;
    //    float speed = 15.0f;
    //    point.position += transform.right * -Xinput.GetAxis(Xinput.ControllerAxis.RightX) * Time.deltaTime * speed;
    //    point.position += transform.up * -Xinput.GetAxis(Xinput.ControllerAxis.RightY) * Time.deltaTime * speed / 2;

    //    point.LookAt(target);

    //    transform.position = target.position + direction * distance;
    //    transform.LookAt(target);
    //    if (target.GetComponent<LockOnSystem>().IsLockedOn())
    //    {
    //        Transform enemy = target.GetComponent<LockOnSystem>().GetTarget();
    //        transform.position = target.position + ((target.position - enemy.position).normalized * distance);
    //        transform.LookAt(enemy);
    //    }
    //}
    public void Update()
    {

        targetPos = transform.position;
        
        targetPos += transform.right * -Xinput.GetAxis(Xinput.ControllerAxis.RightX) * Time.deltaTime * speed;
        targetPos += transform.up * -Xinput.GetAxis(Xinput.ControllerAxis.RightY) * Time.deltaTime * speed / 2;

        Vector3 direction = transform.position - target.position;
        direction = direction.normalized;

        targetPos = target.position + direction * distance;
        transform.position = Vector3.MoveTowards(transform.position, targetPos, lerpToTargetSpeed * Time.deltaTime);
        transform.LookAt(target);
        
    }
    public void SetPosInEditor()
    {

        targetPos = target.position + Vector3.up * verticalDistance;
        Vector3 forward = transform.forward;
        forward.y = 0;
        targetPos += forward * -depthOffset;

        transform.position = targetPos;
    }

}



#if UNITY_EDITOR
[CustomEditor(typeof(TestCamera))]
public class SetCamera : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        //WaypointScript script = (WaypointScript)target;
        if (GUILayout.Button("Set camera view"))
        {
            TestCamera script = (TestCamera)target;
            script.SetPosInEditor();
        }
    }
}
#endif