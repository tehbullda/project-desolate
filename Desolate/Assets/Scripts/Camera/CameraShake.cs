﻿using UnityEngine;
using System.Collections;

public class CameraShake : MonoBehaviour
{
	private const float shakeTime = 0.1f;

	private static float Intensity = 0.2f;
    private static bool shakeActive = false;
    private static float timer = 0.0f;

    public static void StartShake()
    {
        shakeActive = true;
        timer = 0.0f;
    }
    void Update()
    {
		if (shakeActive)
        {
            Shake();
            timer += Time.deltaTime;
			if (timer >= shakeTime)
				shakeActive = false;
        }
        
    }
    void Shake()
    {
        transform.position += new Vector3(Random.Range(-1.0f, 1.0f), Random.Range(-1.0f, 1.0f), Random.Range(-1.0f, 1.0f)) * Intensity * Time.deltaTime;
    }
}
