﻿using UnityEngine;
using System.Collections;

public class CritterDamageCheck : MonoBehaviour
{

    [SerializeField]
    private float damage = 20.0f;
    private float damagingcooldown = 2.0f;
    private float currentdmgcd = 0.0f;
    //[SerializeField]
    private Material mat;
    private Color original;
    // Use this for initialization
    void Start()
    {
        mat = GetComponentInChildren<Renderer>().material;
        original = mat.color;
    }

    // Update is called once per frame
    void Update()
    {
        if (currentdmgcd >= 0.0f)
        {
            currentdmgcd -= Time.deltaTime;
        }
    }
    void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.tag == "Player" && currentdmgcd <= 0.0f)
        {
            collision.collider.GetComponent<Health>().ReduceHealth(damage);
            currentdmgcd = damagingcooldown;
            StartCoroutine(FlashMaterial(Color.red, damagingcooldown - 0.1f));
        }
    }
    IEnumerator FlashMaterial(Color color, float time)
    {
        GetComponent<Rigidbody>().mass = 100.0f;
        float elapsedtime = 0.0f;
        while (elapsedtime < time)
        {
            elapsedtime += Time.deltaTime;
            mat.color = Color.Lerp(color, original, (elapsedtime / time));
            yield return new WaitForEndOfFrame();
        }
        GetComponent<Rigidbody>().mass = 1.0f;
    }
}
