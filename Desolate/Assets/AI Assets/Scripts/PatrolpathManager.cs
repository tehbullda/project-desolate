﻿using UnityEngine;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;
#endif
public class PatrolpathManager : MonoBehaviour {
    public GameObject Patrolpathobj;
    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public GameObject GetPatrolPathPrefab()
    {
        return Patrolpathobj;
    }
}

#if UNITY_EDITOR
[CustomEditor(typeof(PatrolpathManager))]
public class PatrolPathEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        PatrolpathManager script = (PatrolpathManager)target;
        if (GUILayout.Button("Add waypointobject"))
        {
            GameObject temp = Instantiate(script.GetPatrolPathPrefab());
            temp.transform.SetParent(script.transform);
            temp.name = "Patrolpath " + (script.transform.childCount);
        }
    }
}
#endif
