﻿using UnityEngine;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;
#endif
public class WaypointScript : MonoBehaviour {
    [System.Serializable]
    public struct Waypoint
    {
        public float closeEnoughDistance;
        public float agentPauseTime;
		public float moveSpeed;
		[HideInInspector]
		public Vector3 position;
		public AI.Task.TaskInfo[] taskList;
    };
    [SerializeField]
    private Waypoint waypointData;
	void Awake () {
        GetComponent<MeshRenderer>().enabled = false;
		waypointData.position = transform.position;
	}
    public Waypoint GetWaypointData()
    {
        return waypointData;
    }
#if UNITY_EDITOR
    public void Drop()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, -Vector3.up, out hit, 25.0f))
        {
            transform.position = hit.point + new Vector3(0,0.5f,0);
        }
    }
#endif
}
#if UNITY_EDITOR
[CustomEditor(typeof(WaypointScript))]
public class WaypointUI : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        WaypointScript script = (WaypointScript)target;
        if (GUILayout.Button("Drop to surface"))
        {
            script.Drop();
        }
    }
}
#endif
