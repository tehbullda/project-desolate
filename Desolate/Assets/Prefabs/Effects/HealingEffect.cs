﻿using UnityEngine;
using System.Collections;

public class HealingEffect : MonoBehaviour
{

	private ParticleSystem particleSystem;
	private Light light;
	private bool canRun = true;

	// Use this for initialization
	void Start()
	{
		particleSystem = transform.FindChild("Particles").GetComponent<ParticleSystem>();
		light = transform.FindChild("Light").GetComponent<Light>();
		light.enabled = false;
	}

	// Update is called once per frame
	void Update()
	{

	}
	public void Play()
	{

		StartCoroutine(Sequence());
	}
	private IEnumerator Sequence()
	{
		canRun = false;
		particleSystem.Play();
		yield return new WaitForSeconds(0.25f);
		light.enabled = true;
		float value = 1.0f;
		while (value > 0.0f)
		{
			if (!canRun)
				canRun = true;
			value -= Time.deltaTime;
			light.intensity = value;
			yield return new WaitForEndOfFrame();
		}
		light.intensity = 0;
		light.enabled = false;
	}
}
